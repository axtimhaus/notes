[schmid@informatik.uni-leipzig.de](mailto:schmid@informatik.uni-leipzig.de)

## Bibliography:
[[Schmid (2019)]], [[Schmid (2021)]], [[Schmid & Grosse (2022)]]