> Jacques Derrida was an Algerian-born French philosopher. He developed the philosophy of deconstruction, which he utilized in numerous texts, and which was developed through close readings of the linguistics of Ferdinand de Saussure and Husserlian and Heideggerian phenomenology.

(wikipedia.org)