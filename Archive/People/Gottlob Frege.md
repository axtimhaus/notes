Friedrich Ludwig Gottlob Frege

On the Principle of Compositionality: [[The meaning of a complex expression is a function of the meanings of its parts.]]
On function-argument structure: [[Function-argument structure provides a logical structure of quantified sentences.]]