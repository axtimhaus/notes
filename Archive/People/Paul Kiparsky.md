---
alias: Kiparsky
---

```ad-quote
title: Jochen Trommer

"Kiparsky $\approx$ Chomsky"
```

Paul Kiparsky is a phonologist closely linked to stratal phonology. He is placed both at the beginning of [[Lexical Phonology]] and [[Stratal OT]], making him the "face" of the field, in a similar vein to Chomsky for large parts of Generative Grammar.