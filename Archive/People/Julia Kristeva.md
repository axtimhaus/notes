> Julia Kristeva is a Bulgarian-French philosopher, literary critic, semiotician, psychoanalyst, feminist, and, most recently, novelist, who has lived in France since the mid-1960s. She has taught at Columbia University, and is now a professor emerita at Université Paris Cité.

(wikipedia.org)