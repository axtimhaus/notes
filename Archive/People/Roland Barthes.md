> Roland Gérard Barthes was a French literary theorist, essayist, philosopher, critic, and semiotician. His work engaged in the analysis of a variety of sign systems, mainly derived from Western popular culture.

(wikipedia.org)