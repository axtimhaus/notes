---

excalidraw-plugin: parsed
tags: [excalidraw]

---
==⚠  Switch to EXCALIDRAW VIEW in the MORE OPTIONS menu of this document. ⚠==


# Text Elements
(1) Syntax ^57CxkUGl

expressions ^pZQ0tbfG

sound, meaning, syntax ^BOYdsaes

category ^KxxFMbSY

NP, VP, CP ^ygFRIz7I

S ^oNMCHawN

(NP\S) ^pVp0Eu1e

(NP\S) ^R462rEZA

(NP\S) ^L5hMlPvZ

A, A/B, B\A ^b5gEyxeJ

A over B ^8hXEvJt6

B over A ^GjTocEoC

(1) syntax ^1JEOM8Xj

(2) semantic ^Qeu2eHZN

A ^EuEQolsa

α ^cIgXSZOt

A/B ^7aMujlC9

β ^OofMiOLF

α ^CqsyQlg0

B\A ^8n8vIhyb

4/3 * 3 = 4 ^1cLAKD7O

3 * 4/3 = 4 ^vftvrnPB

4/3
3\4 ^o0svNA18

(NP\S)/NP ^vYUSFnm6

NP ^wtb2NcaB

praises ^KAS7n2fh

Bill ^ylURYiJo

NP\S
praises Bill ^6piwIqqX

NP ^VfbLZ0dJ

NP\S ^9NbE5juz

John  laughs ^dLFRheXG

=
S ^7cixcSXG

John laughs ^X8kUMPl0

B * A/B     B\A * B ^RjnqDuWQ

%%
# Drawing
```json
{
	"type": "excalidraw",
	"version": 2,
	"source": "https://github.com/zsviczian/obsidian-excalidraw-plugin/releases/tag/1.8.22",
	"elements": [
		{
			"type": "text",
			"version": 18,
			"versionNonce": 163418023,
			"isDeleted": false,
			"id": "57CxkUGl",
			"fillStyle": "hachure",
			"strokeWidth": 2,
			"strokeStyle": "dashed",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -528.0832565307619,
			"y": -366.73573659261075,
			"strokeColor": "#000",
			"backgroundColor": "#000",
			"width": 136.13592529296875,
			"height": 35,
			"seed": 1275263564,
			"groupIds": [],
			"roundness": null,
			"boundElements": null,
			"updated": 1680694612405,
			"link": null,
			"locked": false,
			"fontSize": 28,
			"fontFamily": 1,
			"text": "(1) Syntax",
			"rawText": "",
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "(1) Syntax",
			"lineHeight": 1.25
		},
		{
			"type": "text",
			"version": 28,
			"versionNonce": 643949129,
			"isDeleted": false,
			"id": "pZQ0tbfG",
			"fillStyle": "hachure",
			"strokeWidth": 2,
			"strokeStyle": "dashed",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -508.0832565307619,
			"y": -268.73573659261075,
			"strokeColor": "#000000",
			"backgroundColor": "#000",
			"width": 108.95986938476562,
			"height": 25,
			"seed": 628727924,
			"groupIds": [
				"0nrLphhqTjY3IHH2x_sez"
			],
			"roundness": null,
			"boundElements": null,
			"updated": 1680694612405,
			"link": null,
			"locked": false,
			"customData": {
				"legacyTextWrap": true
			},
			"fontSize": 20,
			"fontFamily": 1,
			"text": "expressions",
			"rawText": "",
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "expressions",
			"lineHeight": 1.25
		},
		{
			"type": "text",
			"version": 56,
			"versionNonce": 1390084807,
			"isDeleted": false,
			"id": "BOYdsaes",
			"fillStyle": "hachure",
			"strokeWidth": 2,
			"strokeStyle": "dashed",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -275.0832565307619,
			"y": -268.73573659261075,
			"strokeColor": "#000000",
			"backgroundColor": "#000",
			"width": 219.7798309326172,
			"height": 25,
			"seed": 1086705868,
			"groupIds": [
				"0nrLphhqTjY3IHH2x_sez"
			],
			"roundness": null,
			"boundElements": null,
			"updated": 1680694612405,
			"link": null,
			"locked": false,
			"customData": {
				"legacyTextWrap": true
			},
			"fontSize": 20,
			"fontFamily": 1,
			"text": "sound, meaning, syntax",
			"rawText": "",
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "sound, meaning, syntax",
			"lineHeight": 1.25
		},
		{
			"type": "arrow",
			"version": 42,
			"versionNonce": 124694476,
			"isDeleted": false,
			"id": "_tKxnaoSBwOw6KaI24Xq2",
			"fillStyle": "hachure",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -391.0832565307619,
			"y": -253.73573659261075,
			"strokeColor": "#000000",
			"backgroundColor": "#000",
			"width": 108,
			"height": 1,
			"seed": 1943368180,
			"groupIds": [
				"0nrLphhqTjY3IHH2x_sez"
			],
			"roundness": {
				"type": 2
			},
			"boundElements": null,
			"updated": 1680694612405,
			"link": null,
			"locked": false,
			"startBinding": {
				"focus": 0.2367390530437878,
				"gap": 8,
				"elementId": "pZQ0tbfG"
			},
			"endBinding": {
				"focus": -0.03021686688708407,
				"gap": 8,
				"elementId": "BOYdsaes"
			},
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": "arrow",
			"points": [
				[
					0,
					0
				],
				[
					108,
					-1
				]
			]
		},
		{
			"type": "text",
			"version": 60,
			"versionNonce": 2048725479,
			"isDeleted": false,
			"id": "KxxFMbSY",
			"fillStyle": "hachure",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -124.0832565307619,
			"y": -197.73573659261075,
			"strokeColor": "#000000",
			"backgroundColor": "#000",
			"width": 84.69992065429688,
			"height": 25,
			"seed": 1944704844,
			"groupIds": [
				"0nrLphhqTjY3IHH2x_sez"
			],
			"roundness": null,
			"boundElements": null,
			"updated": 1680694612405,
			"link": null,
			"locked": false,
			"customData": {
				"legacyTextWrap": true
			},
			"fontSize": 20,
			"fontFamily": 1,
			"text": "category",
			"rawText": "",
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "category",
			"lineHeight": 1.25
		},
		{
			"type": "arrow",
			"version": 62,
			"versionNonce": 1877869300,
			"isDeleted": false,
			"id": "Crzc-fXVZCnpfMiLXbVNM",
			"fillStyle": "hachure",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -89.88103640486725,
			"y": -203.73573659261075,
			"strokeColor": "#000000",
			"backgroundColor": "#000",
			"width": 5.984601294480399,
			"height": 38.99999999999994,
			"seed": 2042303348,
			"groupIds": [
				"0nrLphhqTjY3IHH2x_sez"
			],
			"roundness": {
				"type": 2
			},
			"boundElements": null,
			"updated": 1680694612405,
			"link": null,
			"locked": false,
			"startBinding": {
				"focus": -0.12564255163297675,
				"gap": 6,
				"elementId": "KxxFMbSY"
			},
			"endBinding": {
				"focus": -0.6015765774440676,
				"gap": 1,
				"elementId": "BOYdsaes"
			},
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": "arrow",
			"points": [
				[
					0,
					0
				],
				[
					-5.984601294480399,
					-38.99999999999994
				]
			]
		},
		{
			"type": "text",
			"version": 65,
			"versionNonce": 143670535,
			"isDeleted": false,
			"id": "ygFRIz7I",
			"fillStyle": "hachure",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -497.0832565307619,
			"y": -190.73573659261075,
			"strokeColor": "#000",
			"backgroundColor": "#000",
			"width": 148.4559783935547,
			"height": 35,
			"seed": 1640566220,
			"groupIds": [],
			"roundness": null,
			"boundElements": null,
			"updated": 1680694612405,
			"link": null,
			"locked": false,
			"fontSize": 28,
			"fontFamily": 1,
			"text": "NP, VP, CP",
			"rawText": "",
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "NP, VP, CP",
			"lineHeight": 1.25
		},
		{
			"type": "text",
			"version": 27,
			"versionNonce": 833684519,
			"isDeleted": false,
			"id": "oNMCHawN",
			"fillStyle": "hachure",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -462.0832565307619,
			"y": -37.73573659261075,
			"strokeColor": "#000",
			"backgroundColor": "#000",
			"width": 17.02398681640625,
			"height": 35,
			"seed": 1165001972,
			"groupIds": [
				"S2EMxC9DAjz9NRbGfaNE6"
			],
			"roundness": null,
			"boundElements": null,
			"updated": 1680694612405,
			"link": null,
			"locked": false,
			"fontSize": 28,
			"fontFamily": 1,
			"text": "S",
			"rawText": "",
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "S",
			"lineHeight": 1.25
		},
		{
			"type": "text",
			"version": 60,
			"versionNonce": 1089336777,
			"isDeleted": false,
			"id": "pVp0Eu1e",
			"fillStyle": "hachure",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -433.0832565307619,
			"y": -9.735736592610749,
			"strokeColor": "#000",
			"backgroundColor": "#000",
			"width": 88.64796447753906,
			"height": 35,
			"seed": 1803383884,
			"groupIds": [
				"S2EMxC9DAjz9NRbGfaNE6"
			],
			"roundness": null,
			"boundElements": null,
			"updated": 1680694612405,
			"link": null,
			"locked": false,
			"fontSize": 28,
			"fontFamily": 1,
			"text": "(NP\\S)",
			"rawText": "",
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "(NP\\S)",
			"lineHeight": 1.25
		},
		{
			"type": "line",
			"version": 36,
			"versionNonce": 90957639,
			"isDeleted": false,
			"id": "7CZGPr-HSHUEJIb4a5BGW",
			"fillStyle": "hachure",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -428.0832565307619,
			"y": -21.73573659261075,
			"strokeColor": "#000",
			"backgroundColor": "#000",
			"width": 24,
			"height": 29,
			"seed": 746309236,
			"groupIds": [
				"S2EMxC9DAjz9NRbGfaNE6"
			],
			"roundness": {
				"type": 2
			},
			"boundElements": null,
			"updated": 1680694612405,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					-24,
					29
				]
			]
		},
		{
			"type": "text",
			"version": 28,
			"versionNonce": 1646093927,
			"isDeleted": false,
			"id": "R462rEZA",
			"fillStyle": "hachure",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -210.0832565307619,
			"y": -118.73573659261075,
			"strokeColor": "#000",
			"backgroundColor": "#000",
			"width": 88.64796447753906,
			"height": 35,
			"seed": 1054935756,
			"groupIds": [
				"LhYnxpGmmK9HdkMRVUGRG"
			],
			"roundness": null,
			"boundElements": null,
			"updated": 1680694612405,
			"link": null,
			"locked": false,
			"fontSize": 28,
			"fontFamily": 1,
			"text": "(NP\\S)",
			"rawText": "",
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "(NP\\S)",
			"lineHeight": 1.25
		},
		{
			"type": "text",
			"version": 43,
			"versionNonce": 1704974217,
			"isDeleted": false,
			"id": "L5hMlPvZ",
			"fillStyle": "hachure",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -112.0832565307619,
			"y": -82.73573659261075,
			"strokeColor": "#000",
			"backgroundColor": "#000",
			"width": 88.64796447753906,
			"height": 35,
			"seed": 1671616500,
			"groupIds": [
				"LhYnxpGmmK9HdkMRVUGRG"
			],
			"roundness": null,
			"boundElements": null,
			"updated": 1680694612405,
			"link": null,
			"locked": false,
			"fontSize": 28,
			"fontFamily": 1,
			"text": "(NP\\S)",
			"rawText": "",
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "(NP\\S)",
			"lineHeight": 1.25
		},
		{
			"type": "line",
			"version": 117,
			"versionNonce": 647102855,
			"isDeleted": false,
			"id": "eSXwMmm6gKk0JWNPrIwsI",
			"fillStyle": "hachure",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -96.0832565307619,
			"y": -104.73573659261075,
			"strokeColor": "#000",
			"backgroundColor": "#000",
			"width": 34,
			"height": 35,
			"seed": 1049481548,
			"groupIds": [
				"LhYnxpGmmK9HdkMRVUGRG"
			],
			"roundness": {
				"type": 2
			},
			"boundElements": null,
			"updated": 1680694612405,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					-34,
					35
				]
			]
		},
		{
			"type": "text",
			"version": 57,
			"versionNonce": 823413065,
			"isDeleted": false,
			"id": "b5gEyxeJ",
			"fillStyle": "hachure",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -496.0832565307619,
			"y": -108.73573659261075,
			"strokeColor": "#000",
			"backgroundColor": "#000",
			"width": 166.2079620361328,
			"height": 35,
			"seed": 384547188,
			"groupIds": [
				"eYElvLP44_Jqgcy-i92oO"
			],
			"roundness": null,
			"boundElements": null,
			"updated": 1680694612405,
			"link": null,
			"locked": false,
			"fontSize": 28,
			"fontFamily": 1,
			"text": "A, A/B, B\\A",
			"rawText": "",
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "A, A/B, B\\A",
			"lineHeight": 1.25
		},
		{
			"type": "text",
			"version": 90,
			"versionNonce": 1344703431,
			"isDeleted": false,
			"id": "8hXEvJt6",
			"fillStyle": "hachure",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -476.0832565307619,
			"y": -135.23573659261075,
			"strokeColor": "#000",
			"backgroundColor": "#000",
			"width": 70.99195861816406,
			"height": 20,
			"seed": 1776142284,
			"groupIds": [
				"eYElvLP44_Jqgcy-i92oO"
			],
			"roundness": null,
			"boundElements": null,
			"updated": 1680694612405,
			"link": null,
			"locked": false,
			"fontSize": 16,
			"fontFamily": 1,
			"text": "A over B",
			"rawText": "",
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "A over B",
			"lineHeight": 1.25
		},
		{
			"type": "text",
			"version": 56,
			"versionNonce": 338573353,
			"isDeleted": false,
			"id": "GjTocEoC",
			"fillStyle": "hachure",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -385.0832565307619,
			"y": -136.73573659261075,
			"strokeColor": "#000",
			"backgroundColor": "#000",
			"width": 70.99195861816406,
			"height": 20,
			"seed": 27028212,
			"groupIds": [
				"eYElvLP44_Jqgcy-i92oO"
			],
			"roundness": null,
			"boundElements": null,
			"updated": 1680694612405,
			"link": null,
			"locked": false,
			"fontSize": 16,
			"fontFamily": 1,
			"text": "B over A",
			"rawText": "",
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "B over A",
			"lineHeight": 1.25
		},
		{
			"type": "line",
			"version": 23,
			"versionNonce": 923761383,
			"isDeleted": false,
			"id": "1R-7AukT4jno_2sq1WYpM",
			"fillStyle": "hachure",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -453.0832565307619,
			"y": -69.73573659261075,
			"strokeColor": "#000",
			"backgroundColor": "#000",
			"width": 48,
			"height": 1,
			"seed": 1156247116,
			"groupIds": [
				"eYElvLP44_Jqgcy-i92oO"
			],
			"roundness": {
				"type": 2
			},
			"boundElements": null,
			"updated": 1680694612405,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					48,
					1
				]
			]
		},
		{
			"type": "freedraw",
			"version": 227,
			"versionNonce": 535251721,
			"isDeleted": false,
			"id": "ZJtsb5Fln0sXiKnjYf_2-",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -454.0832565307619,
			"y": -18.73573659261075,
			"strokeColor": "#000",
			"backgroundColor": "#000",
			"width": 65,
			"height": 16.000000000000007,
			"seed": 104524916,
			"groupIds": [
				"eYElvLP44_Jqgcy-i92oO"
			],
			"roundness": null,
			"boundElements": null,
			"updated": 1680694612405,
			"link": null,
			"locked": false,
			"points": [
				[
					-5.236434108527131,
					-77.5
				],
				[
					-6.496124031007751,
					-79.16666666666667
				],
				[
					-6.496124031007751,
					-79.66666666666667
				],
				[
					-6.7480620155038755,
					-80.5
				],
				[
					-7,
					-81.5
				],
				[
					-7,
					-82.33333333333334
				],
				[
					-6.7480620155038755,
					-83
				],
				[
					-6.496124031007751,
					-84.16666666666667
				],
				[
					-6.2441860465116275,
					-85
				],
				[
					-5.992248062015504,
					-85.66666666666667
				],
				[
					-5.488372093023255,
					-86.16666666666667
				],
				[
					-5.236434108527131,
					-86.66666666666667
				],
				[
					-4.228682170542635,
					-87
				],
				[
					-3.976744186046511,
					-87.16666666666667
				],
				[
					-3.472868217054263,
					-87.5
				],
				[
					-2.465116279069767,
					-87.66666666666667
				],
				[
					-1.7093023255813948,
					-87.83333333333334
				],
				[
					-0.9534883720930232,
					-87.83333333333334
				],
				[
					0.054263565891472965,
					-87.66666666666667
				],
				[
					1.3139534883720936,
					-87.5
				],
				[
					2.3217054263565897,
					-87.33333333333334
				],
				[
					3.0775193798449614,
					-87.16666666666667
				],
				[
					4.0852713178294575,
					-87
				],
				[
					5.093023255813954,
					-86.83333333333334
				],
				[
					6.10077519379845,
					-86.66666666666667
				],
				[
					6.604651162790699,
					-86.5
				],
				[
					7.8643410852713185,
					-86.16666666666667
				],
				[
					8.872093023255815,
					-85.83333333333334
				],
				[
					9.87984496124031,
					-85.66666666666667
				],
				[
					10.887596899224807,
					-85.5
				],
				[
					11.895348837209301,
					-85.33333333333334
				],
				[
					13.406976744186046,
					-85.33333333333334
				],
				[
					13.910852713178297,
					-85.5
				],
				[
					15.170542635658915,
					-85.5
				],
				[
					16.178294573643413,
					-85.66666666666667
				],
				[
					17.43798449612403,
					-85.83333333333334
				],
				[
					18.44573643410853,
					-86.16666666666667
				],
				[
					19.95736434108527,
					-86.66666666666667
				],
				[
					20.713178294573645,
					-87
				],
				[
					21.72093023255814,
					-87.83333333333334
				],
				[
					22.728682170542637,
					-88.5
				],
				[
					23.232558139534884,
					-89.33333333333334
				],
				[
					24.240310077519382,
					-90.5
				],
				[
					24.996124031007753,
					-91.5
				],
				[
					25.248062015503876,
					-92
				],
				[
					25.5,
					-92.33333333333334
				],
				[
					25.751937984496124,
					-92.66666666666667
				],
				[
					26.00387596899225,
					-92.83333333333334
				],
				[
					26.00387596899225,
					-93
				],
				[
					26.00387596899225,
					-92.83333333333334
				],
				[
					26.00387596899225,
					-92.5
				],
				[
					26.00387596899225,
					-92
				],
				[
					26.00387596899225,
					-91.66666666666667
				],
				[
					26.00387596899225,
					-90.83333333333334
				],
				[
					26.255813953488374,
					-90.5
				],
				[
					26.507751937984498,
					-89.5
				],
				[
					26.507751937984498,
					-88.66666666666667
				],
				[
					27.011627906976745,
					-88
				],
				[
					27.011627906976745,
					-87.5
				],
				[
					27.515503875968992,
					-87
				],
				[
					28.271317829457363,
					-86.66666666666667
				],
				[
					28.77519379844961,
					-86.33333333333334
				],
				[
					29.531007751937988,
					-86
				],
				[
					30.538759689922482,
					-85.5
				],
				[
					31.7984496124031,
					-85.5
				],
				[
					33.05813953488372,
					-85.5
				],
				[
					34.31782945736434,
					-85.5
				],
				[
					34.82170542635659,
					-85.5
				],
				[
					36.58527131782945,
					-85.66666666666667
				],
				[
					37.59302325581396,
					-85.83333333333334
				],
				[
					39.1046511627907,
					-86
				],
				[
					40.1124031007752,
					-86
				],
				[
					41.372093023255815,
					-86.16666666666667
				],
				[
					42.63178294573643,
					-86.16666666666667
				],
				[
					44.3953488372093,
					-86.16666666666667
				],
				[
					45.15116279069767,
					-86.16666666666667
				],
				[
					46.15891472868218,
					-86.16666666666667
				],
				[
					47.92248062015504,
					-86
				],
				[
					49.18217054263566,
					-85.83333333333334
				],
				[
					50.945736434108525,
					-85.5
				],
				[
					51.701550387596896,
					-85.33333333333334
				],
				[
					52.70930232558139,
					-84.66666666666667
				],
				[
					53.96899224806202,
					-84.16666666666667
				],
				[
					55.480620155038764,
					-83.5
				],
				[
					56.236434108527135,
					-82.83333333333334
				],
				[
					57.24418604651163,
					-81.83333333333334
				],
				[
					57.748062015503876,
					-80.66666666666667
				],
				[
					58,
					-80.16666666666667
				],
				[
					58,
					-79
				],
				[
					58,
					-77.83333333333333
				],
				[
					58,
					-77.33333333333333
				],
				[
					58,
					-77
				],
				[
					58,
					-77
				]
			],
			"lastCommittedPoint": null,
			"simulatePressure": false,
			"pressures": [
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0
			]
		},
		{
			"type": "freedraw",
			"version": 260,
			"versionNonce": 760055303,
			"isDeleted": false,
			"id": "YyQdaF9eb63cN4xpAvFYO",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -379.5832565307619,
			"y": -18.73573659261075,
			"strokeColor": "#000",
			"backgroundColor": "#000",
			"width": 65,
			"height": 16.000000000000007,
			"seed": 1083131084,
			"groupIds": [
				"eYElvLP44_Jqgcy-i92oO"
			],
			"roundness": null,
			"boundElements": null,
			"updated": 1680694612405,
			"link": null,
			"locked": false,
			"points": [
				[
					-5.236434108527131,
					-77.5
				],
				[
					-6.496124031007751,
					-79.16666666666667
				],
				[
					-6.496124031007751,
					-79.66666666666667
				],
				[
					-6.7480620155038755,
					-80.5
				],
				[
					-7,
					-81.5
				],
				[
					-7,
					-82.33333333333334
				],
				[
					-6.7480620155038755,
					-83
				],
				[
					-6.496124031007751,
					-84.16666666666667
				],
				[
					-6.2441860465116275,
					-85
				],
				[
					-5.992248062015504,
					-85.66666666666667
				],
				[
					-5.488372093023255,
					-86.16666666666667
				],
				[
					-5.236434108527131,
					-86.66666666666667
				],
				[
					-4.228682170542635,
					-87
				],
				[
					-3.976744186046511,
					-87.16666666666667
				],
				[
					-3.472868217054263,
					-87.5
				],
				[
					-2.465116279069767,
					-87.66666666666667
				],
				[
					-1.7093023255813948,
					-87.83333333333334
				],
				[
					-0.9534883720930232,
					-87.83333333333334
				],
				[
					0.054263565891472965,
					-87.66666666666667
				],
				[
					1.3139534883720936,
					-87.5
				],
				[
					2.3217054263565897,
					-87.33333333333334
				],
				[
					3.0775193798449614,
					-87.16666666666667
				],
				[
					4.0852713178294575,
					-87
				],
				[
					5.093023255813954,
					-86.83333333333334
				],
				[
					6.10077519379845,
					-86.66666666666667
				],
				[
					6.604651162790699,
					-86.5
				],
				[
					7.8643410852713185,
					-86.16666666666667
				],
				[
					8.872093023255815,
					-85.83333333333334
				],
				[
					9.87984496124031,
					-85.66666666666667
				],
				[
					10.887596899224807,
					-85.5
				],
				[
					11.895348837209301,
					-85.33333333333334
				],
				[
					13.406976744186046,
					-85.33333333333334
				],
				[
					13.910852713178297,
					-85.5
				],
				[
					15.170542635658915,
					-85.5
				],
				[
					16.178294573643413,
					-85.66666666666667
				],
				[
					17.43798449612403,
					-85.83333333333334
				],
				[
					18.44573643410853,
					-86.16666666666667
				],
				[
					19.95736434108527,
					-86.66666666666667
				],
				[
					20.713178294573645,
					-87
				],
				[
					21.72093023255814,
					-87.83333333333334
				],
				[
					22.728682170542637,
					-88.5
				],
				[
					23.232558139534884,
					-89.33333333333334
				],
				[
					24.240310077519382,
					-90.5
				],
				[
					24.996124031007753,
					-91.5
				],
				[
					25.248062015503876,
					-92
				],
				[
					25.5,
					-92.33333333333334
				],
				[
					25.751937984496124,
					-92.66666666666667
				],
				[
					26.00387596899225,
					-92.83333333333334
				],
				[
					26.00387596899225,
					-93
				],
				[
					26.00387596899225,
					-92.83333333333334
				],
				[
					26.00387596899225,
					-92.5
				],
				[
					26.00387596899225,
					-92
				],
				[
					26.00387596899225,
					-91.66666666666667
				],
				[
					26.00387596899225,
					-90.83333333333334
				],
				[
					26.255813953488374,
					-90.5
				],
				[
					26.507751937984498,
					-89.5
				],
				[
					26.507751937984498,
					-88.66666666666667
				],
				[
					27.011627906976745,
					-88
				],
				[
					27.011627906976745,
					-87.5
				],
				[
					27.515503875968992,
					-87
				],
				[
					28.271317829457363,
					-86.66666666666667
				],
				[
					28.77519379844961,
					-86.33333333333334
				],
				[
					29.531007751937988,
					-86
				],
				[
					30.538759689922482,
					-85.5
				],
				[
					31.7984496124031,
					-85.5
				],
				[
					33.05813953488372,
					-85.5
				],
				[
					34.31782945736434,
					-85.5
				],
				[
					34.82170542635659,
					-85.5
				],
				[
					36.58527131782945,
					-85.66666666666667
				],
				[
					37.59302325581396,
					-85.83333333333334
				],
				[
					39.1046511627907,
					-86
				],
				[
					40.1124031007752,
					-86
				],
				[
					41.372093023255815,
					-86.16666666666667
				],
				[
					42.63178294573643,
					-86.16666666666667
				],
				[
					44.3953488372093,
					-86.16666666666667
				],
				[
					45.15116279069767,
					-86.16666666666667
				],
				[
					46.15891472868218,
					-86.16666666666667
				],
				[
					47.92248062015504,
					-86
				],
				[
					49.18217054263566,
					-85.83333333333334
				],
				[
					50.945736434108525,
					-85.5
				],
				[
					51.701550387596896,
					-85.33333333333334
				],
				[
					52.70930232558139,
					-84.66666666666667
				],
				[
					53.96899224806202,
					-84.16666666666667
				],
				[
					55.480620155038764,
					-83.5
				],
				[
					56.236434108527135,
					-82.83333333333334
				],
				[
					57.24418604651163,
					-81.83333333333334
				],
				[
					57.748062015503876,
					-80.66666666666667
				],
				[
					58,
					-80.16666666666667
				],
				[
					58,
					-79
				],
				[
					58,
					-77.83333333333333
				],
				[
					58,
					-77.33333333333333
				],
				[
					58,
					-77
				],
				[
					58,
					-77
				]
			],
			"lastCommittedPoint": null,
			"simulatePressure": false,
			"pressures": [
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0
			]
		},
		{
			"type": "text",
			"version": 53,
			"versionNonce": 1018888681,
			"isDeleted": false,
			"id": "1JEOM8Xj",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 93.9167434692381,
			"y": -266.23573659261075,
			"strokeColor": "#000",
			"backgroundColor": "#000",
			"width": 95.93992614746094,
			"height": 25,
			"seed": 1183661556,
			"groupIds": [
				"109Vro3M28NEDKHWv8UB-"
			],
			"roundness": null,
			"boundElements": null,
			"updated": 1680694612405,
			"link": null,
			"locked": false,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "(1) syntax",
			"rawText": "",
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "(1) syntax",
			"lineHeight": 1.25
		},
		{
			"type": "text",
			"version": 30,
			"versionNonce": 1105014055,
			"isDeleted": false,
			"id": "Qeu2eHZN",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 92.9167434692381,
			"y": -229.73573659261075,
			"strokeColor": "#000",
			"backgroundColor": "#000",
			"width": 122.15989685058594,
			"height": 25,
			"seed": 577030988,
			"groupIds": [
				"109Vro3M28NEDKHWv8UB-"
			],
			"roundness": null,
			"boundElements": null,
			"updated": 1680694612405,
			"link": null,
			"locked": false,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "(2) semantic",
			"rawText": "",
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "(2) semantic",
			"lineHeight": 1.25
		},
		{
			"type": "text",
			"version": 38,
			"versionNonce": 1881862215,
			"isDeleted": false,
			"id": "EuEQolsa",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 120.9167434692381,
			"y": -160.73573659261075,
			"strokeColor": "#000",
			"backgroundColor": "#000",
			"width": 13.1199951171875,
			"height": 25,
			"seed": 999907188,
			"groupIds": [
				"T8Wfw-7xKV3uG_7-HGGsJ"
			],
			"roundness": null,
			"boundElements": null,
			"updated": 1680694612405,
			"link": null,
			"locked": false,
			"customData": {
				"legacyTextWrap": true
			},
			"fontSize": 20,
			"fontFamily": 1,
			"text": "A",
			"rawText": "",
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "A",
			"lineHeight": 1.25
		},
		{
			"type": "text",
			"version": 33,
			"versionNonce": 812669865,
			"isDeleted": false,
			"id": "cIgXSZOt",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 178.9167434692381,
			"y": -159.73573659261075,
			"strokeColor": "#000",
			"backgroundColor": "#000",
			"width": 13.979995727539062,
			"height": 25,
			"seed": 1824883148,
			"groupIds": [
				"T8Wfw-7xKV3uG_7-HGGsJ"
			],
			"roundness": null,
			"boundElements": null,
			"updated": 1680694612405,
			"link": null,
			"locked": false,
			"customData": {
				"legacyTextWrap": true
			},
			"fontSize": 20,
			"fontFamily": 1,
			"text": "α",
			"rawText": "",
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "α",
			"lineHeight": 1.25
		},
		{
			"type": "text",
			"version": 25,
			"versionNonce": 303255399,
			"isDeleted": false,
			"id": "7aMujlC9",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 108.9167434692381,
			"y": -109.73573659261075,
			"strokeColor": "#000",
			"backgroundColor": "#000",
			"width": 37.65998840332031,
			"height": 25,
			"seed": 1302679796,
			"groupIds": [
				"T8Wfw-7xKV3uG_7-HGGsJ"
			],
			"roundness": null,
			"boundElements": null,
			"updated": 1680694612405,
			"link": null,
			"locked": false,
			"customData": {
				"legacyTextWrap": true
			},
			"fontSize": 20,
			"fontFamily": 1,
			"text": "A/B",
			"rawText": "",
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "A/B",
			"lineHeight": 1.25
		},
		{
			"type": "text",
			"version": 37,
			"versionNonce": 1192820361,
			"isDeleted": false,
			"id": "OofMiOLF",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 183.9167434692381,
			"y": -108.73573659261075,
			"strokeColor": "#000",
			"backgroundColor": "#000",
			"width": 12.55999755859375,
			"height": 25,
			"seed": 1187735628,
			"groupIds": [
				"T8Wfw-7xKV3uG_7-HGGsJ"
			],
			"roundness": null,
			"boundElements": null,
			"updated": 1680694612405,
			"link": null,
			"locked": false,
			"customData": {
				"legacyTextWrap": true
			},
			"fontSize": 20,
			"fontFamily": 1,
			"text": "β",
			"rawText": "",
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "β",
			"lineHeight": 1.25
		},
		{
			"type": "text",
			"version": 36,
			"versionNonce": 300075655,
			"isDeleted": false,
			"id": "CqsyQlg0",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 233.9167434692381,
			"y": -108.73573659261075,
			"strokeColor": "#000",
			"backgroundColor": "#000",
			"width": 13.979995727539062,
			"height": 25,
			"seed": 1815548532,
			"groupIds": [
				"T8Wfw-7xKV3uG_7-HGGsJ"
			],
			"roundness": null,
			"boundElements": null,
			"updated": 1680694612405,
			"link": null,
			"locked": false,
			"customData": {
				"legacyTextWrap": true
			},
			"fontSize": 20,
			"fontFamily": 1,
			"text": "α",
			"rawText": "",
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "α",
			"lineHeight": 1.25
		},
		{
			"type": "text",
			"version": 44,
			"versionNonce": 174736745,
			"isDeleted": false,
			"id": "8n8vIhyb",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 111.9167434692381,
			"y": -56.73573659261075,
			"strokeColor": "#000",
			"backgroundColor": "#000",
			"width": 37.65998840332031,
			"height": 25,
			"seed": 1682996940,
			"groupIds": [
				"T8Wfw-7xKV3uG_7-HGGsJ"
			],
			"roundness": null,
			"boundElements": null,
			"updated": 1680694612405,
			"link": null,
			"locked": false,
			"customData": {
				"legacyTextWrap": true
			},
			"fontSize": 20,
			"fontFamily": 1,
			"text": "B\\A",
			"rawText": "",
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "B\\A",
			"lineHeight": 1.25
		},
		{
			"type": "arrow",
			"version": 22,
			"versionNonce": 1344198220,
			"isDeleted": false,
			"id": "KZC_LSw0FWuGHeYirFSef",
			"fillStyle": "hachure",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 145.9167434692381,
			"y": -146.73573659261075,
			"strokeColor": "#000",
			"backgroundColor": "#000",
			"width": 24,
			"height": 0,
			"seed": 638886900,
			"groupIds": [
				"T8Wfw-7xKV3uG_7-HGGsJ"
			],
			"roundness": {
				"type": 2
			},
			"boundElements": null,
			"updated": 1680694612405,
			"link": null,
			"locked": false,
			"startBinding": {
				"focus": 0.12,
				"gap": 11.886792182922363,
				"elementId": "EuEQolsa"
			},
			"endBinding": {
				"focus": -0.04,
				"gap": 9,
				"elementId": "cIgXSZOt"
			},
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": "arrow",
			"points": [
				[
					0,
					0
				],
				[
					24,
					0
				]
			]
		},
		{
			"type": "arrow",
			"version": 17,
			"versionNonce": 1721336948,
			"isDeleted": false,
			"id": "Rc4nhcCBLnn5vYuIQ_9bX",
			"fillStyle": "hachure",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 154.9167434692381,
			"y": -91.73573659261075,
			"strokeColor": "#000",
			"backgroundColor": "#000",
			"width": 19,
			"height": 0,
			"seed": 1844364620,
			"groupIds": [
				"T8Wfw-7xKV3uG_7-HGGsJ"
			],
			"roundness": {
				"type": 2
			},
			"boundElements": null,
			"updated": 1680694612405,
			"link": null,
			"locked": false,
			"startBinding": {
				"focus": 0.44,
				"gap": 8.339622497558594,
				"elementId": "7aMujlC9"
			},
			"endBinding": {
				"focus": -0.36,
				"gap": 10,
				"elementId": "OofMiOLF"
			},
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": "arrow",
			"points": [
				[
					0,
					0
				],
				[
					19,
					0
				]
			]
		},
		{
			"type": "arrow",
			"version": 18,
			"versionNonce": 218508492,
			"isDeleted": false,
			"id": "ZIyUW0FI4bFQcDtxfpQyK",
			"fillStyle": "hachure",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 200.9167434692381,
			"y": -92.73573659261075,
			"strokeColor": "#000",
			"backgroundColor": "#000",
			"width": 22,
			"height": 0,
			"seed": 1094976884,
			"groupIds": [
				"T8Wfw-7xKV3uG_7-HGGsJ"
			],
			"roundness": {
				"type": 2
			},
			"boundElements": null,
			"updated": 1680694612405,
			"link": null,
			"locked": false,
			"startBinding": {
				"focus": 0.28,
				"gap": 4.433961868286133,
				"elementId": "OofMiOLF"
			},
			"endBinding": {
				"focus": -0.28,
				"gap": 11,
				"elementId": "CqsyQlg0"
			},
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": "arrow",
			"points": [
				[
					0,
					0
				],
				[
					22,
					0
				]
			]
		},
		{
			"type": "arrow",
			"version": 27,
			"versionNonce": 1331222004,
			"isDeleted": false,
			"id": "U34_QZM7FzWjNM29Uarxb",
			"fillStyle": "hachure",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 162.9167434692381,
			"y": -46.73573659261075,
			"strokeColor": "#000",
			"backgroundColor": "#000",
			"width": 23,
			"height": 24,
			"seed": 1761622988,
			"groupIds": [
				"T8Wfw-7xKV3uG_7-HGGsJ"
			],
			"roundness": {
				"type": 2
			},
			"boundElements": null,
			"updated": 1680694612405,
			"link": null,
			"locked": false,
			"startBinding": {
				"focus": 0.9663940550395194,
				"gap": 13.339622497558594,
				"elementId": "8n8vIhyb"
			},
			"endBinding": {
				"focus": -1.1036182212433328,
				"gap": 13,
				"elementId": "OofMiOLF"
			},
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": "arrow",
			"points": [
				[
					0,
					0
				],
				[
					23,
					-24
				]
			]
		},
		{
			"type": "text",
			"version": 100,
			"versionNonce": 2085151719,
			"isDeleted": false,
			"id": "1cLAKD7O",
			"fillStyle": "hachure",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 321.5833023071291,
			"y": -268.24427642822263,
			"strokeColor": "#000",
			"backgroundColor": "#000",
			"width": 125.49995422363281,
			"height": 25,
			"seed": 1393243892,
			"groupIds": [
				"CMiwlz0ytcvpAEItT-d2q"
			],
			"roundness": null,
			"boundElements": null,
			"updated": 1680694612405,
			"link": null,
			"locked": false,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "4/3 * 3 = 4",
			"rawText": "",
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "4/3 * 3 = 4",
			"lineHeight": 1.25
		},
		{
			"type": "text",
			"version": 82,
			"versionNonce": 1947529737,
			"isDeleted": false,
			"id": "vftvrnPB",
			"fillStyle": "hachure",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 322.5833023071291,
			"y": -238.24427642822263,
			"strokeColor": "#000",
			"backgroundColor": "#000",
			"width": 125.49995422363281,
			"height": 25,
			"seed": 530506316,
			"groupIds": [
				"CMiwlz0ytcvpAEItT-d2q"
			],
			"roundness": null,
			"boundElements": null,
			"updated": 1680694612405,
			"link": null,
			"locked": false,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "3 * 4/3 = 4",
			"rawText": "",
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "3 * 4/3 = 4",
			"lineHeight": 1.25
		},
		{
			"type": "text",
			"version": 103,
			"versionNonce": 963759879,
			"isDeleted": false,
			"id": "o0svNA18",
			"fillStyle": "hachure",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -6.416697692870912,
			"y": -191.20174916585296,
			"strokeColor": "#000",
			"backgroundColor": "#000",
			"width": 36.41998291015625,
			"height": 50,
			"seed": 1475360884,
			"groupIds": [],
			"roundness": null,
			"boundElements": null,
			"updated": 1680694612405,
			"link": null,
			"locked": false,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "4/3\n3\\4",
			"rawText": "",
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "4/3\n3\\4",
			"lineHeight": 1.25
		},
		{
			"type": "text",
			"version": 47,
			"versionNonce": 714852585,
			"isDeleted": false,
			"id": "vYUSFnm6",
			"fillStyle": "hachure",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 115.58330230712909,
			"y": 37.75572357177731,
			"strokeColor": "#000",
			"backgroundColor": "#000",
			"width": 99.419921875,
			"height": 25,
			"seed": 145012940,
			"groupIds": [
				"zL0zBXIZhryoaV6qV1Ac2"
			],
			"roundness": null,
			"boundElements": null,
			"updated": 1680694612405,
			"link": null,
			"locked": false,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "(NP\\S)/NP",
			"rawText": "",
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "(NP\\S)/NP",
			"lineHeight": 1.25
		},
		{
			"type": "text",
			"version": 20,
			"versionNonce": 881874471,
			"isDeleted": false,
			"id": "wtb2NcaB",
			"fillStyle": "hachure",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 276.5833023071291,
			"y": 34.75572357177731,
			"strokeColor": "#000",
			"backgroundColor": "#000",
			"width": 26.0999755859375,
			"height": 25,
			"seed": 1270417908,
			"groupIds": [
				"zL0zBXIZhryoaV6qV1Ac2"
			],
			"roundness": null,
			"boundElements": null,
			"updated": 1680694612405,
			"link": null,
			"locked": false,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "NP",
			"rawText": "",
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "NP",
			"lineHeight": 1.25
		},
		{
			"type": "text",
			"version": 50,
			"versionNonce": 1541718985,
			"isDeleted": false,
			"id": "KAS7n2fh",
			"fillStyle": "hachure",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 131.5833023071291,
			"y": 71.75572357177731,
			"strokeColor": "#000",
			"backgroundColor": "#000",
			"width": 68.83992004394531,
			"height": 25,
			"seed": 799784780,
			"groupIds": [
				"zL0zBXIZhryoaV6qV1Ac2"
			],
			"roundness": null,
			"boundElements": null,
			"updated": 1680694612405,
			"link": null,
			"locked": false,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "praises",
			"rawText": "",
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "praises",
			"lineHeight": 1.25
		},
		{
			"type": "text",
			"version": 22,
			"versionNonce": 1418787143,
			"isDeleted": false,
			"id": "ylURYiJo",
			"fillStyle": "hachure",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 276.5833023071291,
			"y": 73.75572357177731,
			"strokeColor": "#000",
			"backgroundColor": "#000",
			"width": 29.439971923828125,
			"height": 25,
			"seed": 1097575284,
			"groupIds": [
				"zL0zBXIZhryoaV6qV1Ac2"
			],
			"roundness": null,
			"boundElements": null,
			"updated": 1680694612405,
			"link": null,
			"locked": false,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "Bill",
			"rawText": "",
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "Bill",
			"lineHeight": 1.25
		},
		{
			"type": "freedraw",
			"version": 51,
			"versionNonce": 873161385,
			"isDeleted": false,
			"id": "hCnp2eXw1oGeLJyNQ9tT2",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 246.5833023071291,
			"y": 48.64819691975919,
			"strokeColor": "#000",
			"backgroundColor": "#000",
			"width": 0.0001,
			"height": 0.0001,
			"seed": 487454156,
			"groupIds": [
				"zL0zBXIZhryoaV6qV1Ac2"
			],
			"roundness": null,
			"boundElements": null,
			"updated": 1680694612405,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					0.0001,
					0.0001
				]
			],
			"lastCommittedPoint": null,
			"simulatePressure": true,
			"pressures": []
		},
		{
			"type": "text",
			"version": 73,
			"versionNonce": 1859139687,
			"isDeleted": false,
			"id": "6piwIqqX",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 183.43235855102557,
			"y": 116.64819691975919,
			"strokeColor": "#000",
			"backgroundColor": "#000",
			"width": 86.62391662597656,
			"height": 40,
			"seed": 2068716788,
			"groupIds": [
				"zL0zBXIZhryoaV6qV1Ac2"
			],
			"roundness": null,
			"boundElements": null,
			"updated": 1680694612405,
			"link": null,
			"locked": false,
			"fontSize": 16,
			"fontFamily": 1,
			"text": "NP\\S\npraises Bill",
			"rawText": "",
			"textAlign": "center",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "NP\\S\npraises Bill",
			"lineHeight": 1.25
		},
		{
			"type": "text",
			"version": 91,
			"versionNonce": 1337664905,
			"isDeleted": false,
			"id": "VfbLZ0dJ",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -140.86009426116925,
			"y": 23.648196919759187,
			"strokeColor": "#000",
			"backgroundColor": "#000",
			"width": 20.879974365234375,
			"height": 20,
			"seed": 1038441548,
			"groupIds": [
				"xCCFmc2OaJW8m6r3gfbDJ"
			],
			"roundness": null,
			"boundElements": null,
			"updated": 1680694612405,
			"link": null,
			"locked": false,
			"fontSize": 16,
			"fontFamily": 1,
			"text": "NP",
			"rawText": "",
			"textAlign": "center",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "NP",
			"lineHeight": 1.25
		},
		{
			"type": "text",
			"version": 34,
			"versionNonce": 2031784839,
			"isDeleted": false,
			"id": "9NbE5juz",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -97.72801895141583,
			"y": 24.648196919759187,
			"strokeColor": "#000",
			"backgroundColor": "#000",
			"width": 38.60797119140625,
			"height": 20,
			"seed": 48651892,
			"groupIds": [
				"xCCFmc2OaJW8m6r3gfbDJ"
			],
			"roundness": null,
			"boundElements": null,
			"updated": 1680694612405,
			"link": null,
			"locked": false,
			"fontSize": 16,
			"fontFamily": 1,
			"text": "NP\\S",
			"rawText": "",
			"textAlign": "center",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "NP\\S",
			"lineHeight": 1.25
		},
		{
			"type": "text",
			"version": 82,
			"versionNonce": 707759209,
			"isDeleted": false,
			"id": "dLFRheXG",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -154.90726394653302,
			"y": 53.64819691975919,
			"strokeColor": "#000",
			"backgroundColor": "#000",
			"width": 98.95994567871094,
			"height": 20,
			"seed": 1275851468,
			"groupIds": [
				"xCCFmc2OaJW8m6r3gfbDJ"
			],
			"roundness": null,
			"boundElements": null,
			"updated": 1680694612405,
			"link": null,
			"locked": false,
			"fontSize": 16,
			"fontFamily": 1,
			"text": "John  laughs",
			"rawText": "",
			"textAlign": "center",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "John  laughs",
			"lineHeight": 1.25
		},
		{
			"type": "text",
			"version": 31,
			"versionNonce": 2074905255,
			"isDeleted": false,
			"id": "7cixcSXG",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -113.3506600379942,
			"y": 83.64819691975919,
			"strokeColor": "#000",
			"backgroundColor": "#000",
			"width": 9.871994018554688,
			"height": 40,
			"seed": 1090154484,
			"groupIds": [
				"xCCFmc2OaJW8m6r3gfbDJ"
			],
			"roundness": null,
			"boundElements": null,
			"updated": 1680694612405,
			"link": null,
			"locked": false,
			"fontSize": 16,
			"fontFamily": 1,
			"text": "=\nS",
			"rawText": "",
			"textAlign": "center",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "=\nS",
			"lineHeight": 1.25
		},
		{
			"type": "text",
			"version": 46,
			"versionNonce": 1261705033,
			"isDeleted": false,
			"id": "X8kUMPl0",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -149.90726394653302,
			"y": 125.64819691975919,
			"strokeColor": "#000",
			"backgroundColor": "#000",
			"width": 90.95994567871094,
			"height": 20,
			"seed": 907786572,
			"groupIds": [
				"xCCFmc2OaJW8m6r3gfbDJ"
			],
			"roundness": null,
			"boundElements": null,
			"updated": 1680694612405,
			"link": null,
			"locked": false,
			"fontSize": 16,
			"fontFamily": 1,
			"text": "John laughs",
			"rawText": "",
			"textAlign": "center",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "John laughs",
			"lineHeight": 1.25
		},
		{
			"type": "text",
			"version": 96,
			"versionNonce": 1127390663,
			"isDeleted": false,
			"id": "RjnqDuWQ",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 36.05499725341815,
			"y": -358.77085316975905,
			"strokeColor": "#000",
			"backgroundColor": "#000",
			"width": 215.03994750976562,
			"height": 25,
			"seed": 361779572,
			"groupIds": [],
			"roundness": null,
			"boundElements": null,
			"updated": 1680694612405,
			"link": null,
			"locked": false,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "B * A/B     B\\A * B",
			"rawText": "",
			"textAlign": "center",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "B * A/B     B\\A * B",
			"lineHeight": 1.25
		}
	],
	"appState": {
		"theme": "light",
		"viewBackgroundColor": "#ffffff",
		"currentItemStrokeColor": "#000000",
		"currentItemBackgroundColor": "transparent",
		"currentItemFillStyle": "hachure",
		"currentItemStrokeWidth": 1,
		"currentItemStrokeStyle": "solid",
		"currentItemRoughness": 1,
		"currentItemOpacity": 100,
		"currentItemFontFamily": 1,
		"currentItemFontSize": 20,
		"currentItemTextAlign": "left",
		"currentItemStartArrowhead": null,
		"currentItemEndArrowhead": "arrow",
		"scrollX": 222,
		"scrollY": 368.9562683105469,
		"zoom": {
			"value": 1
		},
		"currentItemRoundness": "round",
		"gridSize": null,
		"colorPalette": {},
		"currentStrokeOptions": null,
		"previousGridSize": null
	},
	"files": {}
}
```
%%