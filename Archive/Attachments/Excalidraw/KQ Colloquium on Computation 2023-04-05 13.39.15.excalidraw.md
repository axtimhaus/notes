---

excalidraw-plugin: parsed
tags: [excalidraw]

---
==⚠  Switch to EXCALIDRAW VIEW in the MORE OPTIONS menu of this document. ⚠==


# Text Elements
everyone: S/(NP\S) ^SlNNvzAH

everyone praised and someone criticised Bill ^Ilf2V9w8

S/(NP\S) ^srjwyR4s

(NP\S)\NP ^bBjvoIDS

(NP\S)/NP ^c3kU3S0V

NP ^94zlpTZq

S/NP ^uEZvKFSY

c ^H8BtnxdR

S/NP ^Wl36slyV

S/NP ^pxrpGMRK

S ^3OaLRgzh

c ^1aIiGnsg

NP ^hm5Vq3EN

e ^uVFRNQip

noone
everyone ^XalOFMJY

A ^Uk48kb0A

B ^SzNqEhAK

A/B ^8HAx6YHr

B ^kU1Ic3oK

B ^hsH7nFR8

B ^GREGvtxM

A ^Obwg4czu

B ^QA1yILmk

A/B ^FPjdE886

B ^U9EVbe5V

C ^CRbWSpRn

B/C ^mQuxrQID

A ^9Zydwrwy

B ^GzZgUre8

C ^W2W9Xc8m

A ^j0d8bZSA

B ^gJlixl8r

3/4 * 4/2 = 3/2 ^NHr3AZZC

everyone laughed & s.o crit Bill ^lPp8XAoL

S/(NP\S) ^BfQvezX6

NP\S ^4XdboboT

S/(NP\S) ^B0t7puYh

(NP\S) ^UMKiPqjP

NP ^VC6jgLI1

NP ^2IfzUAD3

NP\S ^2xBGe6Ys

S ^OkpMeyQw

NP ^LJzYdr32

S/NP ^JI5181kl

S ^99O9vYYN

c ^K66iUyGW

S ^VNlYGE16

%%
# Drawing
```json
{
	"type": "excalidraw",
	"version": 2,
	"source": "https://github.com/zsviczian/obsidian-excalidraw-plugin/releases/tag/1.8.22",
	"elements": [
		{
			"type": "text",
			"version": 161,
			"versionNonce": 1095859881,
			"isDeleted": false,
			"id": "SlNNvzAH",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -168.07494891478996,
			"y": -231.482871401872,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 181.99984741210938,
			"height": 25,
			"seed": 1688559276,
			"groupIds": [],
			"roundness": null,
			"boundElements": [],
			"updated": 1680694757816,
			"link": null,
			"locked": false,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "everyone: S/(NP\\S)",
			"rawText": "everyone: S/(NP\\S)",
			"textAlign": "center",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "everyone: S/(NP\\S)",
			"lineHeight": 1.25
		},
		{
			"type": "text",
			"version": 184,
			"versionNonce": 966699113,
			"isDeleted": false,
			"id": "Ilf2V9w8",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 112.84459211268586,
			"y": -179.14748071609688,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 427.7396240234375,
			"height": 25,
			"seed": 702951956,
			"groupIds": [
				"E-6mQ-HEGpZ-48n19HawQ"
			],
			"roundness": null,
			"boundElements": [],
			"updated": 1680694757816,
			"link": null,
			"locked": false,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "everyone praised and someone criticised Bill",
			"rawText": "everyone praised and someone criticised Bill",
			"textAlign": "center",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "everyone praised and someone criticised Bill",
			"lineHeight": 1.25
		},
		{
			"type": "text",
			"version": 127,
			"versionNonce": 860696231,
			"isDeleted": false,
			"id": "srjwyR4s",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 120.85090476609093,
			"y": -149.20248401186632,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 68.38395690917969,
			"height": 20,
			"seed": 1952229676,
			"groupIds": [
				"E-6mQ-HEGpZ-48n19HawQ"
			],
			"roundness": null,
			"boundElements": [],
			"updated": 1680694757817,
			"link": null,
			"locked": false,
			"fontSize": 16,
			"fontFamily": 1,
			"text": "S/(NP\\S)",
			"rawText": "S/(NP\\S)",
			"textAlign": "center",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "S/(NP\\S)",
			"lineHeight": 1.25
		},
		{
			"type": "text",
			"version": 126,
			"versionNonce": 1449112393,
			"isDeleted": false,
			"id": "bBjvoIDS",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 198.09252580991324,
			"y": -149.97712786959096,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 79.53593444824219,
			"height": 20,
			"seed": 201248660,
			"groupIds": [
				"E-6mQ-HEGpZ-48n19HawQ"
			],
			"roundness": null,
			"boundElements": [],
			"updated": 1680694757817,
			"link": null,
			"locked": false,
			"fontSize": 16,
			"fontFamily": 1,
			"text": "(NP\\S)\\NP",
			"rawText": "(NP\\S)\\NP",
			"textAlign": "center",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "(NP\\S)\\NP",
			"lineHeight": 1.25
		},
		{
			"type": "text",
			"version": 133,
			"versionNonce": 119456199,
			"isDeleted": false,
			"id": "c3kU3S0V",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 416.3500778077605,
			"y": -149.11444979845305,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 79.53593444824219,
			"height": 20,
			"seed": 1405526956,
			"groupIds": [
				"E-6mQ-HEGpZ-48n19HawQ"
			],
			"roundness": null,
			"boundElements": [],
			"updated": 1680694757817,
			"link": null,
			"locked": false,
			"fontSize": 16,
			"fontFamily": 1,
			"text": "(NP\\S)/NP",
			"rawText": "(NP\\S)/NP",
			"textAlign": "center",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "(NP\\S)/NP",
			"lineHeight": 1.25
		},
		{
			"type": "text",
			"version": 137,
			"versionNonce": 276587049,
			"isDeleted": false,
			"id": "94zlpTZq",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 510.39055659432415,
			"y": -148.25177172731514,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 20.879974365234375,
			"height": 20,
			"seed": 740801812,
			"groupIds": [
				"E-6mQ-HEGpZ-48n19HawQ"
			],
			"roundness": null,
			"boundElements": [],
			"updated": 1680694757817,
			"link": null,
			"locked": false,
			"fontSize": 16,
			"fontFamily": 1,
			"text": "NP",
			"rawText": "NP",
			"textAlign": "center",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "NP",
			"lineHeight": 1.25
		},
		{
			"type": "line",
			"version": 157,
			"versionNonce": 227602663,
			"isDeleted": false,
			"id": "BuwVlyhSYDrMQlxF0T1KN",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 463.0345223963941,
			"y": -123.23410766432085,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 96.61994396742693,
			"height": 60.38746497964166,
			"seed": 1494044204,
			"groupIds": [
				"E-6mQ-HEGpZ-48n19HawQ"
			],
			"roundness": null,
			"boundElements": [],
			"updated": 1680694757817,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					-43.99658162802507,
					31.05641056095851
				],
				[
					-96.61994396742693,
					-29.331054418683152
				]
			]
		},
		{
			"type": "line",
			"version": 213,
			"versionNonce": 81802505,
			"isDeleted": false,
			"id": "r1mJ_AJZWwG0hYk6FGbRy",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 419.25015233674367,
			"y": -68.52620174542176,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 116.4615396035947,
			"height": 109.56011503449281,
			"seed": 64420500,
			"groupIds": [
				"E-6mQ-HEGpZ-48n19HawQ"
			],
			"roundness": null,
			"boundElements": [],
			"updated": 1680694757817,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					-43.99658162802507,
					31.05641056095851
				],
				[
					-116.4615396035947,
					-78.5037044735343
				]
			]
		},
		{
			"type": "text",
			"version": 115,
			"versionNonce": 1613773831,
			"isDeleted": false,
			"id": "uEZvKFSY",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 404.04000986551364,
			"y": -87.86430674767348,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 38.60797119140625,
			"height": 20,
			"seed": 85633196,
			"groupIds": [
				"E-6mQ-HEGpZ-48n19HawQ"
			],
			"roundness": null,
			"boundElements": [],
			"updated": 1680694757817,
			"link": null,
			"locked": false,
			"fontSize": 16,
			"fontFamily": 1,
			"text": "S/NP",
			"rawText": "S/NP",
			"textAlign": "center",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "S/NP",
			"lineHeight": 1.25
		},
		{
			"type": "text",
			"version": 106,
			"versionNonce": 654748649,
			"isDeleted": false,
			"id": "H8BtnxdR",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 416.74442894113236,
			"y": -116.33268309521895,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 8.031997680664062,
			"height": 20,
			"seed": 1841465364,
			"groupIds": [
				"E-6mQ-HEGpZ-48n19HawQ"
			],
			"roundness": null,
			"boundElements": [],
			"updated": 1680694757817,
			"link": null,
			"locked": false,
			"fontSize": 16,
			"fontFamily": 1,
			"text": "c",
			"rawText": "c",
			"textAlign": "center",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "c",
			"lineHeight": 1.25
		},
		{
			"type": "text",
			"version": 137,
			"versionNonce": 855188263,
			"isDeleted": false,
			"id": "Wl36slyV",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 277.2263334082663,
			"y": -14.224594030329854,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 38.60797119140625,
			"height": 20,
			"seed": 731427628,
			"groupIds": [
				"E-6mQ-HEGpZ-48n19HawQ"
			],
			"roundness": null,
			"boundElements": [],
			"updated": 1680694757817,
			"link": null,
			"locked": false,
			"fontSize": 16,
			"fontFamily": 1,
			"text": "S/NP",
			"rawText": "S/NP",
			"textAlign": "center",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "S/NP",
			"lineHeight": 1.25
		},
		{
			"type": "text",
			"version": 182,
			"versionNonce": 1861961417,
			"isDeleted": false,
			"id": "pxrpGMRK",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 208.2120877172465,
			"y": -71.67188794290519,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 38.60797119140625,
			"height": 20,
			"seed": 2111858068,
			"groupIds": [
				"E-6mQ-HEGpZ-48n19HawQ"
			],
			"roundness": null,
			"boundElements": [],
			"updated": 1680694757817,
			"link": null,
			"locked": false,
			"fontSize": 16,
			"fontFamily": 1,
			"text": "S/NP",
			"rawText": "S/NP",
			"textAlign": "center",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "S/NP",
			"lineHeight": 1.25
		},
		{
			"type": "text",
			"version": 168,
			"versionNonce": 106440263,
			"isDeleted": false,
			"id": "3OaLRgzh",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 354.6452291696187,
			"y": 29.261446380209463,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 9.727996826171875,
			"height": 20,
			"seed": 452029868,
			"groupIds": [
				"E-6mQ-HEGpZ-48n19HawQ"
			],
			"roundness": null,
			"boundElements": [],
			"updated": 1680694757817,
			"link": null,
			"locked": false,
			"fontSize": 16,
			"fontFamily": 1,
			"text": "S",
			"rawText": "S",
			"textAlign": "center",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "S",
			"lineHeight": 1.25
		},
		{
			"type": "line",
			"version": 274,
			"versionNonce": 1197049257,
			"isDeleted": false,
			"id": "WLuQoz8rV_dklG92GS6ON",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 249.7569831910132,
			"y": -125.77402624140564,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 94.03190975401321,
			"height": 48.30997198371324,
			"seed": 1499894548,
			"groupIds": [
				"E-6mQ-HEGpZ-48n19HawQ"
			],
			"roundness": null,
			"boundElements": [],
			"updated": 1680694757817,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					-25.0176640629943,
					48.30997198371324
				],
				[
					-94.03190975401321,
					3.4507122845507183
				]
			]
		},
		{
			"type": "line",
			"version": 338,
			"versionNonce": 422167911,
			"isDeleted": false,
			"id": "6cQp9NQs7ySdiHXC2l96g",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 373.1324539850266,
			"y": -35.092431826975144,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 140.6165255954511,
			"height": 37.09515705892295,
			"seed": 1146017836,
			"groupIds": [
				"E-6mQ-HEGpZ-48n19HawQ"
			],
			"roundness": null,
			"boundElements": [],
			"updated": 1680694757817,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					-75.91567026012035,
					22.429629849581033
				],
				[
					-140.6165255954511,
					-14.665527209341917
				]
			]
		},
		{
			"type": "line",
			"version": 413,
			"versionNonce": 994896009,
			"isDeleted": false,
			"id": "gKljsd4v2egbot2tz7E2s",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 520.65040414958,
			"y": -122.22291701188647,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 212.21880549988327,
			"height": 150.96866244910393,
			"seed": 248189076,
			"groupIds": [
				"E-6mQ-HEGpZ-48n19HawQ"
			],
			"roundness": null,
			"boundElements": [],
			"updated": 1680694757817,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					-161.3207993027563,
					150.96866244910393
				],
				[
					-212.21880549988327,
					129.40171067066035
				]
			]
		},
		{
			"type": "text",
			"version": 114,
			"versionNonce": 448430215,
			"isDeleted": false,
			"id": "1aIiGnsg",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 217.4657945083145,
			"y": -101.00294236158902,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 8.031997680664062,
			"height": 20,
			"seed": 1671099052,
			"groupIds": [
				"E-6mQ-HEGpZ-48n19HawQ"
			],
			"roundness": null,
			"boundElements": [],
			"updated": 1680694757817,
			"link": null,
			"locked": false,
			"fontSize": 16,
			"fontFamily": 1,
			"text": "c",
			"rawText": "c",
			"textAlign": "center",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "c",
			"lineHeight": 1.25
		},
		{
			"type": "line",
			"version": 443,
			"versionNonce": 1523116329,
			"isDeleted": false,
			"id": "2RbEPd0GBKdH_gMZpBfDO",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -454.0251601166731,
			"y": -133.16919953856564,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 132.85242295521175,
			"height": 106.97208082107954,
			"seed": 1032481044,
			"groupIds": [
				"QzYhUlEHHErbFMpO-U_5R",
				"vkyvaQS_JZaP4L79446yf"
			],
			"roundness": null,
			"boundElements": [],
			"updated": 1680694757817,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					-85.40512904263643,
					103.52136853652861
				],
				[
					13.802849138203776,
					106.97208082107954
				],
				[
					47.44729391257533,
					69.87692376215682
				],
				[
					0,
					0
				]
			]
		},
		{
			"type": "text",
			"version": 386,
			"versionNonce": 1098144231,
			"isDeleted": false,
			"id": "Uk48kb0A",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -454.09437481667806,
			"y": -160.7748978149732,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 10.495986938476562,
			"height": 20,
			"seed": 91972140,
			"groupIds": [
				"QzYhUlEHHErbFMpO-U_5R",
				"vkyvaQS_JZaP4L79446yf"
			],
			"roundness": null,
			"boundElements": [],
			"updated": 1680694757817,
			"link": null,
			"locked": false,
			"fontSize": 16,
			"fontFamily": 1,
			"text": "A",
			"rawText": "A",
			"textAlign": "center",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "A",
			"lineHeight": 1.25
		},
		{
			"type": "text",
			"version": 430,
			"versionNonce": 33807369,
			"isDeleted": false,
			"id": "SzNqEhAK",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -397.72365977646473,
			"y": -65.52817313616947,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 11.631988525390625,
			"height": 20,
			"seed": 738469524,
			"groupIds": [
				"QzYhUlEHHErbFMpO-U_5R",
				"vkyvaQS_JZaP4L79446yf"
			],
			"roundness": null,
			"boundElements": [],
			"updated": 1680694757817,
			"link": null,
			"locked": false,
			"fontSize": 16,
			"fontFamily": 1,
			"text": "B",
			"rawText": "B",
			"textAlign": "center",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "B",
			"lineHeight": 1.25
		},
		{
			"type": "text",
			"version": 390,
			"versionNonce": 1975420167,
			"isDeleted": false,
			"id": "8HAx6YHr",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -566.0245805507695,
			"y": -140.93330217880543,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 37.65998840332031,
			"height": 25,
			"seed": 581319852,
			"groupIds": [
				"QzYhUlEHHErbFMpO-U_5R",
				"vkyvaQS_JZaP4L79446yf"
			],
			"roundness": null,
			"boundElements": [],
			"updated": 1680694757817,
			"link": null,
			"locked": false,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "A/B",
			"rawText": "A/B",
			"textAlign": "center",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "A/B",
			"lineHeight": 1.25
		},
		{
			"type": "text",
			"version": 398,
			"versionNonce": 790459113,
			"isDeleted": false,
			"id": "kU1Ic3oK",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -572.5842161361236,
			"y": 10.986072554849443,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 14.539993286132812,
			"height": 25,
			"seed": 1869231124,
			"groupIds": [
				"QzYhUlEHHErbFMpO-U_5R",
				"vkyvaQS_JZaP4L79446yf"
			],
			"roundness": null,
			"boundElements": [],
			"updated": 1680694757817,
			"link": null,
			"locked": false,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "B",
			"rawText": "B",
			"textAlign": "center",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "B",
			"lineHeight": 1.25
		},
		{
			"type": "line",
			"version": 508,
			"versionNonce": 1909869607,
			"isDeleted": false,
			"id": "JvoTcsAT8LstLy2D8D406",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -499.31575565551816,
			"y": 33.32766819101812,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 61.25014944255099,
			"height": 49.99933590218872,
			"seed": 982632236,
			"groupIds": [
				"QzYhUlEHHErbFMpO-U_5R",
				"vkyvaQS_JZaP4L79446yf"
			],
			"roundness": null,
			"boundElements": [],
			"updated": 1680694757817,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					-30.625074721275496,
					49.99933590218872
				],
				[
					30.625074721275496,
					49.314413492569734
				],
				[
					0,
					0
				]
			]
		},
		{
			"type": "text",
			"version": 473,
			"versionNonce": 1990274505,
			"isDeleted": false,
			"id": "hsH7nFR8",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -504.69574059754336,
			"y": 6.936784839401071,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 11.631988525390625,
			"height": 20,
			"seed": 1169330580,
			"groupIds": [
				"QzYhUlEHHErbFMpO-U_5R",
				"vkyvaQS_JZaP4L79446yf"
			],
			"roundness": null,
			"boundElements": [],
			"updated": 1680694757817,
			"link": null,
			"locked": false,
			"fontSize": 16,
			"fontFamily": 1,
			"text": "B",
			"rawText": "B",
			"textAlign": "center",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "B",
			"lineHeight": 1.25
		},
		{
			"type": "line",
			"version": 687,
			"versionNonce": 564670279,
			"isDeleted": false,
			"id": "unjkb7nM9j8UChAIby3P8",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -318.9398706628374,
			"y": -94.31563757390518,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 47.468983073586614,
			"height": 40.51437512818512,
			"seed": 296223148,
			"groupIds": [
				"QzYhUlEHHErbFMpO-U_5R",
				"vkyvaQS_JZaP4L79446yf"
			],
			"roundness": null,
			"boundElements": [],
			"updated": 1680694757817,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					-23.734491536793307,
					40.51437512818512
				],
				[
					23.734491536793307,
					39.959383688073025
				],
				[
					0,
					0
				]
			]
		},
		{
			"type": "text",
			"version": 551,
			"versionNonce": 1336577193,
			"isDeleted": false,
			"id": "GREGvtxM",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -322.60534084720734,
			"y": -118.11848671210896,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 11.631988525390625,
			"height": 20,
			"seed": 1041448724,
			"groupIds": [
				"QzYhUlEHHErbFMpO-U_5R",
				"vkyvaQS_JZaP4L79446yf"
			],
			"roundness": null,
			"boundElements": [],
			"updated": 1680694757817,
			"link": null,
			"locked": false,
			"fontSize": 16,
			"fontFamily": 1,
			"text": "B",
			"rawText": "B",
			"textAlign": "center",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "B",
			"lineHeight": 1.25
		},
		{
			"type": "line",
			"version": 520,
			"versionNonce": 1066446439,
			"isDeleted": false,
			"id": "FZT4dgsPdCm_P9cVx4Btl",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -388.53049504162914,
			"y": 78.67871003261394,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 87.15615221724036,
			"height": 73.32652862087382,
			"seed": 1326640172,
			"groupIds": [
				"j_ynwlNXcOhHmlwZnUNdO",
				"vkyvaQS_JZaP4L79446yf"
			],
			"roundness": null,
			"boundElements": [],
			"updated": 1680694757817,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					-56.02895499679758,
					70.9611567298779
				],
				[
					9.055184645947023,
					73.32652862087382
				],
				[
					31.127197220442795,
					47.89878079266759
				],
				[
					0,
					0
				]
			]
		},
		{
			"type": "text",
			"version": 415,
			"versionNonce": 1105530761,
			"isDeleted": false,
			"id": "Obwg4czu",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -394.5676695801128,
			"y": 51.93568982734428,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 10.495986938476562,
			"height": 20,
			"seed": 285359252,
			"groupIds": [
				"j_ynwlNXcOhHmlwZnUNdO",
				"vkyvaQS_JZaP4L79446yf"
			],
			"roundness": null,
			"boundElements": [],
			"updated": 1680694757817,
			"link": null,
			"locked": false,
			"fontSize": 16,
			"fontFamily": 1,
			"text": "A",
			"rawText": "A",
			"textAlign": "center",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "A",
			"lineHeight": 1.25
		},
		{
			"type": "text",
			"version": 479,
			"versionNonce": 106711431,
			"isDeleted": false,
			"id": "QA1yILmk",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -349.41176946469,
			"y": 134.24224343908213,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 11.631988525390625,
			"height": 20,
			"seed": 616335020,
			"groupIds": [
				"j_ynwlNXcOhHmlwZnUNdO",
				"vkyvaQS_JZaP4L79446yf"
			],
			"roundness": null,
			"boundElements": [],
			"updated": 1680694757817,
			"link": null,
			"locked": false,
			"fontSize": 16,
			"fontFamily": 1,
			"text": "B",
			"rawText": "B",
			"textAlign": "center",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "B",
			"lineHeight": 1.25
		},
		{
			"type": "text",
			"version": 330,
			"versionNonce": 1401697897,
			"isDeleted": false,
			"id": "FPjdE886",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -400.93019592849373,
			"y": 31.514277835328585,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 30.127975463867188,
			"height": 20,
			"seed": 2016774676,
			"groupIds": [
				"j_ynwlNXcOhHmlwZnUNdO",
				"vkyvaQS_JZaP4L79446yf"
			],
			"roundness": null,
			"boundElements": [],
			"updated": 1680694757817,
			"link": null,
			"locked": false,
			"fontSize": 16,
			"fontFamily": 1,
			"text": "A/B",
			"rawText": "A/B",
			"textAlign": "center",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "A/B",
			"lineHeight": 1.25
		},
		{
			"type": "line",
			"version": 571,
			"versionNonce": 591888551,
			"isDeleted": false,
			"id": "wfDEinV86pxre1w-DSMod",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -246.1570745687079,
			"y": 84.69165099289353,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 87.15615221724038,
			"height": 73.32652862087382,
			"seed": 2001654060,
			"groupIds": [
				"j_ynwlNXcOhHmlwZnUNdO",
				"vkyvaQS_JZaP4L79446yf"
			],
			"roundness": null,
			"boundElements": [],
			"updated": 1680694757817,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					-56.02895499679758,
					70.9611567298779
				],
				[
					9.055184645947023,
					73.32652862087382
				],
				[
					31.127197220442795,
					47.89878079266759
				],
				[
					0,
					0
				]
			]
		},
		{
			"type": "text",
			"version": 440,
			"versionNonce": 929792329,
			"isDeleted": false,
			"id": "U9EVbe5V",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -253.6229648332062,
			"y": 57.08595271648642,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 11.631988525390625,
			"height": 20,
			"seed": 111355796,
			"groupIds": [
				"j_ynwlNXcOhHmlwZnUNdO",
				"vkyvaQS_JZaP4L79446yf"
			],
			"roundness": null,
			"boundElements": [],
			"updated": 1680694757817,
			"link": null,
			"locked": false,
			"fontSize": 16,
			"fontFamily": 1,
			"text": "B",
			"rawText": "B",
			"textAlign": "center",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "B",
			"lineHeight": 1.25
		},
		{
			"type": "text",
			"version": 517,
			"versionNonce": 1744753607,
			"isDeleted": false,
			"id": "CRbWSpRn",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -213.2793960584297,
			"y": 134.2164379013982,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 10.303985595703125,
			"height": 20,
			"seed": 104057772,
			"groupIds": [
				"j_ynwlNXcOhHmlwZnUNdO",
				"vkyvaQS_JZaP4L79446yf"
			],
			"roundness": null,
			"boundElements": [],
			"updated": 1680694757817,
			"link": null,
			"locked": false,
			"fontSize": 16,
			"fontFamily": 1,
			"text": "C",
			"rawText": "C",
			"textAlign": "center",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "C",
			"lineHeight": 1.25
		},
		{
			"type": "text",
			"version": 358,
			"versionNonce": 987656233,
			"isDeleted": false,
			"id": "mQuxrQID",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -259.3251141559829,
			"y": 36.66454072447118,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 29.93597412109375,
			"height": 20,
			"seed": 8459540,
			"groupIds": [
				"j_ynwlNXcOhHmlwZnUNdO",
				"vkyvaQS_JZaP4L79446yf"
			],
			"roundness": null,
			"boundElements": [],
			"updated": 1680694757817,
			"link": null,
			"locked": false,
			"fontSize": 16,
			"fontFamily": 1,
			"text": "B/C",
			"rawText": "B/C",
			"textAlign": "center",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "B/C",
			"lineHeight": 1.25
		},
		{
			"type": "line",
			"version": 911,
			"versionNonce": 1017037543,
			"isDeleted": false,
			"id": "XLzvnoqIbzviWyWJqNApC",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -97.34837865711256,
			"y": 64.73405832074354,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 96.64874424906398,
			"height": 97.48151461274006,
			"seed": 328562220,
			"groupIds": [
				"j_ynwlNXcOhHmlwZnUNdO",
				"vkyvaQS_JZaP4L79446yf"
			],
			"roundness": null,
			"boundElements": [],
			"updated": 1680694757817,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					-69.89543822892384,
					96.00664911775695
				],
				[
					-11.133397408543033,
					97.48151461274006
				],
				[
					26.753306020140144,
					51.00163901628207
				],
				[
					0,
					0
				]
			]
		},
		{
			"type": "line",
			"version": 1018,
			"versionNonce": 1943044873,
			"isDeleted": false,
			"id": "pa8MSXpPhOiK-kBtFs6Hw",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -70.23015435018465,
			"y": 115.16795026259865,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 59.55045394084618,
			"height": 49.15035966951742,
			"seed": 860181140,
			"groupIds": [
				"j_ynwlNXcOhHmlwZnUNdO",
				"vkyvaQS_JZaP4L79446yf"
			],
			"roundness": null,
			"boundElements": [],
			"updated": 1680694757817,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					-37.85109564068944,
					46.70218612516942
				],
				[
					5.559657916143593,
					49.15035966951742
				],
				[
					21.699358300156746,
					25.204858763405355
				],
				[
					0,
					0
				]
			]
		},
		{
			"type": "text",
			"version": 471,
			"versionNonce": 1370738183,
			"isDeleted": false,
			"id": "9Zydwrwy",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -89.18155072541481,
			"y": 50.933366467424776,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 10.495986938476562,
			"height": 20,
			"seed": 207474860,
			"groupIds": [
				"j_ynwlNXcOhHmlwZnUNdO",
				"vkyvaQS_JZaP4L79446yf"
			],
			"roundness": null,
			"boundElements": [],
			"updated": 1680694757817,
			"link": null,
			"locked": false,
			"fontSize": 16,
			"fontFamily": 1,
			"text": "A",
			"rawText": "A",
			"textAlign": "center",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "A",
			"lineHeight": 1.25
		},
		{
			"type": "text",
			"version": 518,
			"versionNonce": 784153065,
			"isDeleted": false,
			"id": "GzZgUre8",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -64.72992431729722,
			"y": 94.92994809544984,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 11.631988525390625,
			"height": 20,
			"seed": 1302716436,
			"groupIds": [
				"j_ynwlNXcOhHmlwZnUNdO",
				"vkyvaQS_JZaP4L79446yf"
			],
			"roundness": null,
			"boundElements": [],
			"updated": 1680694757817,
			"link": null,
			"locked": false,
			"fontSize": 16,
			"fontFamily": 1,
			"text": "B",
			"rawText": "B",
			"textAlign": "center",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "B",
			"lineHeight": 1.25
		},
		{
			"type": "text",
			"version": 547,
			"versionNonce": 305820967,
			"isDeleted": false,
			"id": "W2W9Xc8m",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -41.639916965274324,
			"y": 134.61313936778538,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 10.303985595703125,
			"height": 20,
			"seed": 808901420,
			"groupIds": [
				"j_ynwlNXcOhHmlwZnUNdO",
				"vkyvaQS_JZaP4L79446yf"
			],
			"roundness": null,
			"boundElements": [],
			"updated": 1680694757818,
			"link": null,
			"locked": false,
			"fontSize": 16,
			"fontFamily": 1,
			"text": "C",
			"rawText": "C",
			"textAlign": "center",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "C",
			"lineHeight": 1.25
		},
		{
			"type": "line",
			"version": 260,
			"versionNonce": 481301705,
			"isDeleted": false,
			"id": "3mpZDXwCHddP-QDfX1O90",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -127.93284922660882,
			"y": -135.71938847288834,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 87.13048518491178,
			"height": 84.54245097149806,
			"seed": 932756884,
			"groupIds": [
				"PEO9KnTQq6RBRRt3cLlzM",
				"vkyvaQS_JZaP4L79446yf"
			],
			"roundness": null,
			"boundElements": [],
			"updated": 1680694757818,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					50.89800619712696,
					84.54245097149806
				],
				[
					-9.489458782514703,
					83.67977290036038
				],
				[
					-36.232478987784816,
					57.7994307662284
				],
				[
					0,
					0
				]
			]
		},
		{
			"type": "text",
			"version": 135,
			"versionNonce": 1436497991,
			"isDeleted": false,
			"id": "j0d8bZSA",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -123.68867357092472,
			"y": -153.92366218019401,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 10.495986938476562,
			"height": 20,
			"seed": 965383596,
			"groupIds": [
				"PEO9KnTQq6RBRRt3cLlzM",
				"vkyvaQS_JZaP4L79446yf"
			],
			"roundness": null,
			"boundElements": [],
			"updated": 1680694757818,
			"link": null,
			"locked": false,
			"fontSize": 16,
			"fontFamily": 1,
			"text": "A",
			"rawText": "A",
			"textAlign": "center",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "A",
			"lineHeight": 1.25
		},
		{
			"type": "text",
			"version": 179,
			"versionNonce": 1095013289,
			"isDeleted": false,
			"id": "gJlixl8r",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -181.19146392089237,
			"y": -81.01853313755805,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 11.631988525390625,
			"height": 20,
			"seed": 1783884564,
			"groupIds": [
				"PEO9KnTQq6RBRRt3cLlzM",
				"vkyvaQS_JZaP4L79446yf"
			],
			"roundness": null,
			"boundElements": [],
			"updated": 1680694757818,
			"link": null,
			"locked": false,
			"fontSize": 16,
			"fontFamily": 1,
			"text": "B",
			"rawText": "B",
			"textAlign": "center",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "B",
			"lineHeight": 1.25
		},
		{
			"type": "text",
			"version": 168,
			"versionNonce": 1371445095,
			"isDeleted": false,
			"id": "NHr3AZZC",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -416.0692465553823,
			"y": 190.43285309132511,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 173.9799346923828,
			"height": 25,
			"seed": 92471340,
			"groupIds": [
				"vkyvaQS_JZaP4L79446yf"
			],
			"roundness": null,
			"boundElements": [],
			"updated": 1680694757818,
			"link": null,
			"locked": false,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "3/4 * 4/2 = 3/2",
			"rawText": "3/4 * 4/2 = 3/2",
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "3/4 * 4/2 = 3/2",
			"lineHeight": 1.25
		},
		{
			"type": "text",
			"version": 86,
			"versionNonce": 90326092,
			"isDeleted": false,
			"id": "hm5Vq3EN",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 241.73697450456507,
			"y": 96.377994051075,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 20.879974365234375,
			"height": 20,
			"seed": 1841552916,
			"groupIds": [
				"26VEgMRc9JphcxEU73M7Z"
			],
			"roundness": null,
			"boundElements": [
				{
					"id": "HqLWDozuLU4ow6tpgw1aA",
					"type": "arrow"
				}
			],
			"updated": 1680694807880,
			"link": null,
			"locked": false,
			"customData": {
				"legacyTextWrap": true
			},
			"fontSize": 16,
			"fontFamily": 1,
			"text": "NP",
			"rawText": "NP",
			"textAlign": "center",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "NP",
			"lineHeight": 1.25
		},
		{
			"type": "text",
			"version": 97,
			"versionNonce": 1672207052,
			"isDeleted": false,
			"id": "uVFRNQip",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 315.09190218502295,
			"y": 95.5153159799371,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 8.751998901367188,
			"height": 20,
			"seed": 662682924,
			"groupIds": [
				"26VEgMRc9JphcxEU73M7Z"
			],
			"roundness": null,
			"boundElements": [
				{
					"id": "HqLWDozuLU4ow6tpgw1aA",
					"type": "arrow"
				}
			],
			"updated": 1680694807880,
			"link": null,
			"locked": false,
			"customData": {
				"legacyTextWrap": true
			},
			"fontSize": 16,
			"fontFamily": 1,
			"text": "e",
			"rawText": "e",
			"textAlign": "center",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "e",
			"lineHeight": 1.25
		},
		{
			"type": "arrow",
			"version": 237,
			"versionNonce": 738981492,
			"isDeleted": false,
			"id": "HqLWDozuLU4ow6tpgw1aA",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 265.9832202110672,
			"y": 108.45548704700298,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 36.23247898778527,
			"height": 0.8626780711376796,
			"seed": 1598487444,
			"groupIds": [
				"26VEgMRc9JphcxEU73M7Z"
			],
			"roundness": null,
			"boundElements": [],
			"updated": 1680694807880,
			"link": null,
			"locked": false,
			"startBinding": {
				"elementId": "hm5Vq3EN",
				"focus": 0.23478526011102907,
				"gap": 3.3662713412677476
			},
			"endBinding": {
				"elementId": "uVFRNQip",
				"focus": -0.16495396413261695,
				"gap": 12.876202986170483
			},
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": "arrow",
			"points": [
				[
					0,
					0
				],
				[
					36.23247898778527,
					-0.8626780711376796
				]
			]
		},
		{
			"type": "text",
			"version": 77,
			"versionNonce": 834840396,
			"isDeleted": false,
			"id": "XalOFMJY",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 221.21704847368323,
			"y": 145.5506441059257,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 65.34397888183594,
			"height": 40,
			"seed": 1499936684,
			"groupIds": [
				"26VEgMRc9JphcxEU73M7Z"
			],
			"roundness": null,
			"boundElements": [],
			"updated": 1680694807702,
			"link": null,
			"locked": false,
			"fontSize": 16,
			"fontFamily": 1,
			"text": "noone\neveryone",
			"rawText": "noone\neveryone",
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "noone\neveryone",
			"lineHeight": 1.25
		},
		{
			"type": "freedraw",
			"version": 569,
			"versionNonce": 1050100596,
			"isDeleted": false,
			"id": "3ni76sxkn-wGXb9-dB0jy",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 342.70310527984816,
			"y": 156.6032199493185,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 36.48223875149825,
			"height": 30.401865626248764,
			"seed": 1213032596,
			"groupIds": [
				"26VEgMRc9JphcxEU73M7Z"
			],
			"roundness": null,
			"boundElements": [],
			"updated": 1680694807702,
			"link": null,
			"locked": false,
			"points": [
				[
					-10.822371900349504,
					-19.39873627056268
				],
				[
					-13.176064723026705,
					-19.616670791180987
				],
				[
					-13.568346860139531,
					-19.725638051490105
				],
				[
					-14.156770065808894,
					-19.943572572108344
				],
				[
					-14.941334340034793,
					-20.16150709272665
				],
				[
					-15.529757545704157,
					-20.379441613344888
				],
				[
					-15.922039682816983,
					-20.379441613344888
				],
				[
					-16.70660395704263,
					-20.597376133963195
				],
				[
					-17.687309299824822,
					-20.924277914890624
				],
				[
					-18.275732505494183,
					-21.251179695817978
				],
				[
					-19.060296779720083,
					-21.687048737054525
				],
				[
					-19.648719985389448,
					-22.23188503860019
				],
				[
					-19.648719985389448,
					-22.449819559218497
				],
				[
					-19.844861053945735,
					-22.776721340145922
				],
				[
					-20.04100212250227,
					-23.103623121073277
				],
				[
					-20.04100212250227,
					-23.648459422619013
				],
				[
					-19.844861053945735,
					-24.193295724164678
				],
				[
					-19.45257891683291,
					-24.847099286019457
				],
				[
					-18.66801464260701,
					-25.391935587565122
				],
				[
					-18.275732505494183,
					-25.60987010818343
				],
				[
					-17.098886094155457,
					-26.154706409729094
				],
				[
					-15.922039682816983,
					-26.69954271127476
				],
				[
					-14.352911134365431,
					-27.244379012820424
				],
				[
					-12.587641517357342,
					-27.680248054056968
				],
				[
					-11.803077243131693,
					-27.898182574675275
				],
				[
					-10.23394869468014,
					-27.898182574675275
				],
				[
					-8.664820146228589,
					-27.898182574675275
				],
				[
					-7.291832666333576,
					-27.789215314366157
				],
				[
					-6.311127323551388,
					-27.46231353343873
				],
				[
					-5.134280912212663,
					-27.135411752511303
				],
				[
					-4.349716637986763,
					-26.808509971583877
				],
				[
					-3.957434500873937,
					-26.69954271127476
				],
				[
					-3.565152363761112,
					-26.37264093034733
				],
				[
					-3.1728702266482864,
					-26.045739149419976
				],
				[
					-2.780588089535211,
					-25.50090284787431
				],
				[
					-2.5844470209789243,
					-25.065033806637764
				],
				[
					-2.388305952422387,
					-24.52019750509203
				],
				[
					-2.1921648838660985,
					-24.411230244782914
				],
				[
					-1.9960238153095613,
					-24.084328463855556
				],
				[
					-1.9960238153095613,
					-23.75742668292813
				],
				[
					-1.9960238153095613,
					-23.648459422619013
				],
				[
					-1.9960238153095613,
					-23.430524902000705
				],
				[
					-1.9960238153095613,
					-23.321557641691587
				],
				[
					-1.9960238153095613,
					-23.212590381382466
				],
				[
					-1.799882746753024,
					-23.212590381382466
				],
				[
					-1.6037416781967355,
					-23.321557641691587
				],
				[
					-1.4076006096401983,
					-23.430524902000705
				],
				[
					-1.0153184725273725,
					-23.539492162309823
				],
				[
					-0.623036335414298,
					-23.648459422619013
				],
				[
					-0.4268952668580095,
					-23.75742668292813
				],
				[
					-0.03461312974518371,
					-23.75742668292813
				],
				[
					0.16152793881135352,
					-23.86639394323725
				],
				[
					0.7499511444807165,
					-23.97536120354637
				],
				[
					1.1422332815935405,
					-24.193295724164678
				],
				[
					1.3383743501500778,
					-24.302262984473796
				],
				[
					1.5345154187063663,
					-24.411230244782914
				],
				[
					1.926797555819192,
					-24.52019750509203
				],
				[
					2.3190796929322666,
					-24.52019750509203
				],
				[
					2.7113618300450923,
					-24.62916476540122
				],
				[
					2.9075028986016296,
					-24.73813202571034
				],
				[
					3.2997850357144554,
					-24.847099286019457
				],
				[
					3.8882082413838184,
					-24.95606654632858
				],
				[
					4.280490378496642,
					-25.065033806637764
				],
				[
					4.672772515609468,
					-25.174001066946886
				],
				[
					5.457336789835368,
					-25.282968327256004
				],
				[
					5.849618926948194,
					-25.282968327256004
				],
				[
					6.438042132617555,
					-25.282968327256004
				],
				[
					7.02646533828692,
					-25.282968327256004
				],
				[
					7.222606406843207,
					-25.282968327256004
				],
				[
					8.007170681069107,
					-25.174001066946886
				],
				[
					8.203311749625394,
					-25.174001066946886
				],
				[
					8.595593886738472,
					-25.065033806637764
				],
				[
					9.184017092407585,
					-24.95606654632858
				],
				[
					9.772440298076946,
					-24.847099286019457
				],
				[
					10.164722435189772,
					-24.62916476540122
				],
				[
					10.36086350374631,
					-24.52019750509203
				],
				[
					10.753145640859136,
					-24.411230244782914
				],
				[
					11.145427777971959,
					-24.193295724164678
				],
				[
					11.537709915085037,
					-23.97536120354637
				],
				[
					11.537709915085037,
					-23.86639394323725
				],
				[
					11.733850983641323,
					-23.75742668292813
				],
				[
					11.929992052197859,
					-23.539492162309823
				],
				[
					11.929992052197859,
					-23.212590381382466
				],
				[
					11.733850983641323,
					-22.667754079836733
				],
				[
					11.341568846528498,
					-22.122917778291068
				],
				[
					10.753145640859136,
					-21.687048737054525
				],
				[
					10.36086350374631,
					-21.251179695817978
				],
				[
					10.164722435189772,
					-21.14221243550886
				],
				[
					8.987876023851294,
					-20.27047435303577
				],
				[
					8.595593886738472,
					-19.943572572108344
				],
				[
					8.203311749625394,
					-19.50770353087187
				],
				[
					7.418747475399746,
					-18.962867229326136
				],
				[
					6.830324269730381,
					-18.635965448398778
				],
				[
					6.438042132617555,
					-18.30906366747135
				],
				[
					5.653477858391655,
					-17.873194626234806
				],
				[
					4.868913584166005,
					-17.43732558499826
				],
				[
					4.280490378496642,
					-17.110423804070834
				],
				[
					3.6920671728272794,
					-16.783522023143476
				],
				[
					3.2997850357144554,
					-16.56558750252517
				],
				[
					2.7113618300450923,
					-16.347652981906933
				],
				[
					2.515220761488555,
					-16.347652981906933
				],
				[
					2.1229386243757293,
					-16.347652981906933
				],
				[
					1.7306564872629036,
					-16.238685721597744
				],
				[
					1.3383743501500778,
					-16.129718461288626
				],
				[
					1.1422332815935405,
					-16.129718461288626
				],
				[
					1.1422332815935405,
					-16.020751200979507
				],
				[
					1.1422332815935405,
					-15.911783940670388
				],
				[
					1.7306564872629036,
					-15.69384942005208
				],
				[
					2.3190796929322666,
					-15.58488215974296
				],
				[
					3.4959261042709926,
					-15.040045858197297
				],
				[
					4.868913584166005,
					-14.277275036033327
				],
				[
					6.24190106406102,
					-13.514504213869424
				],
				[
					7.222606406843207,
					-12.642766131396334
				],
				[
					8.399452818181933,
					-12.206897090159789
				],
				[
					8.987876023851294,
					-11.771028048923242
				],
				[
					9.38015816096412,
					-11.444126267995816
				],
				[
					10.164722435189772,
					-11.008257226759271
				],
				[
					10.753145640859136,
					-10.463420925213608
				],
				[
					11.341568846528498,
					-9.809617363358825
				],
				[
					11.733850983641323,
					-9.155813801503973
				],
				[
					12.322274189310685,
					-8.284075719030882
				],
				[
					12.518415257867224,
					-7.957173938103455
				],
				[
					12.518415257867224,
					-7.194403115939554
				],
				[
					12.518415257867224,
					-6.976468595321247
				],
				[
					12.518415257867224,
					-6.431632293775582
				],
				[
					12.518415257867224,
					-5.886795992229919
				],
				[
					12.126133120754398,
					-5.450926950993374
				],
				[
					11.537709915085037,
					-5.015057909756829
				],
				[
					11.537709915085037,
					-4.906090649447709
				],
				[
					10.949286709415672,
					-4.797123389138591
				],
				[
					10.36086350374631,
					-4.797123389138591
				],
				[
					9.772440298076946,
					-4.688156128829402
				],
				[
					8.791734955294759,
					-4.688156128829402
				],
				[
					8.007170681069107,
					-4.797123389138591
				],
				[
					7.222606406843207,
					-5.015057909756829
				],
				[
					6.438042132617555,
					-5.124025170065947
				],
				[
					5.457336789835368,
					-5.450926950993374
				],
				[
					4.47663144705318,
					-5.668861471611612
				],
				[
					3.4959261042709926,
					-5.995763252539037
				],
				[
					2.7113618300450923,
					-6.213697773157344
				],
				[
					1.1422332815935405,
					-6.540599554084702
				],
				[
					0.16152793881135352,
					-6.867501335012127
				],
				[
					-0.23075419830147226,
					-6.976468595321247
				],
				[
					-1.6037416781967355,
					-7.194403115939554
				],
				[
					-2.780588089535211,
					-7.194403115939554
				],
				[
					-3.7612934323174,
					-7.194403115939554
				],
				[
					-4.741998775099588,
					-6.976468595321247
				],
				[
					-5.330421980768951,
					-6.758534074703009
				],
				[
					-5.918845186438314,
					-6.540599554084702
				],
				[
					-6.507268392107676,
					-6.431632293775582
				],
				[
					-7.291832666333576,
					-6.213697773157344
				],
				[
					-7.8802558720026905,
					-5.995763252539037
				],
				[
					-8.468679077672054,
					-5.668861471611612
				],
				[
					-8.860961214784878,
					-5.559894211302492
				],
				[
					-9.253243351897954,
					-5.341959690684254
				],
				[
					-9.253243351897954,
					-5.124025170065947
				],
				[
					-9.44938442045424,
					-4.797123389138591
				],
				[
					-9.44938442045424,
					-4.470221608211164
				],
				[
					-9.44938442045424,
					-4.0343525669746185
				],
				[
					-9.253243351897954,
					-3.7074507860471915
				],
				[
					-8.860961214784878,
					-3.2715817448106463
				],
				[
					-8.468679077672054,
					-3.05364722419241
				],
				[
					-7.684114803446402,
					-2.508810922646745
				],
				[
					-6.899550529220502,
					-1.9639746211010127
				],
				[
					-6.114986254994851,
					-1.3101710592462297
				],
				[
					-5.134280912212663,
					-0.6563674973914466
				],
				[
					-3.957434500873937,
					-0.00256393553659251
				],
				[
					-3.565152363761112,
					0.21537058508164364
				],
				[
					-2.780588089535211,
					0.8691741469364977
				],
				[
					-1.9960238153095613,
					1.1960759278638555
				],
				[
					-1.6037416781967355,
					1.5229777087912808
				],
				[
					-1.0153184725273725,
					1.740912229409588
				],
				[
					-0.623036335414298,
					1.849879489718706
				],
				[
					-0.23075419830147226,
					2.067814010336946
				],
				[
					-0.23075419830147226,
					2.176781270646064
				],
				[
					-0.03461312974518371,
					2.176781270646064
				],
				[
					0.16152793881135352,
					2.285748530955253
				],
				[
					-0.03461312974518371,
					2.285748530955253
				],
				[
					-0.23075419830147226,
					2.285748530955253
				],
				[
					-0.4268952668580095,
					2.285748530955253
				],
				[
					-0.623036335414298,
					2.285748530955253
				],
				[
					-0.8191774039708353,
					2.285748530955253
				],
				[
					-1.0153184725273725,
					2.285748530955253
				],
				[
					-1.211459541083661,
					2.285748530955253
				],
				[
					-1.4076006096401983,
					2.285748530955253
				],
				[
					-1.6037416781967355,
					2.285748530955253
				],
				[
					-1.799882746753024,
					2.285748530955253
				],
				[
					-1.9960238153095613,
					2.285748530955253
				],
				[
					-2.388305952422387,
					2.394715791264371
				],
				[
					-2.780588089535211,
					2.394715791264371
				],
				[
					-3.1728702266482864,
					2.503683051573489
				],
				[
					-3.7612934323174,
					2.503683051573489
				],
				[
					-3.957434500873937,
					2.503683051573489
				],
				[
					-4.5458577065433,
					2.503683051573489
				],
				[
					-4.741998775099588,
					2.503683051573489
				],
				[
					-5.134280912212663,
					2.503683051573489
				],
				[
					-5.722704117882025,
					2.503683051573489
				],
				[
					-6.114986254994851,
					2.503683051573489
				],
				[
					-6.311127323551388,
					2.503683051573489
				],
				[
					-6.899550529220502,
					2.503683051573489
				],
				[
					-7.487973734889865,
					2.503683051573489
				],
				[
					-7.8802558720026905,
					2.503683051573489
				],
				[
					-8.664820146228589,
					2.503683051573489
				],
				[
					-9.057102283341415,
					2.503683051573489
				],
				[
					-9.44938442045424,
					2.503683051573489
				],
				[
					-9.645525489010778,
					2.503683051573489
				],
				[
					-10.037807626123604,
					2.503683051573489
				],
				[
					-10.626230831792967,
					2.503683051573489
				],
				[
					-11.018512968905792,
					2.503683051573489
				],
				[
					-11.606936174575155,
					2.503683051573489
				],
				[
					-11.99921831168798,
					2.503683051573489
				],
				[
					-13.372205791583243,
					2.285748530955253
				],
				[
					-13.568346860139531,
					2.176781270646064
				],
				[
					-13.960628997252606,
					2.067814010336946
				],
				[
					-14.352911134365431,
					1.9588467500278242
				],
				[
					-14.549052202921718,
					1.849879489718706
				],
				[
					-14.745193271478257,
					1.849879489718706
				],
				[
					-15.137475408591083,
					1.740912229409588
				],
				[
					-15.137475408591083,
					1.5229777087912808
				],
				[
					-15.529757545704157,
					1.305043188173041
				],
				[
					-15.922039682816983,
					1.087108667554734
				],
				[
					-15.922039682816983,
					0.9781414072456158
				],
				[
					-16.31432181992981,
					0.8691741469364977
				],
				[
					-16.510462888486344,
					0.6512396263181905
				],
				[
					-16.70660395704263,
					0.43330510569995084
				],
				[
					-17.098886094155457,
					0.32433784539083277
				],
				[
					-17.295027162711996,
					0.21537058508164364
				],
				[
					-17.49116823126853,
					-0.00256393553659251
				],
				[
					-17.883450368381357,
					-0.3294657164640178
				],
				[
					-18.079591436937648,
					-0.6563674973914466
				],
				[
					-18.275732505494183,
					-1.09223653862799
				],
				[
					-18.471873574050722,
					-1.4191383195553477
				],
				[
					-18.471873574050722,
					-1.746040100482773
				],
				[
					-18.471873574050722,
					-2.0729418814101983
				],
				[
					-18.66801464260701,
					-2.290876402028438
				],
				[
					-18.66801464260701,
					-2.508810922646745
				],
				[
					-18.66801464260701,
					-2.9446799638832886
				],
				[
					-18.66801464260701,
					-3.3805490051198355
				],
				[
					-18.471873574050722,
					-3.8164180463563806
				],
				[
					-18.275732505494183,
					-4.143319827283737
				],
				[
					-18.079591436937648,
					-4.470221608211164
				],
				[
					-18.079591436937648,
					-4.688156128829402
				],
				[
					-18.079591436937648,
					-4.797123389138591
				],
				[
					-17.883450368381357,
					-5.124025170065947
				],
				[
					-17.687309299824822,
					-5.450926950993374
				],
				[
					-17.49116823126853,
					-5.668861471611612
				],
				[
					-17.098886094155457,
					-6.104730512848226
				],
				[
					-16.90274502559917,
					-6.431632293775582
				],
				[
					-16.90274502559917,
					-6.540599554084702
				],
				[
					-16.70660395704263,
					-6.867501335012127
				],
				[
					-16.70660395704263,
					-7.194403115939554
				],
				[
					-16.70660395704263,
					-7.303370376248672
				],
				[
					-16.70660395704263,
					-7.630272157176099
				],
				[
					-16.70660395704263,
					-7.848206677794337
				],
				[
					-16.70660395704263,
					-7.957173938103455
				],
				[
					-16.70660395704263,
					-8.066141198412645
				],
				[
					-16.70660395704263,
					-8.39304297934007
				],
				[
					-16.70660395704263,
					-8.719944760267428
				],
				[
					-16.70660395704263,
					-8.828912020576546
				],
				[
					-16.70660395704263,
					-9.046846541194853
				],
				[
					-16.90274502559917,
					-9.155813801503973
				],
				[
					-17.098886094155457,
					-9.37374832212228
				],
				[
					-17.295027162711996,
					-9.700650103049636
				],
				[
					-17.49116823126853,
					-9.809617363358825
				],
				[
					-17.883450368381357,
					-9.918584623667943
				],
				[
					-18.079591436937648,
					-10.136519144286181
				],
				[
					-18.275732505494183,
					-10.245486404595301
				],
				[
					-18.471873574050722,
					-10.354453664904488
				],
				[
					-18.864155711163548,
					-10.463420925213608
				],
				[
					-19.25643784827637,
					-10.572388185522726
				],
				[
					-19.648719985389448,
					-10.681355445831846
				],
				[
					-19.844861053945735,
					-10.790322706141033
				],
				[
					-20.23714319105856,
					-11.008257226759271
				],
				[
					-20.4332842596151,
					-11.008257226759271
				],
				[
					-20.825566396727922,
					-11.226191747377579
				],
				[
					-21.02170746528446,
					-11.335159007686698
				],
				[
					-21.413989602397287,
					-11.444126267995816
				],
				[
					-21.80627173951011,
					-11.553093528304935
				],
				[
					-22.198553876622938,
					-11.662060788614054
				],
				[
					-22.59083601373601,
					-11.771028048923242
				],
				[
					-22.7869770822923,
					-11.879995309232363
				],
				[
					-22.98311815084884,
					-11.988962569541481
				],
				[
					-23.179259219405374,
					-12.09792982985067
				],
				[
					-23.179259219405374,
					-12.206897090159789
				],
				[
					-23.37540028796166,
					-12.206897090159789
				],
				[
					-23.5715413565182,
					-12.206897090159789
				],
				[
					-23.76768242507474,
					-12.206897090159789
				],
				[
					-23.963823493631025,
					-12.206897090159789
				],
				[
					-23.963823493631025,
					-12.315864350468907
				],
				[
					-23.963823493631025,
					-12.424831610778027
				],
				[
					-23.963823493631025,
					-12.533798871087146
				],
				[
					-23.76768242507474,
					-12.533798871087146
				],
				[
					-23.5715413565182,
					-12.533798871087146
				],
				[
					-23.37540028796166,
					-12.642766131396334
				],
				[
					-23.179259219405374,
					-12.751733391705454
				],
				[
					-22.7869770822923,
					-12.860700652014572
				],
				[
					-22.59083601373601,
					-12.860700652014572
				],
				[
					-22.394694945179474,
					-12.96966791232369
				],
				[
					-22.002412808066648,
					-12.96966791232369
				],
				[
					-21.610130670953822,
					-12.96966791232369
				],
				[
					-21.217848533840748,
					-13.078635172632879
				],
				[
					-21.02170746528446,
					-13.187602432941997
				],
				[
					-20.825566396727922,
					-13.296569693251117
				],
				[
					-20.825566396727922,
					-13.405536953560237
				],
				[
					-20.629425328171635,
					-13.514504213869424
				],
				[
					-20.23714319105856,
					-13.623471474178544
				],
				[
					-19.844861053945735,
					-13.732438734487662
				],
				[
					-19.648719985389448,
					-13.84140599479678
				],
				[
					-19.45257891683291,
					-13.84140599479678
				],
				[
					-19.060296779720083,
					-13.84140599479678
				],
				[
					-18.66801464260701,
					-13.9503732551059
				],
				[
					-18.079591436937648,
					-14.168307775724207
				],
				[
					-18.079591436937648,
					-14.277275036033327
				],
				[
					-17.687309299824822,
					-14.495209556651634
				],
				[
					-17.49116823126853,
					-14.604176816960752
				],
				[
					-17.098886094155457,
					-14.71314407726987
				],
				[
					-16.70660395704263,
					-14.931078597888177
				],
				[
					-16.31432181992981,
					-15.040045858197297
				],
				[
					-15.922039682816983,
					-15.149013118506417
				],
				[
					-15.725898614260444,
					-15.257980378815535
				],
				[
					-15.333616477147618,
					-15.366947639124724
				],
				[
					-15.137475408591083,
					-15.475914899433842
				],
				[
					-14.941334340034793,
					-15.58488215974296
				],
				[
					-14.352911134365431,
					-15.911783940670388
				],
				[
					-14.156770065808894,
					-16.020751200979507
				],
				[
					-13.568346860139531,
					-16.238685721597744
				],
				[
					-13.568346860139531,
					-16.347652981906933
				],
				[
					-13.176064723026705,
					-16.347652981906933
				],
				[
					-12.78378258591388,
					-16.56558750252517
				],
				[
					-12.391500448801054,
					-16.892489283452598
				],
				[
					-12.195359380244518,
					-17.110423804070834
				],
				[
					-11.803077243131693,
					-17.32835832468914
				],
				[
					-11.606936174575155,
					-17.43732558499826
				],
				[
					-11.410795106018867,
					-17.54629284530738
				],
				[
					-11.21465403746233,
					-17.6552601056165
				],
				[
					-11.018512968905792,
					-17.6552601056165
				],
				[
					-11.018512968905792,
					-17.764227365925688
				],
				[
					-10.822371900349504,
					-17.873194626234806
				],
				[
					-10.822371900349504,
					-17.982161886543924
				],
				[
					-10.626230831792967,
					-18.20009640716223
				],
				[
					-10.626230831792967,
					-18.30906366747135
				],
				[
					-10.626230831792967,
					-18.52699818808959
				],
				[
					-10.626230831792967,
					-18.635965448398778
				],
				[
					-10.626230831792967,
					-18.744932708707896
				],
				[
					-10.626230831792967,
					-18.853899969017014
				],
				[
					-10.626230831792967,
					-18.962867229326136
				],
				[
					-10.626230831792967,
					-19.07183448963532
				],
				[
					-10.626230831792967,
					-19.18080174994444
				],
				[
					-10.626230831792967,
					-19.28976901025356
				],
				[
					-10.626230831792967,
					-19.39873627056268
				],
				[
					-10.822371900349504,
					-19.50770353087187
				],
				[
					-10.822371900349504,
					-19.616670791180987
				],
				[
					-11.018512968905792,
					-19.616670791180987
				],
				[
					-11.018512968905792,
					-19.725638051490105
				],
				[
					-11.018512968905792,
					-19.834605311799223
				],
				[
					-11.018512968905792,
					-19.943572572108344
				],
				[
					-10.822371900349504,
					-19.39873627056268
				]
			],
			"lastCommittedPoint": null,
			"simulatePressure": false,
			"pressures": [
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				0
			]
		},
		{
			"type": "arrow",
			"version": 104,
			"versionNonce": 201842124,
			"isDeleted": false,
			"id": "bPYMlPxa_GIGlqFWazLf7",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 359.87121763349455,
			"y": 137.28909355146664,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 42.562611876747724,
			"height": 0,
			"seed": 1007295148,
			"groupIds": [
				"26VEgMRc9JphcxEU73M7Z"
			],
			"roundness": null,
			"boundElements": [],
			"updated": 1680694807702,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": "arrow",
			"points": [
				[
					0,
					0
				],
				[
					42.562611876747724,
					0
				]
			]
		},
		{
			"type": "text",
			"version": 186,
			"versionNonce": 135277543,
			"isDeleted": false,
			"id": "lPp8XAoL",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -70.60727740248035,
			"y": -9.391339584950174,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 247.53585815429688,
			"height": 20,
			"seed": 1966814740,
			"groupIds": [
				"FZrDRf4b3pnOArtieNIP3"
			],
			"roundness": null,
			"boundElements": [],
			"updated": 1680694757818,
			"link": null,
			"locked": false,
			"fontSize": 16,
			"fontFamily": 1,
			"text": "everyone laughed & s.o crit Bill",
			"rawText": "everyone laughed & s.o crit Bill",
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "everyone laughed & s.o crit Bill",
			"lineHeight": 1.25
		},
		{
			"type": "text",
			"version": 194,
			"versionNonce": 1631988233,
			"isDeleted": false,
			"id": "BfQvezX6",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -56.368965224418616,
			"y": 13.737622335561582,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 42.39802551269531,
			"height": 12.406233505033688,
			"seed": 2009720108,
			"groupIds": [
				"FZrDRf4b3pnOArtieNIP3"
			],
			"roundness": null,
			"boundElements": [],
			"updated": 1680694757818,
			"link": null,
			"locked": false,
			"fontSize": 9.92498680402695,
			"fontFamily": 1,
			"text": "S/(NP\\S)",
			"rawText": "S/(NP\\S)",
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "S/(NP\\S)",
			"lineHeight": 1.25
		},
		{
			"type": "text",
			"version": 211,
			"versionNonce": 1561335559,
			"isDeleted": false,
			"id": "4XdboboT",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 20.123980480543423,
			"y": 13.229830454269859,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 23.936935424804688,
			"height": 12.406233505033688,
			"seed": 773018516,
			"groupIds": [
				"FZrDRf4b3pnOArtieNIP3"
			],
			"roundness": null,
			"boundElements": [],
			"updated": 1680694757818,
			"link": null,
			"locked": false,
			"fontSize": 9.92498680402695,
			"fontFamily": 1,
			"text": "NP\\S",
			"rawText": "NP\\S",
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "NP\\S",
			"lineHeight": 1.25
		},
		{
			"type": "text",
			"version": 217,
			"versionNonce": 1290141929,
			"isDeleted": false,
			"id": "B0t7puYh",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 76.18782441083681,
			"y": 13.229830454269859,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 42.39802551269531,
			"height": 12.406233505033688,
			"seed": 1848401836,
			"groupIds": [
				"FZrDRf4b3pnOArtieNIP3"
			],
			"roundness": null,
			"boundElements": [],
			"updated": 1680694757818,
			"link": null,
			"locked": false,
			"fontSize": 9.92498680402695,
			"fontFamily": 1,
			"text": "S/(NP\\S)",
			"rawText": "S/(NP\\S)",
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "S/(NP\\S)",
			"lineHeight": 1.25
		},
		{
			"type": "text",
			"version": 241,
			"versionNonce": 1562746407,
			"isDeleted": false,
			"id": "UMKiPqjP",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 126.24016870177547,
			"y": 13.229830454269859,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 31.40667724609375,
			"height": 12.406233505033688,
			"seed": 554418452,
			"groupIds": [
				"FZrDRf4b3pnOArtieNIP3"
			],
			"roundness": null,
			"boundElements": [],
			"updated": 1680694757818,
			"link": null,
			"locked": false,
			"fontSize": 9.92498680402695,
			"fontFamily": 1,
			"text": "(NP\\S)",
			"rawText": "(NP\\S)",
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "(NP\\S)",
			"lineHeight": 1.25
		},
		{
			"type": "text",
			"version": 238,
			"versionNonce": 673709001,
			"isDeleted": false,
			"id": "VC6jgLI1",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 149.02146818667552,
			"y": 27.468142632332956,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 12.945587158203125,
			"height": 12.406233505033688,
			"seed": 391875116,
			"groupIds": [
				"FZrDRf4b3pnOArtieNIP3"
			],
			"roundness": null,
			"boundElements": [],
			"updated": 1680694757818,
			"link": null,
			"locked": false,
			"fontSize": 9.92498680402695,
			"fontFamily": 1,
			"text": "NP",
			"rawText": "NP",
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "NP",
			"lineHeight": 1.25
		},
		{
			"type": "line",
			"version": 135,
			"versionNonce": 1262305607,
			"isDeleted": false,
			"id": "UKgm2YY0tPtaSjNXOW3Iu",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 162.90104231774058,
			"y": 26.07749288988248,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 23.730520296770464,
			"height": 6.644545683095885,
			"seed": 1167752852,
			"groupIds": [
				"FZrDRf4b3pnOArtieNIP3"
			],
			"roundness": null,
			"boundElements": [],
			"updated": 1680694757818,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					-23.730520296770464,
					6.644545683095885
				]
			]
		},
		{
			"type": "text",
			"version": 225,
			"versionNonce": 52927145,
			"isDeleted": false,
			"id": "2IfzUAD3",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 166.10744280034942,
			"y": 12.28060964239944,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 12.945587158203125,
			"height": 12.406233505033688,
			"seed": 1447126188,
			"groupIds": [
				"FZrDRf4b3pnOArtieNIP3"
			],
			"roundness": null,
			"boundElements": [],
			"updated": 1680694757818,
			"link": null,
			"locked": false,
			"fontSize": 9.92498680402695,
			"fontFamily": 1,
			"text": "NP",
			"rawText": "NP",
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "NP",
			"lineHeight": 1.25
		},
		{
			"type": "text",
			"version": 255,
			"versionNonce": 1488158823,
			"isDeleted": false,
			"id": "2xBGe6Ys",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 148.07224737480465,
			"y": 54.99554617658646,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 23.936935424804688,
			"height": 12.406233505033688,
			"seed": 880118804,
			"groupIds": [
				"FZrDRf4b3pnOArtieNIP3"
			],
			"roundness": null,
			"boundElements": [],
			"updated": 1680694757818,
			"link": null,
			"locked": false,
			"fontSize": 9.92498680402695,
			"fontFamily": 1,
			"text": "NP\\S",
			"rawText": "NP\\S",
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "NP\\S",
			"lineHeight": 1.25
		},
		{
			"type": "text",
			"version": 245,
			"versionNonce": 1586519433,
			"isDeleted": false,
			"id": "OkpMeyQw",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 131.9354935729998,
			"y": 78.72606647335647,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 6.0313568115234375,
			"height": 12.406233505033688,
			"seed": 545779500,
			"groupIds": [
				"FZrDRf4b3pnOArtieNIP3"
			],
			"roundness": null,
			"boundElements": [],
			"updated": 1680694757818,
			"link": null,
			"locked": false,
			"customData": {
				"legacyTextWrap": true
			},
			"fontSize": 9.92498680402695,
			"fontFamily": 1,
			"text": "S",
			"rawText": "S",
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "S",
			"lineHeight": 1.25
		},
		{
			"type": "text",
			"version": 248,
			"versionNonce": 1465179015,
			"isDeleted": false,
			"id": "LJzYdr32",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 100.61120678126281,
			"y": 77.7768456614856,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 12.945587158203125,
			"height": 12.406233505033688,
			"seed": 2140334484,
			"groupIds": [
				"FZrDRf4b3pnOArtieNIP3"
			],
			"roundness": null,
			"boundElements": [],
			"updated": 1680694757818,
			"link": null,
			"locked": false,
			"customData": {
				"legacyTextWrap": true
			},
			"fontSize": 9.92498680402695,
			"fontFamily": 1,
			"text": "NP",
			"rawText": "NP",
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "NP",
			"lineHeight": 1.25
		},
		{
			"type": "text",
			"version": 289,
			"versionNonce": 429769833,
			"isDeleted": false,
			"id": "JI5181kl",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 106.30653165248805,
			"y": -36.12965176301236,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 23.936935424804688,
			"height": 12.406233505033688,
			"seed": 647571884,
			"groupIds": [
				"FZrDRf4b3pnOArtieNIP3"
			],
			"roundness": null,
			"boundElements": [],
			"updated": 1680694757818,
			"link": null,
			"locked": false,
			"fontSize": 9.92498680402695,
			"fontFamily": 1,
			"text": "S/NP",
			"rawText": "S/NP",
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "S/NP",
			"lineHeight": 1.25
		},
		{
			"type": "line",
			"version": 207,
			"versionNonce": 727569063,
			"isDeleted": false,
			"id": "1WBx0EfOQJfYGHOvEpVAJ",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 102.15091035800833,
			"y": -7.145235525595808,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 30.375065979866577,
			"height": 14.238312178062415,
			"seed": 170432276,
			"groupIds": [
				"FZrDRf4b3pnOArtieNIP3"
			],
			"roundness": null,
			"boundElements": [],
			"updated": 1680694757818,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					17.085974613674807,
					-14.238312178062415
				],
				[
					30.375065979866577,
					-0.9492208118708731
				]
			]
		},
		{
			"type": "line",
			"version": 261,
			"versionNonce": 538660681,
			"isDeleted": false,
			"id": "Lmjyj3FBN9lKms4I4DtsF",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 151.97739076662947,
			"y": 42.96479745144484,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 21.832078673029173,
			"height": 17.08597461367458,
			"seed": 2074022956,
			"groupIds": [
				"FZrDRf4b3pnOArtieNIP3"
			],
			"roundness": null,
			"boundElements": [],
			"updated": 1680694757818,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					9.492208118708277,
					7.593766494966303
				],
				[
					21.832078673029173,
					-9.492208118708277
				]
			]
		},
		{
			"type": "line",
			"version": 303,
			"versionNonce": 2105206215,
			"isDeleted": false,
			"id": "V0bTkogLXjvAe1jk4UBGJ",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 102.49127851828598,
			"y": 30.931105898036776,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 56.00402790037879,
			"height": 47.4610405935407,
			"seed": 1621922964,
			"groupIds": [
				"FZrDRf4b3pnOArtieNIP3"
			],
			"roundness": null,
			"boundElements": [],
			"updated": 1680694757818,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					39.8672740985744,
					47.4610405935407
				],
				[
					56.00402790037879,
					39.8672740985744
				]
			]
		},
		{
			"type": "arrow",
			"version": 390,
			"versionNonce": 1339635732,
			"isDeleted": false,
			"id": "2xSk40HPbBtSMOq1anos5",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 117.33844334794048,
			"y": 84.92918322587298,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 9.492208118708277,
			"height": 0,
			"seed": 1032040108,
			"groupIds": [
				"FZrDRf4b3pnOArtieNIP3"
			],
			"roundness": null,
			"boundElements": [],
			"updated": 1680694757819,
			"link": null,
			"locked": false,
			"startBinding": {
				"focus": 0.15302320587231938,
				"gap": 3.764972505703554,
				"elementId": "LJzYdr32"
			},
			"endBinding": {
				"focus": 3.6654747043246886e-14,
				"gap": 5.104842106351043,
				"elementId": "OkpMeyQw"
			},
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": "arrow",
			"points": [
				[
					0,
					0
				],
				[
					9.492208118708277,
					0
				]
			]
		},
		{
			"type": "line",
			"version": 341,
			"versionNonce": 751342823,
			"isDeleted": false,
			"id": "BuQVvEc2BAtTpSNW8dZ9t",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 125.27257800318603,
			"y": -38.362013368533326,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 35.12117003922049,
			"height": 32.27350760360787,
			"seed": 282386964,
			"groupIds": [
				"FZrDRf4b3pnOArtieNIP3"
			],
			"roundness": null,
			"boundElements": [],
			"updated": 1680694757818,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					18.984416237416553,
					-8.542987306837404
				],
				[
					35.12117003922049,
					23.730520296770464
				]
			]
		},
		{
			"type": "text",
			"version": 281,
			"versionNonce": 1365561609,
			"isDeleted": false,
			"id": "99O9vYYN",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 144.69464135829458,
			"y": -61.75861368352412,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 6.0313568115234375,
			"height": 12.406233505033688,
			"seed": 1022425388,
			"groupIds": [
				"FZrDRf4b3pnOArtieNIP3"
			],
			"roundness": null,
			"boundElements": [],
			"updated": 1680694757818,
			"link": null,
			"locked": false,
			"fontSize": 9.92498680402695,
			"fontFamily": 1,
			"text": "S",
			"rawText": "S",
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "S",
			"lineHeight": 1.25
		},
		{
			"type": "text",
			"version": 271,
			"versionNonce": 13852679,
			"isDeleted": false,
			"id": "K66iUyGW",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 114.31957537842845,
			"y": -18.09445633746668,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 4.979827880859375,
			"height": 12.406233505033688,
			"seed": 26705812,
			"groupIds": [
				"FZrDRf4b3pnOArtieNIP3"
			],
			"roundness": null,
			"boundElements": [],
			"updated": 1680694757818,
			"link": null,
			"locked": false,
			"fontSize": 9.92498680402695,
			"fontFamily": 1,
			"text": "c",
			"rawText": "c",
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "c",
			"lineHeight": 1.25
		},
		{
			"type": "text",
			"version": 258,
			"versionNonce": 815253481,
			"isDeleted": false,
			"id": "VNlYGE16",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -7.141330997351133,
			"y": -51.52155162852273,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 6.0313568115234375,
			"height": 12.406233505033688,
			"seed": 626062252,
			"groupIds": [
				"FZrDRf4b3pnOArtieNIP3"
			],
			"roundness": null,
			"boundElements": [],
			"updated": 1680694757818,
			"link": null,
			"locked": false,
			"fontSize": 9.92498680402695,
			"fontFamily": 1,
			"text": "S",
			"rawText": "S",
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "S",
			"lineHeight": 1.25
		},
		{
			"type": "line",
			"version": 290,
			"versionNonce": 462931751,
			"isDeleted": false,
			"id": "EyTMutYjSxT3mdlie1dvd",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -31.143030865593346,
			"y": -7.7882574872121495,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 63.585155185596705,
			"height": 30.509137621014816,
			"seed": 1261071636,
			"groupIds": [
				"FZrDRf4b3pnOArtieNIP3"
			],
			"roundness": null,
			"boundElements": [],
			"updated": 1680694757818,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					28.77100600087624,
					-29.92085430299062
				],
				[
					63.585155185596705,
					0.588283318024196
				]
			]
		},
		{
			"type": "line",
			"version": 365,
			"versionNonce": 1611195081,
			"isDeleted": false,
			"id": "GiOAsDePJUH4ezCaBobjQ",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 6.361836455027742,
			"y": -54.25053246668426,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 78.65269565856715,
			"height": 16.056598800002348,
			"seed": 617455148,
			"groupIds": [
				"FZrDRf4b3pnOArtieNIP3"
			],
			"roundness": null,
			"boundElements": [],
			"updated": 1680694757818,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					61.366093554648614,
					-13.930811352083538
				],
				[
					78.65269565856715,
					2.1257874479188104
				]
			]
		},
		{
			"type": "line",
			"version": 346,
			"versionNonce": 729958983,
			"isDeleted": false,
			"id": "E6OChV2EUvZwLBbdCk6Pe",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 73.39701651844621,
			"y": -15.505428393332977,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 35.91008084748864,
			"height": 36.685872474528196,
			"seed": 251898516,
			"groupIds": [
				"FZrDRf4b3pnOArtieNIP3"
			],
			"roundness": null,
			"boundElements": [],
			"updated": 1680694757818,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					14.318467179864001,
					-36.685872474528196
				],
				[
					35.91008084748864,
					-23.08928028235755
				]
			]
		}
	],
	"appState": {
		"theme": "light",
		"viewBackgroundColor": "#ffffff",
		"currentItemStrokeColor": "#000000",
		"currentItemBackgroundColor": "transparent",
		"currentItemFillStyle": "hachure",
		"currentItemStrokeWidth": 1,
		"currentItemStrokeStyle": "solid",
		"currentItemRoughness": 1,
		"currentItemOpacity": 100,
		"currentItemFontFamily": 1,
		"currentItemFontSize": 20,
		"currentItemTextAlign": "left",
		"currentItemStartArrowhead": null,
		"currentItemEndArrowhead": "arrow",
		"scrollX": 727.0024425106142,
		"scrollY": 583.9422053527483,
		"zoom": {
			"value": 0.6046104171872138
		},
		"currentItemRoundness": "round",
		"gridSize": null,
		"colorPalette": {},
		"currentStrokeOptions": null,
		"previousGridSize": null
	},
	"files": {}
}
```
%%