---

excalidraw-plugin: parsed
tags: [excalidraw]

---
==⚠  Switch to EXCALIDRAW VIEW in the MORE OPTIONS menu of this document. ⚠==


# Text Elements
PNQ ^yo0M7cej

PN ^qHKOc1uf

PQ ^LZ0QRH4l

QN ^QYF1K3LM

P ^LNQEJJCG

N ^gT19Ut6n

Q ^Cc2hOWUR

P ^DoMpK6BC

N ^Z4f41REm

Q ^Rlk6hALn

%%
# Drawing
```json
{
	"type": "excalidraw",
	"version": 2,
	"source": "https://github.com/zsviczian/obsidian-excalidraw-plugin/releases/tag/1.8.22",
	"elements": [
		{
			"type": "text",
			"version": 82,
			"versionNonce": 1252463636,
			"isDeleted": false,
			"id": "yo0M7cej",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -25.88679313659668,
			"y": -148.9683485146379,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 56.28302001953125,
			"height": 35,
			"seed": 1995853044,
			"groupIds": [],
			"roundness": null,
			"boundElements": null,
			"updated": 1680701686467,
			"link": null,
			"locked": false,
			"fontSize": 28,
			"fontFamily": 1,
			"text": "PNQ",
			"rawText": "",
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "PNQ",
			"lineHeight": 1.25
		},
		{
			"type": "text",
			"version": 60,
			"versionNonce": 1712514860,
			"isDeleted": false,
			"id": "qHKOc1uf",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -127.88679313659668,
			"y": -67.96834851463791,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 36.54716873168945,
			"height": 35,
			"seed": 1410870348,
			"groupIds": [],
			"roundness": null,
			"boundElements": null,
			"updated": 1680701686467,
			"link": null,
			"locked": false,
			"fontSize": 28,
			"fontFamily": 1,
			"text": "PN",
			"rawText": "",
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "PN",
			"lineHeight": 1.25
		},
		{
			"type": "text",
			"version": 103,
			"versionNonce": 1716744596,
			"isDeleted": false,
			"id": "LZ0QRH4l",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -49.88679313659668,
			"y": -62.96834851463791,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 38.24528121948242,
			"height": 35,
			"seed": 973639284,
			"groupIds": [],
			"roundness": null,
			"boundElements": null,
			"updated": 1680701686467,
			"link": null,
			"locked": false,
			"fontSize": 28,
			"fontFamily": 1,
			"text": "PQ",
			"rawText": "",
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "PQ",
			"lineHeight": 1.25
		},
		{
			"type": "text",
			"version": 76,
			"versionNonce": 1879384492,
			"isDeleted": false,
			"id": "QYF1K3LM",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 26.11320686340332,
			"y": -61.96834851463791,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 37.77358627319336,
			"height": 35,
			"seed": 862241484,
			"groupIds": [],
			"roundness": null,
			"boundElements": null,
			"updated": 1680701686467,
			"link": null,
			"locked": false,
			"fontSize": 28,
			"fontFamily": 1,
			"text": "QN",
			"rawText": "",
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "QN",
			"lineHeight": 1.25
		},
		{
			"type": "text",
			"version": 81,
			"versionNonce": 1469587220,
			"isDeleted": false,
			"id": "LNQEJJCG",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -112.88679313659668,
			"y": 20.03165148536209,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 18.50943374633789,
			"height": 35,
			"seed": 878092276,
			"groupIds": [],
			"roundness": null,
			"boundElements": null,
			"updated": 1680701686467,
			"link": null,
			"locked": false,
			"fontSize": 28,
			"fontFamily": 1,
			"text": "P",
			"rawText": "",
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "P",
			"lineHeight": 1.25
		},
		{
			"type": "text",
			"version": 77,
			"versionNonce": 617352236,
			"isDeleted": false,
			"id": "gT19Ut6n",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -43.88679313659668,
			"y": 12.03165148536209,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 18.037734985351562,
			"height": 35,
			"seed": 1507494220,
			"groupIds": [],
			"roundness": null,
			"boundElements": null,
			"updated": 1680701686467,
			"link": null,
			"locked": false,
			"fontSize": 28,
			"fontFamily": 1,
			"text": "N",
			"rawText": "",
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "N",
			"lineHeight": 1.25
		},
		{
			"type": "text",
			"version": 90,
			"versionNonce": 1726684308,
			"isDeleted": false,
			"id": "Cc2hOWUR",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 27.11320686340332,
			"y": 25.03165148536209,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 19.735849380493164,
			"height": 35,
			"seed": 770081140,
			"groupIds": [],
			"roundness": null,
			"boundElements": null,
			"updated": 1680701686467,
			"link": null,
			"locked": false,
			"fontSize": 28,
			"fontFamily": 1,
			"text": "Q",
			"rawText": "",
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "Q",
			"lineHeight": 1.25
		},
		{
			"type": "text",
			"version": 64,
			"versionNonce": 642885292,
			"isDeleted": false,
			"id": "DoMpK6BC",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -84.88679313659668,
			"y": 84.03165148536209,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 13.226414680480957,
			"height": 25,
			"seed": 1792245708,
			"groupIds": [],
			"roundness": null,
			"boundElements": null,
			"updated": 1680701686467,
			"link": null,
			"locked": false,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "P",
			"rawText": "",
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "P",
			"lineHeight": 1.25
		},
		{
			"type": "text",
			"version": 78,
			"versionNonce": 1577216532,
			"isDeleted": false,
			"id": "Z4f41REm",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -28.88679313659668,
			"y": 65.03165148536209,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 12.886792182922363,
			"height": 25,
			"seed": 2113811188,
			"groupIds": [],
			"roundness": null,
			"boundElements": null,
			"updated": 1680701686467,
			"link": null,
			"locked": false,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "N",
			"rawText": "",
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "N",
			"lineHeight": 1.25
		},
		{
			"type": "text",
			"version": 61,
			"versionNonce": 1228927276,
			"isDeleted": false,
			"id": "Rlk6hALn",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -4.88679313659668,
			"y": 80.03165148536209,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 14.094339370727539,
			"height": 25,
			"seed": 241804876,
			"groupIds": [],
			"roundness": null,
			"boundElements": null,
			"updated": 1680701686467,
			"link": null,
			"locked": false,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "Q",
			"rawText": "",
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "Q",
			"lineHeight": 1.25
		},
		{
			"type": "ellipse",
			"version": 53,
			"versionNonce": 1607731092,
			"isDeleted": false,
			"id": "UceRqeYTdEjkTQd1cLG72",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -46.88679313659668,
			"y": 99.03165148536209,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 23,
			"height": 21,
			"seed": 589542516,
			"groupIds": [],
			"roundness": {
				"type": 2
			},
			"boundElements": null,
			"updated": 1680701686467,
			"link": null,
			"locked": false
		},
		{
			"type": "line",
			"version": 78,
			"versionNonce": 1023470508,
			"isDeleted": false,
			"id": "cSY-zmDMmoIL5d2IzIEdj",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -15.88679313659668,
			"y": 98.03165148536209,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 37,
			"height": 28,
			"seed": 1500970188,
			"groupIds": [],
			"roundness": {
				"type": 2
			},
			"boundElements": null,
			"updated": 1680701686467,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					-18,
					11
				],
				[
					-37,
					28
				]
			]
		},
		{
			"type": "arrow",
			"version": 59,
			"versionNonce": 2066022132,
			"isDeleted": false,
			"id": "4s1BS-PvguJWQmbnRNW7P",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -17.88679313659668,
			"y": 103.03165148536209,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 45,
			"height": 41,
			"seed": 1528628724,
			"groupIds": [],
			"roundness": {
				"type": 2
			},
			"boundElements": null,
			"updated": 1680701686468,
			"link": null,
			"locked": false,
			"startBinding": {
				"focus": 0.6366953673473986,
				"gap": 7.296278603944776,
				"elementId": "UceRqeYTdEjkTQd1cLG72"
			},
			"endBinding": {
				"focus": -0.3967125855048426,
				"gap": 2,
				"elementId": "Cc2hOWUR"
			},
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": "arrow",
			"points": [
				[
					0,
					0
				],
				[
					45,
					-41
				]
			]
		},
		{
			"type": "arrow",
			"version": 51,
			"versionNonce": 1961204300,
			"isDeleted": false,
			"id": "10ZoZrmJ3RKQU__zqJc4U",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -35.88679313659668,
			"y": 95.03165148536209,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 0,
			"height": 46,
			"seed": 898990924,
			"groupIds": [],
			"roundness": {
				"type": 2
			},
			"boundElements": null,
			"updated": 1680701686468,
			"link": null,
			"locked": false,
			"startBinding": {
				"focus": -0.043478260869565216,
				"gap": 4.007530831141617,
				"elementId": "UceRqeYTdEjkTQd1cLG72"
			},
			"endBinding": {
				"focus": 0.11297066882324229,
				"gap": 2,
				"elementId": "gT19Ut6n"
			},
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": "arrow",
			"points": [
				[
					0,
					0
				],
				[
					0,
					-46
				]
			]
		},
		{
			"type": "arrow",
			"version": 54,
			"versionNonce": 1771110516,
			"isDeleted": false,
			"id": "Kup47BzTTMHL8qPpODjLp",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -50.88679313659668,
			"y": 102.03165148536209,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 44,
			"height": 46,
			"seed": 988817268,
			"groupIds": [],
			"roundness": {
				"type": 2
			},
			"boundElements": null,
			"updated": 1680701686468,
			"link": null,
			"locked": false,
			"startBinding": {
				"focus": -0.545317982426113,
				"gap": 5.920244662604716,
				"elementId": "UceRqeYTdEjkTQd1cLG72"
			},
			"endBinding": {
				"focus": 0.3443264454486527,
				"gap": 1,
				"elementId": "LNQEJJCG"
			},
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": "arrow",
			"points": [
				[
					0,
					0
				],
				[
					-44,
					-46
				]
			]
		},
		{
			"type": "arrow",
			"version": 66,
			"versionNonce": 1951690956,
			"isDeleted": false,
			"id": "QZAwaWoKcgRjo7gds5TNh",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 25.11320686340332,
			"y": 34.03165148536209,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 49,
			"height": 56,
			"seed": 1226783180,
			"groupIds": [],
			"roundness": {
				"type": 2
			},
			"boundElements": null,
			"updated": 1680701686468,
			"link": null,
			"locked": false,
			"startBinding": {
				"focus": -0.17594717899903684,
				"gap": 2,
				"elementId": "Cc2hOWUR"
			},
			"endBinding": {
				"focus": 0.3974178809036104,
				"gap": 6,
				"elementId": "LZ0QRH4l"
			},
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": "arrow",
			"points": [
				[
					0,
					0
				],
				[
					-49,
					-56
				]
			]
		},
		{
			"type": "arrow",
			"version": 108,
			"versionNonce": 2113982964,
			"isDeleted": false,
			"id": "wRLK4SC4Ndh-eiFhofBse",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -97.88679313659668,
			"y": 27.03165148536209,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 61,
			"height": 51,
			"seed": 1189288180,
			"groupIds": [],
			"roundness": {
				"type": 2
			},
			"boundElements": null,
			"updated": 1680701686468,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": {
				"focus": -0.48916560712925977,
				"gap": 4,
				"elementId": "LZ0QRH4l"
			},
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": "arrow",
			"points": [
				[
					0,
					0
				],
				[
					61,
					-51
				]
			]
		},
		{
			"type": "arrow",
			"version": 56,
			"versionNonce": 2091153228,
			"isDeleted": false,
			"id": "uNtDxQNM1wUqBVEzGOP9I",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -18.88679313659668,
			"y": 44.03165148536209,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 60,
			"height": 60,
			"seed": 1596858444,
			"groupIds": [],
			"roundness": {
				"type": 2
			},
			"boundElements": null,
			"updated": 1680701686468,
			"link": null,
			"locked": false,
			"startBinding": {
				"focus": 1.149413055280086,
				"gap": 6.9622650146484375,
				"elementId": "gT19Ut6n"
			},
			"endBinding": {
				"focus": -0.6764324289586305,
				"gap": 11,
				"elementId": "QYF1K3LM"
			},
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": "arrow",
			"points": [
				[
					0,
					0
				],
				[
					60,
					-60
				]
			]
		},
		{
			"type": "arrow",
			"version": 182,
			"versionNonce": 1736078196,
			"isDeleted": false,
			"id": "mVVpNuBPj92kVOf-9q785",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 38.11320686340332,
			"y": 29.03165148536209,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 3,
			"height": 51,
			"seed": 175889012,
			"groupIds": [],
			"roundness": {
				"type": 2
			},
			"boundElements": null,
			"updated": 1680701686468,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": {
				"focus": 0.12870241783183958,
				"gap": 5,
				"elementId": "QYF1K3LM"
			},
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": "arrow",
			"points": [
				[
					0,
					0
				],
				[
					3,
					-51
				]
			]
		},
		{
			"type": "arrow",
			"version": 55,
			"versionNonce": 976203212,
			"isDeleted": false,
			"id": "LqeGJ6c5D3yfM9gnf9yhE",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -107.88679313659668,
			"y": 17.03165148536209,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 1,
			"height": 41,
			"seed": 1335637708,
			"groupIds": [],
			"roundness": {
				"type": 2
			},
			"boundElements": null,
			"updated": 1680701686468,
			"link": null,
			"locked": false,
			"startBinding": {
				"focus": -0.3878220294885764,
				"gap": 3,
				"elementId": "LNQEJJCG"
			},
			"endBinding": {
				"focus": -0.004281946501678698,
				"gap": 9,
				"elementId": "qHKOc1uf"
			},
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": "arrow",
			"points": [
				[
					0,
					0
				],
				[
					-1,
					-41
				]
			]
		},
		{
			"type": "arrow",
			"version": 60,
			"versionNonce": 71558388,
			"isDeleted": false,
			"id": "SX6LI5L1lKZyvz2Hz0z7u",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -48.88679313659668,
			"y": 25.03165148536209,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 55,
			"height": 51,
			"seed": 813722612,
			"groupIds": [],
			"roundness": {
				"type": 2
			},
			"boundElements": null,
			"updated": 1680701686468,
			"link": null,
			"locked": false,
			"startBinding": {
				"focus": -0.32862892826430806,
				"gap": 5,
				"elementId": "gT19Ut6n"
			},
			"endBinding": {
				"focus": 0.5571280536401801,
				"gap": 7,
				"elementId": "qHKOc1uf"
			},
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": "arrow",
			"points": [
				[
					0,
					0
				],
				[
					-55,
					-51
				]
			]
		},
		{
			"type": "arrow",
			"version": 58,
			"versionNonce": 1041241164,
			"isDeleted": false,
			"id": "FPim8T-f0qdWSjwzZZUgd",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -101.88679313659668,
			"y": -62.96834851463791,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 66,
			"height": 53,
			"seed": 1728281932,
			"groupIds": [],
			"roundness": {
				"type": 2
			},
			"boundElements": null,
			"updated": 1680701686468,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": {
				"focus": 0.37729076856360383,
				"gap": 10,
				"elementId": "yo0M7cej"
			},
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": "arrow",
			"points": [
				[
					0,
					0
				],
				[
					66,
					-53
				]
			]
		},
		{
			"type": "arrow",
			"version": 46,
			"versionNonce": 2044929652,
			"isDeleted": false,
			"id": "4Dvv5u5pB0pveijzTpHe9",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -27.88679313659668,
			"y": -56.96834851463791,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 0,
			"height": 58,
			"seed": 452382068,
			"groupIds": [],
			"roundness": {
				"type": 2
			},
			"boundElements": null,
			"updated": 1680701686468,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": {
				"focus": 1.071069391774143,
				"gap": 2,
				"elementId": "yo0M7cej"
			},
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": "arrow",
			"points": [
				[
					0,
					0
				],
				[
					0,
					-58
				]
			]
		},
		{
			"type": "arrow",
			"version": 149,
			"versionNonce": 1713351372,
			"isDeleted": false,
			"id": "fGeqDOBLdAePPEADiL-hp",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 27.11320686340332,
			"y": -51.96834851463791,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 49,
			"height": 56,
			"seed": 1321071564,
			"groupIds": [],
			"roundness": {
				"type": 2
			},
			"boundElements": null,
			"updated": 1680701686468,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": {
				"focus": 1.0287660448303637,
				"gap": 6,
				"elementId": "yo0M7cej"
			},
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": "arrow",
			"points": [
				[
					0,
					0
				],
				[
					-49,
					-56
				]
			]
		}
	],
	"appState": {
		"theme": "light",
		"viewBackgroundColor": "#ffffff",
		"currentItemStrokeColor": "#000000",
		"currentItemBackgroundColor": "transparent",
		"currentItemFillStyle": "hachure",
		"currentItemStrokeWidth": 1,
		"currentItemStrokeStyle": "solid",
		"currentItemRoughness": 1,
		"currentItemOpacity": 100,
		"currentItemFontFamily": 1,
		"currentItemFontSize": 20,
		"currentItemTextAlign": "left",
		"currentItemStartArrowhead": null,
		"currentItemEndArrowhead": "arrow",
		"scrollX": 222,
		"scrollY": 368.9562683105469,
		"zoom": {
			"value": 1
		},
		"currentItemRoundness": "round",
		"gridSize": null,
		"colorPalette": {},
		"currentStrokeOptions": null,
		"previousGridSize": null
	},
	"files": {}
}
```
%%