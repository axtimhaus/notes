Introduction: [[Pustejovsky (1995)]]

## Introduction

## Levels of representation
### Lexical type structure
### Argument structure
### Event structure
### Qualia structure
#### Constitative
#### Formal
#### Telic
#### Agentive

## Generative devices
### Subselection
### Type coercion
### Co-composition
