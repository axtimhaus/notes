---
alias: OT
---

For a general overview, see: [[The optimal output of a grammar is the least costly violation of ranked universal constraints.]]
Introductory textbook: [[Kager (1999)]]