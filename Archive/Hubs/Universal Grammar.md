---
alias: UG
---

For a general introduction, see: [[Grammars of individual languages draw their basic options from a limited set of core properties, the Universal Grammar.]]