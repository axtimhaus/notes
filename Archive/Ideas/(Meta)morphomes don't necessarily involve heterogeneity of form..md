```ad-north
Metamorphomes are irregular partitionings of sets of cells within paradigms. They represent a systematic mapping of a set of heterogeneous forms to a set of heterogenous functions. This heterogeneity of form is used a diagnostic for morphomic patterns.

See: [[Some morphological phenomena are independent of phonological or functional conditioning.]]
```

A common example of metamorphomes is the L-pattern morphome (also called *Waluigi-pattern*) in Romance languages. Galician speakers abstract from the L-pattern to merge the reflexes of *colber* and *caber*:

| Input | <span style="font-variant:small-caps;">prs.ind</span> | <span style="font-variant:small-caps;">prs.sbjv</span> |
| -----:|:----------------------------------------------------- |:------------------------------------------------------ |
|   1sg | **ˈkoʎo**                                                 | **ˈkoʎa**                                                  |
|   2sg | ˈkaβes                                                | **ˈkoʎas**                                                 |
|   3sg | ˈkaβe                                                 | **ˈkoʎa**                                                  |
|   1pl | kaˈβemos                                              | **koˈʎamos**                                               |
|   2pl | kaˈβeðes                                              | **koˈʎaðes**                                               |
|   3pl | ˈkaβeŋ                                                | **ˈkoʎvaŋ**                                                |

This introduces the problem that here the morphomic pattern clearly references an incoherent array of paradigm cells, but no longer the phonological content. The set of cells is independent from their content.
In Romanian, first conjugation verbs show a morphomic pattern because of historical, no longer active phonological rules, which were phonologically coherent. This pattern has been extended to verbs that would not have undergone these rules.
[[Maiden (2021)]] argues that metamorphomic patterns do not need to be heterogeneous in form, since the abstraction of coherent patterns allows still for morphomic patterns to emerge.

```ad-south

```
```ad-east
*Can patterns that emerge from analogy still be said to be phonologically coherent?*
```
```ad-west
Similarly to heterogeneity of form, typological uniqueness has also been argued to be an inherent property of morphomes, but this diagnostic has limited usefulness.

See: [[A pattern is likely not morphomic if it is not typologically unique.]]
```

## Links
[[Some morphological phenomena are independent of phonological or functional conditioning.]]
[[A pattern is likely not morphomic if it is not typologically unique.]]