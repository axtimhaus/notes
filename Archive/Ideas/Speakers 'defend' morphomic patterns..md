```ad-north
Systematicity and predictiveness are two features commonly assumed for morphomes. While, as [[Maiden (2021)]] points out, these are not inherent to morphomes, there is some drive towards systematicity present in morphomic patterns, even against the alternative of eliminating irregular form.

See: [[Morphomic patterns need to acquire systematicity and predictiveness to survive.]]
```

Morphomic patterns do not only appear in archaic lexems and small variants of languages, but are present in high-frequency constructions and paradigms. This robustness exists even when the language could just as well rid itself of these irregular forms in favour of a more natural class.
One reason for this robustness is that the transition state to such a class, one where only a few lexems are regular, while others remain morphomic, would be maximally unpredictable. The natural class remains forever out of reach.

```ad-south
This "defense" of morphemic patterns and - in consequence - of predictive patterns against more elegant ones, might be the reason of the easy spread of morphomic patters across unrelated lexems in the first place. A morphome needs to abstract to survive, often to a point where it jumps paradigms.
```
```ad-east
Keeping irregular forms around because they are predictable is in the end inelegant. This is in contrast with the tendency in linguistics to move towards the most elegant models able to explain a phenomenon. The shift of whole paradigms is more likely to create remnants that themselves become morphomic than to simplify a language's grammar.
```
```ad-west

```

## Links
[[Maiden (2021)]]
[[Morphomic patterns need to acquire systematicity and predictiveness to survive.]]