The default head of a nominal is the rightmost morpheme (i.e. suffix), the information of which is passed upwards. In a root+case-nominal, the case would be the head of the nominal, with the case marking passing the case information to the syntax.
According to [[Christdas (1986)]], in Tamil the nominative is unmarked and behaves differently to different cases. She argues that nominals in nominative case do not have a case head.
The alternative interpretation would require a zero morpheme or a weak headedness condition.

More on heads:
- [[Head movement of N to D is not possible in Danish.]]

Open questions:
- **Q: Does German Nominative Singular have a case head?**