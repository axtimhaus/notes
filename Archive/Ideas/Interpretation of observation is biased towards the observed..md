By examining a subject not in totality but in parts the observed parts tend to bias the interpretation (or the model) of the whole. 

In the vedic parable of people touching an elephant and each proposing the animal resembles the part they are touching maps closely to minimal research questions as described in [[Newell (1973)]].

> perceiving the elephant through touching its different parts, [we]
come to have diverse notions regarding it, each one regarding
[the elephant] to be like the part that they had touched; and as
none of [us] had touched the whole elephant, none had any
idea of the elephant as a whole entity [28; §18,29]. 

(from [[Martin & Baggio (2020)]])

A researcher only studying morphology will tend to place morphological components with high prominence in human language processing compared to a semantician. Without broad and complex research agendas, it is impossible to find a model that can explain every aspect of the collected observations. 