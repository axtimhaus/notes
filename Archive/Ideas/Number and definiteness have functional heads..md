```ad-north
There are many independent proposals that nominal structure should parrallel sentential structure by employing one or more functional heads. Number and definiteness are such functional categories that are specified above the NP, e.g. through determina.
```

Number and definiteness are not lexical categories, but are are functional heads of their own phrasal projections above NP. 
[[Ritter (1991)]] provides evidence in Modern Hebrew, where definite markers attach to the arguments of the head of a genitive NP, after the N is raised into the specifier of the DP:
![[Ritter (1991) 2023-04-09 13.30.26.excalidraw]]

```ad-south
Since NPs are not the highest projection of a noun phrase and arguments can be moved to higher projections above NP within the noun phrase, the maximal projection of a noun phrase are DPs, not NPs.

See: [[Noun phrases are DPs.]]
```
```ad-east
NPs provide the lexical head of a phrase, so depending on the definition of headedness, one could still argue to label the higher projections as NP.
```
```ad-west
Sententital derivations also frequently employ functional heads like COMP, INFL and AGR. Those are also usually headed by closed lexical items, like determina and quantifiers for NumPs and DPs.
```

## Links
- [[Noun phrases are DPs.]]