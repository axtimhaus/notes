In [[Distributed Morphology]] semantic and phonological interpretation are largely separate. Semantic interpretation happens pre-spellout on abstract morphemes, phonological interpretation after spellout on concrete morphemes.

Generally, semantic/syntactic scope [[Mirror Principle|mirrors]] morphological constituent strcuture and order. In cyclic phonology, phonological properties of bases are directly passed to their derivatives. According to [[Kiparsky (2015)]], semantic scope mismatches related _directly_ to phonological anti-cyclicity and counter-cyclicity. 

He gives the following example for this: Case scopes over number and possessors (_\* John's in houses._).  However, in Finnish, counter-scopal order is not uncommon:

talo- i- ssa- si
house- Pl- Iness- 2Sg
'in your houses'

In Generative Lexicalist Morphology, the assumption is that in this case morphology generates a flat structure, that semantics interprets as Case scoping over Poss. In the English example (_\* John's in houses._) the nominal stem plus case do not form a domain on their own. The nominal stem is not combined with Case until the Possessor suffixes are added and both are introduced in the same cycle domain, they can select each other.

Because anti-scopal morpheme order calls into question semantic-morphological correspondence, this forces the analysis to commit to semantic scopal opacity within a stratum. (See [[Phonological representation consists of underlying, lexical and phonetic levels.]])