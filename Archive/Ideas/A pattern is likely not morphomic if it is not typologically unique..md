```ad-north
Morphomes are morphological phenomena that are historically isolated and remnants of no longer active patterns. Because they definitionally come about as historical accidents, these accidents should be unlikely to appear in unrelated languages.

See: [[Some morphological phenomena are independent of phonological or functional conditioning.]]
```

As [[Maiden (2021)]] points out, typological uniqueness is often mentioned as a diagnostic of morphomic patterns. Although it is statistically expected for a feature to be typologically unique, it is not an inherent property of morphomes. 
Although it is a good negative diagnostic: If a pattern is not unique to a single language (or through inheritance in a language family), it is unlikely to be morphomic. On the other side, typological uniqueness is not enough to diagnose morphomehood.

```ad-south
Because typological uniqueness is more of a symptom than a diagnosis, one should not classify a phenomenon as morphomic just on the basis of this one feature.
```
```ad-east
Typological uniqueness precludes many cross-linguistic studies definitionally, although research questions on a meta-level, e.g. in regards to naturalness, are still worthwhile to pursue typologically.
[[Herce (2019)]] uses this constraint to relegate uniqueness to "an unnecessary footnote to the definition of the morphome".
```
```ad-west
[[Aronoff (2016)]] argues that "morphomes have no reality outside the narrow system of the language itself". 
```

## Links
[[Some morphological phenomena are independent of phonological or functional conditioning.]]
[[Aronoff (2016)]]
[[Herce (2019)]]