---
alias: transcendental signified
---

There is an unstable relationship between signifier and signified. There is no guaranty that two speakers link the same signifier to the same signified concept. Because [[Language is always in a 'ceaseless flow of becoming'.]] one could even argue, it is impossible two speakers brought up in their own contexts would be able to agree on the same relation between the two except for surface features. 
To the _Tel Quel_ [[Poststructuralism|Poststructuralist]]s it is in the interest of dominant power holders to try to enforce a single and stable relationship between signifier and signified. This stable **transcendental signified** upholds dominant and surpresses revolutionary idiology. Transcendental signified do not refer to any signified, but are only there to uphold power. 
[[Jacques Derrida]] names God as an example of a transcendental, i.e. empty signified. This empty signification spawns signifiers like Lord or Almighty that themselves do only refer to the transcendental signified. 
In Institutions build on transcendental signifieds, the church on God, the law on Justice, the actual meaning of the signified becomes more and more unstable. By nature, it rebels against monologic signification.