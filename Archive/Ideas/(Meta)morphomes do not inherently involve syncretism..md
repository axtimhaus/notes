```ad-north
Syncretism is the appearance of functionally distinct morphs with identical form. (Meta)morphomes are generally definitionally expected to involve syncretism, with the morphomic pattern being syncretic.

See: [[Some morphological phenomena are independent of phonological or functional conditioning.]]
```

In Romanian, the verb "to hiccup" (1. conjugation) has in some dialects been contaminated by the semantically related "to swallow" (4. conjugation). This contamination is introducing the 4. conjugation into the contaminated verb, but not a coherent syncretic root, since the 4. conjugation features root allomorphy. This introduction leads to a incoherent, morphomic pattern, that is itself *less* syncretic than the 1. conjugation table before.

```ad-south
Morphomic patterns are not necessarily irregular in the form of their syncretic cells, but can also be morphomic because of the shape of an abstract pattern applied seemingly arbitrarily.
```
```ad-east
[[Maiden (2021)]] introduces the caveat, that still syncretism over *some set of cells* seems to be a necessary condition for a morphomic split to arise, if not a property of the morphomic pattern itself.
```
```ad-west
Syncretism belongs to a list of properties assumed to be inherent to morphomes.

See: [[(Meta)morphomes don't necessarily involve heterogeneity of form.]], [[A pattern is likely not morphomic if it is not typologically unique.]]
```

## Links 
[[Some morphological phenomena are independent of phonological or functional conditioning.]]
[[Maiden (2021)]]]
[[(Meta)morphomes don't necessarily involve heterogeneity of form.]]
[[A pattern is likely not morphomic if it is not typologically unique.]]