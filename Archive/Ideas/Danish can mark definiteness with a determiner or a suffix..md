Danish, Faroese, Icelandic, Norwegian, and Swedish have two ways of expressing definiteness: by a suffix on the noun or by a prenominal lexical determiner:

hest-**en**
horse-DEF
'the horse'

**den** røde hest
DEF red horse
'the red horse'

Adjectives block definite suffixation, and suffixation only appears on the noun:
\* røde hest-**en**
\* røde-**en** hest

With relative clauses, the interpretation changes depending on the definiteness marking:
Den hest der vandt løb-et, er til salg.
'The horse that won the race is for sale' (restrictive)

Hest-en, der vandt løb-et, er til salg.
'The horse, which won the race, is for sale.' (nonrestrictive)

Present participle nouns do not take a postnominal definiteness marker:
\* studerende-en
\* studerend-en

Proper names used as common nouns (e.g. _I know a Mary._) cannot take a postnominal marker with relative clauses to derive a nonrestrictive reading:
\* Maria-en, som har boet I København i mange år, flyttede til Malmö i januar.

[[Hankamer & Mikkelsen (2002)]] offer a rule based account of this data, [[Delsing (1993)]] explains this phenomenon through head movement from N to D.