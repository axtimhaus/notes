No constraint is violated without a good reason, i.e. avoiding the violation of a higher-ranked constraint.
Violability is in stark constrast to derivational models of phonology, where constraints are "hard" or inviolable.
