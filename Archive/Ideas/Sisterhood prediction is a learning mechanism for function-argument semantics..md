Sisterhood prediction is only dependent on the constituent parts of the phrase, not on the _actual_ sister node. The meaning of a phrase therefore in this conception is reliant on it's constituent parts, the linguistic structure thereof and the _predicted linguistic context_. 
By predicting the contents of it's sister node, the latent representation becomes (ideally) optimally receptive for possible complements. This limits the semantic space the merged phrase can occupy and defines the argument-type of the embedded phrase.
[[Function-argument structure provides a logical structure of quantified sentences.]]




