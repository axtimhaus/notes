In some [[Lexical Phonology]] frameworks, like those proposed by [[Paul Kiparsky]], strata "contain" phonological rules. [[Mohanan & Mohanan (1984)]] argue that it is the other way round: Phonological rules have the strata they apply to specified in the lexicon.

Related: [[Semantic scope mismatches relate to phonological anti-cyclicity and counter-cyclicity.]]