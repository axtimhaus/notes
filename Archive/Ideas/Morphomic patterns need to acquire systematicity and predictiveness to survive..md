```ad-north
Almost all morphomic patterns we encounter in one category, e.g. $\phi$-features, holds in all other categories (tense, case, etc.). This systematicity allows speakers to generalise and predict the application of the morphomic pattern across multiple words. Predictiveness and systematicity have been postulated to be inherent to morphomes.

See: [[Some morphological phenomena are independent of phonological or functional conditioning.]]
```

[[Maiden (2021)]] argues that morphomes are neither inherently systematic, nor inherently predictive. They merely acquire these features to survive, i.e. become psychologically manifested in a speakers grammar. 
Maiden describes a situation where a morphomic pattern jumps from a phonologically embedded context to an unrelated lexeme, but not yet to all other lexemes of that conjugation, class, or similar category. This intermediary form would fulfill all criteria of morphomicity, but lacks systematicity and predictiveness.
The shift from a morphomic paradigm towards 
[[Speakers 'defend' morphomic patterns.]]

```ad-south
If systematicity and predictiveness are not inherent criteria, suddenly closed lexical classes like many European "to be"-paradigms and the Italian avera-essere-sapera trifecta could also be called morphomic.
```
```ad-east

```
```ad-west
[[Maiden (2021)]] reexamines, among systematicity and predictiveness, also many other commonly named criteria for morphomic patterns.

See: [[(Meta)morphomes don't necessarily involve heterogeneity of form.]], [[(Meta)morphomes do not inherently involve syncretism.]], [[A pattern is likely not morphomic if it is not typologically unique.]]
```

## Links
[[Some morphological phenomena are independent of phonological or functional conditioning.]]
[[A pattern is likely not morphomic if it is not typologically unique.]]
[[(Meta)morphomes don't necessarily involve heterogeneity of form.]]
[[(Meta)morphomes do not inherently involve syncretism.]]