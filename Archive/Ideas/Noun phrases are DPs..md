```ad-north
Noun phrases have higher projections than NPs, the highest of which is the DP.

See: [[Number and definiteness have functional heads.]]
```

[[Ritter (1991)]] argues for functional projections above the NP within a noun phrase. The highest construction here is the DP, which marks for definiteness, below which NumP (NBRP) marks number.

```ad-south
Number and definiteness need a mechanism to percolate their features downwards to trigger Agree with the NP.
```
```ad-east
Headedness is a multi-dimensional concept, so the semantic content of NPs could also be reason to still label noun phrases as DPs.
```
```ad-west
Sentences are CPs.
```

## Links
- [[Number and definiteness have functional heads.]]