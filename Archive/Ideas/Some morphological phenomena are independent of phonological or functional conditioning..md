---
alias: morphome
---

The notion of a morphome, to be distinguished from a morpheme, which was introduced by [[Aronoff (1994)]], has been widely accepted, although its usefulness as a concept is questionable. Morphomes are morphological patterns that are no longer synchronically explainable. The disruption these defunct pattern cause cannot be attributed to patterns outside of morphology. They are usually the result of historical, extramorphological changes to a language that leave remnants of a defunct pattern stranded in synchronic morphology.
In contrast to morphemes the relation between form and meaning is anisomorphic.
Morphomes do still play a part in acting as templates for morphological innovation.