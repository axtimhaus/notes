In [[Lexical Phonology]], rules that require morphological/lexical information must apply in the lexicon. Those that do no can apply either lexical or postlexical (or both). For example trisyllabic shortening in English must apply in the lexicon, as it requires morphological information: It appears with class I affixes as in _divin+ity_, but not with derivational affixes like _maiden#hood_. In contrast, flapping does not require morphological information and applies across words: _see you$[$D$]$omorrow_. 

The lexicon consists of ordered strata (or levels). The lexicon contains a set of morphological rules of affixation or compounding and these rules are specific to their domain in terms of lexical strata. For example, in English class I affixes (_-ity,-ion, in-_) attach at stratum 1, class II affixes (_-ness, -hood, un-_) attach at stratum 2. The domains of phonological rules are characterised in terms of these strata: The domain of word stress rules is stratum 1, making class II affixes stress neutral. The domain of trisyllabic shortening is also stratum 1, allowing it to apply to _divinity_, but not _maidenhood_.

| **Stratum 1**       |                |                        |
| --------------- | -------------- | ---------------------- |
| \[divīn], -iti] | \[mǣden], -hud] | Underlying             |
| ---             | ---            | Phonological rules     |
| \[\[divīn]iti]    | ---            | Affixation             |
| \[\[divin]iti]     | ---            | Trisyllabic shortening |
| ---             | ---            | Other rules            |
| **Stratum 2** |             |                    |
| ---       | ---         | Phonological rules |
| ---       | \[\[mǣden]hud] | Affixation         |
| ---       | ---         | Phonological rules |
| \[diviniti]  | \[meydenhud]   |                    |

The _ŋ~ŋg_ alternation in *long* to _longer_, but not in _longing_ can also be explained using strata and the Bracket Erasure Convention (BEC): At the end of a stratum, all internal brackets are erased. 
The domain of nasal assimilation and g-deletion is stratum 2.

| **Stratum 1** |               |                |                    |
| ------------- | ------------- | -------------- | ------------------ |
| \[long]       | \[long], -er] | \[long], -ing] | Underlying         |
| ---           | ---           | ---            | Phonological rules |
| ---           | \[\[long]er]  | ---            | Affixation         |
| ---           | ---           | ---            | Phonological rules |
| \[long]       | \[longer]     | \[long], -ing] | BEC                |
| **Stratum 2** |               |                |                    |
| \[long]       | \[longer]     | \[long], -ing] | Nasal assimilation |
| \[loŋ]        | ---           | \[loŋ], -iŋ]   | g-deletion         |
| ---           | ---           | \[\[loŋ]iŋ]    | Affixation         |

