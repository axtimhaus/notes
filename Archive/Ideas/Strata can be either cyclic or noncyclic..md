[[Mohanan & Mohanan (1984)]] argue that not every phonological rule obeys Strict Cyclicity. Cyclicity or lack thereof is a feature of strata. Cyclic strata can go through multiple cycles of morphological and phonological rule application, e.g. compounding.
Noncyclic strata can only undergo each rule application once, e.g. case assignment.

Related [[Morphophonological properties are hierarchically ordered.]]