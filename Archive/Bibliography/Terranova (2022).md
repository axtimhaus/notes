```ad-tldr
title: Tl;dr

The internet as it once existed has gone underground. The Corporate Platform Complex has all but completely subsumed the decentralised web of forums and websites of the 1990s. Terranove sketches the history of this change and how capitalism colonised a space that was meant to be shared by everyone.
```

````ad-cite
title: Bibtex
collapse: close
```bibtex
@book{terranova2022after,
  title={After the Internet: Digital Networks between Capital and the Common},
  author={Terranova, Tiziana},
  series={Intervention},
  volume={33},
  year={2022},
  publisher={Semiotext(e)}
}
```
````
[[Tiziana Terranova]]



## Introduction
- The internet moved from decentralised communication to **The Corporate Platform Complex** (CPC).
- The CPC grew during the pandemic, while the economy shrank.
- Metaverse aims to make pandemic experience universal.
- The internet as a *"set of interoperable network protocols governed by a series of public and/or voluntary non-profit organisations"* vs. the internet as *"gated digital communities with strong ownership of data, software and infrastructure"*.
- Close relationship with financial capital
- Monopolies on ecosystems composed of smaller economic agents (e.g. app stores)
- **[[The user is an addict.]]** (from "master of the machine")
- Interfaces are designed to maximise engagement.
- The internet enables fake news, conspiracy theories and hate speech (see [[The cost of information changes its nature and how people interact with it.]])
- "**The internet stopped being a possibility.**" ([[Steyerl (2013)]])
	- The internet is a _residual technology_, barely noticed by the tech giants.
	- Public protocols are burried under corporate ones.
- Native subcultures of the internet have gone underground (dark web).
- **[[The internet is undead.]]**: The internet has been _subsumed_, not dissolved.
- Strict asymmetry between servers and clients.
- "the platform economy turns digital labor into casual and precarious work"

### A Much-Abridged History of Networks
- Networks have three main properties:
	- abstract mathematical symbols, 
	- observable empirical objects,
	- engineered artificial systems
- The internet is a network of networks (inter-networks)
- The internet combines (information) networks with computation.
- The subcultures of the 1980s and 90s were predominantly white male nerds.
- Mostly (male white) North American users championed a cyber-libertarianism
	- "Information wants to be free."
- Mostly (male white) European DIY anarchists saw the internet as a potential for autonomous forms of political organisation
- Queer, black and asian technocultured also existed.
- The internet was opened for corporate use in the 1990s. 
- Dot-com companies styled themselved as "no collar" workers (Andrew Ross)
	- Influenced by counter culture and ivy-league campus life
	- Informal hierarchies, work as play, personalisation of relationships
- "The California Ideology" (Andy Cameron, Richard Barbook)
- Dot-com bubble burst in 2001
- Followed by Web 2.0: Harnessing free labour from users
	- Less impacted by 2008 financial crisis
- Platform giants hatched between two financial crises
- Sustained by financial capital until profitable
- Rise of the start-up corporate culture
	- Technical wizardry good (coded as male); linguistic, humanistic and social knowledge bad (coded as female)
	- Cult of founders
	- Shareholder employees as a social class
	- Affective investment into company's future
- Consolidation of _network effects_ to form monopolies.
- Use of open source software for private accumulation of wealth.
- Cooperation with military and government.
- Protest against under-regulated taking over of public functions.
- [[The CPC is agitated by a deep state of unrest.]]

### Between the Market and the Common(s)
- The process of subsumption implicated a series of potentials and conflicts
- The progressive revolution was a counter-revolution against the Common
- Attention economy: Communicative capitalisms circuits of drive (Jodi Dean)
- The Free and Open Source software movement, Wikipedia, et al.  questioned that market economics is the only efficient way of organising individual actions.
- The Commons connect to so-called "red cybernetics".
- Chinas Communist Party seamlessly adopted platform capitalism.
	- "Real socialism turned out to be just another kind of capitalist economy." (Cornelius Castoriadis)
- "Tragedy of the commons" (Garrett Hardin)
	- Failure of individuals to care for shared property.
	- Evokes similarities to european colonialism against native americans.
	- Two critiques: 
		- Reanalysis of value of the commons (Elinor Olstrom)
		- Marxian analysis: _living knowledges, social cooperation_
- Commons-based peer production is made possible by the nonrival properties of information.
	- Couldn't compete with venture capital founded companies harnessing network effects
	- [[Capitalism does not optimise distribution of nonrival resources.]]
- Model of peers exchanging information overwhelmed by social media.
- Platformisation turnes participation into revenue
- Corporations steal the Common and transform it into property.
-