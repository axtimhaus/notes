---
alias: "(Kiparsky 2000)"
---
# Opacity and cyclicity
#to-read 

```ad-tldr
title: Tl;dr

Here be dragons.
```

````ad-cite
title: Bibtex
collapse: close
```bibtex
@article{kiparsky2000opacity,
  title={Opacity and cyclicity},
  author={Kiparsky, Paul},
  year={2000},
  publisher={Walter de Gruyter},
  address={Berlin/New York}
}
```
````
[[Paul Kiparsky]]
Attachments: [[kiparsky2000opacity.pdf]]