---
alias: "(Delsing 1993)"
---

```ad-tldr
title: Tl;dr

Here be dragons.
```

````ad-cite
title: Bibtex
collapse: close
```bibtex
@article{delsing1993attributive,
  title={On attributive adjectives in Scandinavian and other languages},
  author={Delsing, Lars-Olof},
  journal={Studia linguistica},
  volume={47},
  number={2},
  pages={105--125},
  year={1993},
  publisher={Wiley Online Library}
}
```
````
[[Lars-Olof Delsing]]
Attachments: [[delsing1993attributive.pdf]]

[[The definite suffix in Danish is the result of head movement of N to D.]]
