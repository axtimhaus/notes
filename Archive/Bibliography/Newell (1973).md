---
alias: "(Newell 1973)"
---

```ad-tldr
title: Tl;dr

Psychological research is trapped in a data collection hell. Without a theoretical practice of mechanistically tying data into full processing structures, progress in cognitive psychology is unlikely to advance.
```

````ad-cite
title: Bibtex
collapse: close
```bibtex
@article{newell1973you,
  title={You can't play 20 questions with nature and win: Projective comments on the papers of this symposium},
  author={Newell, Allen},
  year={1973}
}
```
````
[[Allen Newell]]
Attachments: [[newell1973you.pdf]]

- Increasing number of clarifying experiments that expand our understanding of a phenomenon, but not on overarching issues.
- Phenomena are largely treated as separate and beget separate models.
- Research questions are often framed as binary to reduce the complexity of experiments.
- Clarifying research is not cumulative. Are we getting anywhere?
- [[Science does not progress through 'clarifying' experiments.]]
