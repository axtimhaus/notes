---
alias: "(Mohanan & Mohanan 1984)"
---

```ad-tldr
title: Tl;dr

Here be dragons.
```

````ad-cite
title: Bibtex
collapse: close
```bibtex
@article{mohanan1984lexical,
  title={Lexical phonology of the consonant system in Malayalam},
  author={Mohanan, Karuvannur P and Mohanan, Tara},
  journal={Linguistic inquiry},
  pages={575--602},
  year={1984},
  publisher={JSTOR}
}
```
````
[[K. P. Mohanan]], [[Tara Mohanan]]
Attachments: [[mohanan1984lexical.pdf]]


This paper argues the following two positions:
1. [[The phonological rule system is independent of modules.]]
2. [[Strata can be either cyclic or noncyclic.]]

Lexical phonology has the following properties:
1. [[Phonological rules may apply in the lexicon.]]
2. Morphological information can be expresses in terms of brackets and lexical strata.
3. [[Phonological representation consists of underlying, lexical and phonetic levels.]]