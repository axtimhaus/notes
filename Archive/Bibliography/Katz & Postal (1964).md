Katz, J.J. and Postal, P. M., 1964. An Integrated Theory of Linguistic Descriptions. _Cambridge, Massachusetts_.
[[Jerrold J. Katz]], [[Paul M. Postal]]