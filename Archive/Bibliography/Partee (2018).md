Partee, B. 2018. 'Formal semantics and pragmatics: Origin, issues, impact' \[Presentation\], _Cognitive Science_. Dartmouth College. 13 April.
[[Barbara Partee]]

Early 19th century attitude towards logic/semantics: [[Natural language is too complex to be modelled by formal logic.]]
Chomsky: [[Syntax is autonomous from semantics.]]
Katz & Fodor: [[Semantic feature bundles can explain contrast between readings, but not meaning.]]
Chomsky: [[Syntax maps semantic deep structure to phonologic surface structure.]]
Leibniz: [[A universal languages is a formal framework.]]
Frege: [[Function-argument structure provides a logical structure of quantified sentences.]]

