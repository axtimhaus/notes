Rehbein, I., Ruppenhofer, J. and Zimmermann, V., 2018. A harmonised testsuite for POS tagging of German social media data.
[[Ines Rehbein]], [[Josef Ruppenhofer]], [[Victor Zimmermann]]