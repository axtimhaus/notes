---
alias: "(Kiparsky 2015)"
---

```ad-tldr
title: Tl;dr

Evidence from contextual morphology seems to favour lexicalist morphology over distributed morphology.
```

````ad-cite
title: Bibtex
collapse: close
```bibtex
@article{kiparsky2015approaches,
  title={Two approaches to locality in morphology},
  author={Kiparsky, Paul},
  year={2015}
}
```
````
[[Paul Kiparsky]]
Attachments: [[kiparsky2015approaches.pdf]]

## (1) Core assumptions of Lexical Morphology:
- Morpheme based
- Generative (Merging of properties of parts.)
- Lexicalist

## (2) Two layers of morphology.
The lexicon feeds into stem morphology (which feeds into phonology and semantics).
Stem morphology feeds into word morphology.
Word morphology feeds into syntax.
All layers are cyclic and interact with phonology and semantics.

## (3) Motivation for lexicalism.
- Displacement of morphemes in words is local.
- No syntactic movement out of words, or into words (no discontinuous words)
- No traces inside words.
- No anaphoric dependencies.
- No sentences as constiuents.
- No obligatory control.
- No structural case.
- Subject to locality constraints.
- Contexts limited by adjacency and cyclicity.
- Confined by phonological contexts (syntax is phonology free)
- Level-ordered phonology.
- Syntax and morphology are separate modules.

## (4) Morphemes
- Minimal lexical entries.
- Phonological, morphological, morphosyntactic and semantic information.
- Allomorphy, allosemy.

## (5) Example: the plural suffix
- Phonological form
- Meaning
- Categorial and subcategorisation properties

## (6) Morphology
- No rules inserting morphemes

## (20) Case
- Bracketing erasure is independently motivated in lexicalist morphology.
- Intrinstic plurals recieve theta-role in compouns, derived plurals do not: people-oriented, \* persons-oriented.
- Intrinsic plural can be verbalised: they peopled/\*personsed
- Not clear what the motivation in DM is.
