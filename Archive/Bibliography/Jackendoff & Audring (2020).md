```ad-tldr
title: Tl;dr

Here be dragons.
```

````ad-cite
title: Bibtex
collapse: close
```bibtex
@book{jackendoff2020texture,
  title={The texture of the lexicon: relational morphology and the parallel architecture},
  author={Jackendoff, Ray and Audring, Jenny},
  year={2020},
  publisher={Oxford University Press, USA}
}
```
````
[[Ray Jackendoff]], [[Jenny Audring]]

## Situating Morphology
- [[Relational Morphology]]: Ensemble of Morphology, lexicon and cognition.
- Language is a finite computational system yielding an infinity of expressions.
- Patterns are either productive or unproductive.
- Unproductive patterns are usually consigned into the lexicon.
- Lexicon as a ragbag of idiosyncrasies.
- Example -(t)ion suffix: seems like productive pattern, but not all verbs can take suffix and not all words ending in -(t)ion have a verb base.
- Such patterns cannot be captured by rules. *((but maybe by ranked constraints?))*
- Instead lexical rules / schemas
- Does linguistic theory need two principles, one for productive patterns and one for schemas?
- Idioms are unproductive and are usually relegated to the lexicon, but adhere to some grammattical patterns (-s suffix in "It's rainging cats and dogs.")
- Is the plural form both listed in the lexicon and generated by the grammar?
- Productive rules can be used **generatively** (novel forms) and **relationally** (generalisations over the lexicon).
- **Relational Hypothesis**
	- **[[All rules can be used relationally.]]**
	- **[[Only a subset of rules can be used generatively.]]**