```ad-tldr
title: Tl;dr

```

````ad-cite
title: Bibtex
collapse: close
```bibtex
@article{baroni2014frege,
  title={Frege in space: A program for compositional distributional semantics},
  author={Baroni, Marco and Bernardi, Raffaella and Zamparelli, Roberto},
  journal={Linguistic Issues in language technology},
  volume={9},
  pages={241--346},
  year={2014}
}
```
````

[[Marco Baroni]], [[Raffaella Bernardi]], [[Roberto Zamparelli]]

## Introduction
- Semantic [[Compositionality]]: [[The meaning of a complex expression is a function of the meanings of its parts.]]
- Denotational meaning: Is an utterance truthful with respect to a model of the world?
- Natural language semantics/formal semantics
- Compositionality does not apply to idioms
- But compositional meanings of idioms still retrievable
- Compositionality is a primitive in human language.
- In binary trees, one sister is considered the function, the other the argument.
- Lexical ambiguity mostly excluded from sematic study.
- Many cases of context-driven polysemy are systematic.
- [[Pustejovsky (1995)]]: [[Generative Lexicon]] 
- Success of a theory must be measured outside of a lab.
- Choosing the wrong meaning does not lead to falsity, but to non-sensicality.
- 