---
alias: "(Stonham 2007)"
---
# Nuuchahnulth Double Reduplication and Stratal Optimality Theory
#to-read

```ad-tldr
title: Tl;dr

Nuu-chah-nulth features two kinds of reduplication: Pluralisation reduplication and affix triggered reduplication. Both can appear simultaneously on the same word, leading to double reduplication. Stonham presents a Stratal OT account to account for their interaction.
```

````ad-cite
title: Bibtex
collapse: close
```bibtex
@article{stonham2007nuuchahnulth,
  title={Nuuchahnulth double reduplication and stratal optimality theory},
  author={Stonham, John},
  journal={Canadian Journal of Linguistics/Revue canadienne de linguistique},
  volume={52},
  number={1-2},
  pages={105--130},
  year={2007},
  publisher={Cambridge University Press}
}
```
````
[[John Stonham]]
Attachments: [[stonham2007nuuchahnulth.pdf]]