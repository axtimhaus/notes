---
alias: "(Schmid 2019)"
---

```ad-tldr
title: Tl;dr

Modern education presumes a Constructivist modelling approach. Construction and Reconstruction map closely to common machine learning practices, but Deconstruction needs meta data to evaluate conflicts and resolve them. A Constructivist Machine Learning is a paradigm based on meta data.
```

````ad-cite
title: Bibtex
collapse: close
```bibtex
@inproceedings{schmid2019deconstructing,
  title={Deconstructing the Final Frontier of Artificial Intelligence: Five Theses for a Constructivist Machine Learning.},
  author={Schmid, Thomas},
  booktitle={aaai Spring Symposium: Combining Machine Learning with Knowledge Engineering},
  year={2019}
}
```
````
[[Thomas Schmid]]
Attachments: [[schmid2019deconstructing.pdf]]


## notes
- **Ambiguity** and **diversity** in cognition final frontier in AI
- Modern machine learning adaptive systems
- Deep neural networks don't allow abstraction and differentiation
- Statistical models don't reflect or yield structures and strategies of human thinking.
- Realign machine learning with constructivism: [[Constructivist Machine Learning]]
- classifiers are models with limited validity
- **meta data-based machine learning**
- No convincing answers to ambiguous environment challenges
- No concepts allowing contradictory judgements.
- [[Constructivism]]: 
	- Cognition is highly individual.
	- Active role of humans in perception.
	- [[There is no such thing as a human-independet reality.]]
- **Principle 1: [[The key component of cognitive functionality is a model.]]**
	- Artifical neural network: neuron often considered key component of learning system
	- Cognitive functions are acquired skills
	- Cognitive functions as mental models
	- Mental models as hypothetical constructs, ordered hierarchically
	- allow predictions about social and physical environment
	- not static, underlay continuous modifications
	- Philosophy: Relationship between image and origin of the image.
	- _General Model Theory_
		- Model limites to specific subjects, temporal ranges and purposes.
		- Limitations a matter of fact, not definition
	- Ambiguous model with unknown validity becomes number of models with limited validity.
- **Principle 2: [[Learning constitutes from constructing, reconstructing of deconstructing models.]]**
	- Constructivism is a dominant idea in modern education.
	- Assumption: Humans acquire knowledge actively and individually
	- Processes of knowledge acquisition:
		- Construction
		- Reconstruction
		- Deconstruction
	- Construction:
		- Creation, Innovation, Production
		- Variations, Combinations, Knowledge Transfer
		- Unsupervised Learning
	 - Reconstruction:
		- Application, Repetition, Imitation
		- Order, Patterns, Models
		- Supervised Learning
	  - Deconstruction:
		- Reconsideration, Doubt, Modification
		- Omissions, Additions, Defective Parts
		- Very few algorithmic deconstruction processes
	- Machine Learning mirrors education concepts.
- ***Principle 3: [[Deconstructing models computationally requires model-based meta data.]]**
	- To implement deconstruction models need to be held available.
	- Novel models need to be unambiguously identifiable by calculation or operations.
	- Machine learning is learning from training vectors.
	- A **Stachowiak-like training vector** has three additional pragmatic features:
		- $V^* = (V,T_V,\Sigma_V,Z_V)$
		- $T_V$: point $\tau$ in time
		- $\Sigma_V$: subset of infinite set of model subjects $\Sigma$
		- $Z_V$: subset of infinite set of model purposes $Z$
	- A **Stachowiak-like model** is an approximation over $n$ training vectors $V$:
		- $M^* = (M,T_M,\Sigma_M,Z_M)$
		- $T_M$: is the time span between the minimal $\tau$ and maximal $\tau$ in the training set.
		- $\Sigma_M$: Union of the sets of model subjects of the training set.
		- $Z_M$: Union of the sets of model purposes of the training set.
	- By comparing the meta data of two models possible conflicts can be anticipated.
- **Principle 4: Deconstructing models implies to either abstract, differentiate or discard them.**
	- 