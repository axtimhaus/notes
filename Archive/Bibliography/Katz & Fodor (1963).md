Katz, J.J. and Fodor, J.A., 1963. The structure of a semantic theory. _language_, _39_(2), pp.170-210.
[[Jerrold J. Katz]], [[Jerry A. Fodor]]