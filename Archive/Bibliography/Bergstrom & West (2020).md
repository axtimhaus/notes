West, J.D. and Bergstrom, C.T., 2020. _Calling bullshit: the art of scepticism in a data-driven world_. Penguin UK.
[[Carl T. Bergstrom]], [[Jevin D. West]]

[[The cost of information changes its nature and how people interact with it.]]
