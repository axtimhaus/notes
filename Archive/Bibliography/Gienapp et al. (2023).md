Gienapp, L., Zimmermann, V., Kircheis, W., Sievers, B., Stein, B. and Potthast, M., 2023. _An Empirical Study of Text Reuse in Scientific Open-Access Publications_. Unpublished.
[[Lukas Gienapp]], [[Victor Zimmermann]], [[Wolfgang Kircheis]], [[Bjarne Sievers]], [[Benno Stein]], [[Martin Potthast]]

- Text reuse essential to scientific writing.
- Associated with scientific misconduct.
- Little broad research into topic.
- Webis-STEREO-21 corpus
	- 30 million instances
- Diverse characteristics
- Cliché