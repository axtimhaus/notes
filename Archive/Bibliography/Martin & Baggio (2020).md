---
alias: "(Martin & Baggio 2020)"
---

```ad-tldr
title: Tl;dr

Here be dragons.
```

````ad-cite
title: Bibtex
collapse: close
```bibtex
@article{martin2020modelling,
author = {Martin, Andrea E.  and Baggio, Giosuè },
title = {Modelling meaning composition from formalism to mechanism},
journal = {Philosophical Transactions of the Royal Society B: Biological Sciences},
volume = {375},
number = {1791},
year = {2020},
doi = {10.1098/rstb.2019.0298},
URL = {https://royalsocietypublishing.org/doi/abs/10.1098/rstb.2019.0298},
eprint = {https://royalsocietypublishing.org/doi/pdf/10.1098/rstb.2019.0298},
abstract = { Human thought and language have extraordinary expressive power because meaningful parts can be assembled into more complex semantic structures. This partly underlies our ability to compose meanings into endlessly novel configurations, and sets us apart from other species and current computing devices. Crucially, human behaviour, including language use and linguistic data, indicates that composing parts into complex structures does not threaten the existence of constituent parts as independent units in the system: parts and wholes exist simultaneously yet independently from one another in the mind and brain. This independence is evident in human behaviour, but it seems at odds with what is known about the brain's exquisite sensitivity to statistical patterns: everyday language use is productive and expressive precisely because it can go beyond statistical regularities. Formal theories in philosophy and linguistics explain this fact by assuming that language and thought are compositional: systems of representations that separate a variable (or role) from its values (fillers), such that the meaning of a complex expression is a function of the values assigned to the variables. The debate on whether and how compositional systems could be implemented in minds, brains and machines remains vigorous. However, it has not yet resulted in mechanistic models of semantic composition: how, then, are the constituents of thoughts and sentences put and held together? We review and discuss current efforts at understanding this problem, and we chart possible routes for future research. This article is part of the theme issue ‘Towards mechanistic models of meaning composition’. }
}
```
````
[[Andrea E. Martin]], [[Giosuè Baggio]]
Attachments: [[martin2020modelling.pdf]]

## Meaning composition
- Traditional view: function application
- Frege's conjecture: Meaning composition always consists in the 'saturation of an unsaturated meaning component'
- Lambda calculus produces formulae in predicate logic that still need to be _interpreted_.
- Does this yield meaning?
- Strong version of [[Compositionality]]: composition and interpretation closely mirror each other.
- Lexical meaning is just a placeholder for a typed entity.
- Reduces to placeholders of typed entities.
- Lexical meanings are richer data structures than lambda terms in formal semantics (e.g. [[Generative Lexicon]])
- Rich, internally elaborated data structures (qualia, event, argument structures; distributional vectors) are difficult (impossible) to describe in terms of function application. [[Natural language is too complex to be modelled by formal logic.]]
- What is composition? Simple or complex? One or many operations? Autonomous or fully reliant on syntax?
	- In [[Zimmermann (2023)]]: Complex, one operation, fully reliant.
- Two general requirements for compositionality:
	- lexical meanings are rich semantic representations and can account for human lexical competence.
	- meaning composition upholds independence of predicates and arguments
- Long term goal: integrative framework in which theories of meaning composition are seamlessly connected.