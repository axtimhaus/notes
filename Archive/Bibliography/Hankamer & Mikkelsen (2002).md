---
alias: "(Hankamer & Mikkelsen 2002)"
---

```ad-tldr
title: Tl;dr

Here be dragons.
```

````ad-cite
title: Bibtex
collapse: close
```bibtex
@article{hankamer2002morphological,
  title={A morphological analysis of definite nouns in Danish},
  author={Hankamer, Jorge and Mikkelsen, Line},
  journal={Journal of Germanic Linguistics},
  volume={14},
  number={2},
  pages={137--175},
  year={2002},
  publisher={Cambridge University Press}
}
```
````
[[Jorge Hankamer]], [[Line Mikkelsen]]
Attachments: [[hankamer2002morphological.pdf]]

## Contents
[[Danish can mark definiteness with a determiner or a suffix.]]
[[Head movement of N to D is not possible in Danish.]]

