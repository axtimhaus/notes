---
alias: "(Allen 2000)"
---

# Intertextuality

```ad-tldr
title: Tl;dr

Meaning of text can only be understood in relation to other text.
```

````ad-cite
title: Bibtex
collapse: close
```bibtex
@book{allen2000intertextuality,
  title={Intertextuality},
  author={Allen, Graham},
  year={2000},
  edition={2nd edn.}
  publisher={Routledge}
}
```
````
[[Graham Allen]]

- God is the author of two books: The bible and the book of nature.
- [[Language is always in a 'ceaseless flow of becoming'.]]
- [[Ideology maintains power by enforcing a stable relationship between signifier and signified.]]
- [[Text is always in a state of production.]]
- The subject of enunciation highlights the role of the speaker, the subject of utterance is independent of the speaker. 
- Society is always in conflict between monologic and dialogic forces. 
- [[Poetic discourse allows us to escape prohibition.]]
- Intertextual relations are passionate: split between reason and desire. (Kristeva)
- The _chora_, a pior to language, bubbles up in poetry. (Kristeva)
- Phenotext: Part of the text and conscious communication.
- Genotext: Part of the text and subconscious.
- The subject in writing is not identical to the subject itself.
- The transposition from one sign system to another destroys the old position and forms the new one.
- Jouissance describes a playful interaction with dialogic language.
- A work occupies physical space, a text is a methodological field.
- One cannot count up texts.
- **The intertext is not the source of the text, but a text itself.**
- **[[Text does not carry meaning.]]**
- 