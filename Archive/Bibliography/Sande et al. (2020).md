---
alias: "(Sande et al. 2020)"
---

```ad-tldr
title: Tl;dr

Here be dragons.
```

````ad-cite
title: Bibtex
collapse: close
```bibtex
@article{sande2020cophonologies,
  title={Cophonologies by ph (r) ase},
  author={Sande, Hannah and Jenks, Peter and Inkelas, Sharon},
  journal={Natural Language \& Linguistic Theory},
  volume={38},
  pages={1211--1261},
  year={2020},
  publisher={Springer}
}
```
````
[[Hannah Sande]], [[Peter Jenks]], [[Sharon Inkelas]]
Attachments: [[sande2020cophonologies.pdf]]

# Cophonologies by Ph(r)ase
