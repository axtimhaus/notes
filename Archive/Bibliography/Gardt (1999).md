Gardt, A., 1999. _Geschichte der Sprachwissenschaft in Deutschland: vom Mittelalter bis ins 20. Jahrhundert_. Walter de Gruyter.
[[Andreas Gardt]]

## Contents
### Linguistics in the Middle Ages
[[The word 'deutsch' goes back to the Germanic word for people.]] This was much to the displeasure of many medieval scholars, who wanted to see German on the same level as Latin, which is considered one of the three holy languages. We can see this desire as many [[Medieval popular etymologies trace 'deutsch' back to Noah and his descendants.]] 
We see grammars of this period focus on a sytemisation from reality to language, away from pure descriptions of individual languages (e.g. Latin). This new brand of grammar is called universalist: [[Thought and perception are universal.]] and only the perception to language-step is specific to individual languages. Since we are all endowed with the same perception and thought (broadly), differences in languages are accidental and insignificant. 
A popular tribe of grammarians in Northern Europe were the Modists/Speculative Grammarians: for them [[Language is a parallel system of modes.]] The wish to directly link language to reality, with little abstraction, remains into the 18th century, but the arbitrariness of the sign remains well established despite Modist objections.

### 16th Century
In the early modern period vernaculars win ground against Latin across Europe, including in the attention spent on their linguistic description. Patriotic notions of language are also on the rise: [[Language differences derive from different language spirits.]]
German starts to be used in schools beyond teaching Latin, which leads to language guides for proper writing: It becomes apparent that [[The standard variety of a language is shaped by high status exemplars.]]