---
alias: "(Maiden 2021)"
---
# The morphome

```ad-tldr
title: Tl;dr

Morphomic patterns may be more pervasive than assumed in the relevant literature. Maiden re-examines commonly assumed inherent properties of morphomes, like phonological heterogeneity, systematicity, predictiveness, typological uniqueness and syncretism, and by weakening these diagnostics invites many more phenomena under the umbrella of the morphome.
```

````ad-cite
title: Bibtex
collapse: close
```bibtex
@article{maiden2021morphome,
  title={The morphome},
  author={Maiden, Martin},
  journal={Annual Review of Linguistics},
  volume={7},
  pages={89--108},
  year={2021},
  publisher={Annual Reviews}
}
```
````
[[Martin Maiden]]
Attachments: [[maiden2021morphome.pdf]]

## Ideas
- [[Some morphological phenomena are independent of phonological or functional conditioning.]]
- [[A pattern is likely not morphomic if it is not typologically unique.]]
- [[(Meta)morphomes don't necessarily involve heterogeneity of form.]]
- [[(Meta)morphomes do not inherently involve syncretism.]]
- [[Morphomic patterns need to acquire systematicity and predictiveness to survive.]]
- [[Speakers 'defend' morphomic patterns.]]

## Notes
### Aims and perspectives
**[[Some morphological phenomena are independent of phonological or functional conditioning.]]**

- First postulated in [[Aronoff (1994)]].
- Value of notion controversial, existence is not.
- Lots of properties, some contingent, some inherent
- Morpheme: form-meaning pair, has phonological form, Saussurean sign
- "morphomic pattern"
- anisomorphic relation between form and meaning
- involve disruptions that cannot be attributed to patterns outside of morphology
- historical explanations, but synchronically inherently unnatural
- original determinants defunct, effects remain entrenched and stranded
- Latin third stem
- accidental result of extramorphological motivated changes
- act as abstract templates that guide and constrain morphological innovation

### Some assumptions about fundamental characteristics of the morphome
**We make can agree on four properties of morphomes generally assumed in the relevant literature: **
1. {a} **typological uniqueness**
2. **phonological heterogeneity**
3. **syncretism**
4. **systematicity and predictiveness**  

**These properties are generally assumed to be inherent to morphemes, but [[Maiden (2021)]] examines if they also could be just contingent.**

- Morphomes are fundamentally arbitrary in nature.
- "distributional pattern over an incoherent, irreducible set of features or values for those features"
- No reality outside system of the language
- Typologically unique
	- Same morphomes in different languages are probably not morphomes
- Phonologically heterogeneous
- No syntactic motivation
- systemic: not random, not limited to a small or arbitrary set of lexems, always predictable on some basis

### Are morphomes by definition typologically unique?
[[A pattern is likely not morphomic if it is not typologically unique.]]
- Typological uniqueness is a negative diagnostic
	- If the pattern is not morphomic, it should be typologically diverse.

### Do (meta)morphomes necessarily involve heterogeneity of form?
[[(Meta)morphomes don't necessarily involve heterogeneity of form.]]
- Metamorphome: systematic mapping of a set of heterogeneous forms onto a set of heterogeneous functions
	- partitioning of sets of cells within paradigms
- 


### Is syncretism an inherent property of morphomic structures?
[[(Meta)morphomes do not inherently involve syncretism.]]



### Are systematicity and predictiveness inherent properties of morphomic structures?
[[Morphomic patterns need to acquire systematicity and predictiveness to survive.]]
[[Speakers 'defend' morphomic patterns.]]