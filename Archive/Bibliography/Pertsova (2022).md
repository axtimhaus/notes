---
alias: "(Pertsova 2022)"
---
# A case for a binary feature underlying clusivity: the possibility of ABA
#to-read

```ad-tldr
title: Tl;dr

Here be dragons.
```

````ad-cite
title: Bibtex
collapse: close
```bibtex
@article{pertsova2022case,
  title={A case for a binary feature underlying clusivity: the possibility of ABA},
  author={Pertsova, Katya},
  journal={Morphology},
  volume={32},
  number={4},
  pages={389--429},
  year={2022},
  publisher={Springer}
}
```
````
[[Katya Pertsova]]
Attachments: [[pertsova2022clusivity.pdf]]