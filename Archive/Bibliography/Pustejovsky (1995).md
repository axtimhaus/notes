Pustejovsky, J., 1995. The Generative Lexicon.
[[James Pustejovsky]]

Also: Pustejovsky (1998), Pustejovsky (2006, unpublished)
Attachments: [[pustejovsky2006lexicon.pdf]]

## Summary
The Generative Lexicon addresses many issues with traditional static lexica. It provides a type-based structure that supplies argument-like structure (Qualia) to nouns, that verb selection can generalise over. These generalisations proof more reliable and flexible to innovation that a static enumeration of senses. The relatedness of word senses is highlighted.

## Introduction
- [[Generative Lexicon]]: Theory of linguistic semantics, distributed nature of compositionality.
- Knowledge representation framework.
- [[The lexicon is constantly evolving.]]
- Selectional knowledge in natural language.
- Lexical items have four levels:
	- Lexical Typing Structure
	- Argument Structure
	- Event Structure
	- Qualia Structure
- Qualia structure: Modes of explanation
	- Formal: meaning of word in larger domain
	- Constitutive: relation between object and parts
	- Telic: purpose or function
	- Agentive: origins, "coming into being"

## Traditional Lexical Representations
- Open ended in nature, non-static
- Explains aspects of learnability
- Improvements in the robustness of coverage
- Scheme of explicit encoding of lexical knowledge at several levels.
- Lexical ambiguity resolution integral part of uniform semantic analysis procedure
- Dynamic interpretation of a word in context.
- Carry-over from computational representation
	- Generic knowledge representation (inheritance, rule bases)
	- Shift from world knowledge to lexical knowledge
- Preserves partitioning of information space.
- Lexical meaning separate from other linguistic factors (see [[Syntax is autonomous from semantics.]]) and world knowledge.
- Semantic expressions constructed by a fixed number of generative devices.
- Core set of senses.
- Extended set of word senses through [[Compositionality]].

## The Nature of Polysemy
Word senses are highly flexible and dependent on context. A novel context could enable a new word sense, which is not captured in static lexicons. The level of abstraction in static lexicons is arbitrary and not natural. Static lexicons miss relatedness between word senses.

- [[Context enforces a certain reading of a word.]]
- Traditionally the space of possible senses is partitioned and the sense closest to the word expected in a context wins. (Fuzzy matching)
- This lacks explanatory power necessary for making generalisations/predictions about how words are used in different ways.
- Fuzzy matching is computationally undesirable.
- The lexical entry does not provide enough pointers to discriminate between word senses.
- Any finite enumeration of word senses will not account for creative applications of this adjective in the language. (5)
- There is too much overlap in the core semantic components of different readings of the word *bake*. (5)
- Static lexicons lack any appropriate or natural level of abstraction. (6)
- The traditional notion of word sense, as implemented in current dictionaries is inadequate (6)
- Posting separate word senses weakens the relatedness between senses. (6)

## Levels of Lexical Meaning
- The [[Generative Lexicon]] combines predicate-argument structure (see [[Function-argument structure provides a logical structure of quantified sentences.]]), primitive composition and conceptual organisation. (7)
- Lexical entry represents range of representative aspects of lexical meaning. (7)
	- For isolated word: semantic boundaries appropriate (7)
- In context mutually compatible roles become more prominent (7)
- Beyond feature matching (7)
- Mechanisms to compose individual entries on the phrasal level. (7)
- Composition on the phrasal level (7)
- The static definition of a word provides its literal meaning
- projection generates new meaning in context.
- **Lexical Typing Structure** relates words in a structured type system (inheritance), links to general world knowledge
- **Argument Structure** is a mapping from a word to a function. Relates to syntactic realisation.
- **Event structure** encodes event type for verb or phrase: State, process, transition, focus and rules for event composition.
- **Qualia structure** defines essential attributes of objects, events and relations. Argument structure for nominals.
- Generative devices connecting the levels: Subselection, type coercion and co-composition. 

### Qualia Structure
- [[The qualia structure of a noun is the lexical information about its semantics.]]
- Relations that characterise semantics of lexical items
- Similar to argument structure of a verb
- Qualia structure as a set of constraints on types
- Qualia are ordered hierarchical and can be unified for complex concepts. (physobj and artifact are artifact_obj, food and substance food_substance, etc.)
- Domain of individuals three ranks of type:
	- Natural types
	- Functional types
	- Complex types

### Coercion and Compositionality
- Type coercion converts an argument tot the type which is expected by a function, where it would otherwise result in a type error.
- It allows to bend words according to their qualia, for example "kill" takes any noun as an event type.
	- "The gun killed John." could never be captured in a traditional lexicon, for example.

### Complex Types in Language
- Complex types are _dot objects_.
- These capture inherent ambiguities like *container-containee* (book)
- There must be a relation $R$ that relates the elements of the pairing.
- ![[pustejovsky2006lexicon_19.png]]

## Recent Developments in Generative Lexicon
- Co-composition applied in light verb constructions in Korean and Japanese.
- Qualia structure adopted in noun analyses
- RRG
- Type coercion as part of general theory of selection
	- Pure selection, type exploitation, type coercion