Kager, R., 1999. _[[Optimality Theory]]_. Cambridge university press.
[[René Kager]]

## Basic concepts of OT
[[Grammars of individual languages draw their basic options from a limited set of core properties, the Universal Grammar.]]
[[Marked values are cross-linguistically dispreferred and only used to create contrast.]]
[[The optimal output of a grammar is the least costly violation of ranked universal constraints.]]
[[Language is a system of conflicting forces.]]
[[Markedness is grounded outside of the grammatical system proper.]]
[[Constraints are universal.]]
[[Constraints are violable, but violation must be minimal.]]
