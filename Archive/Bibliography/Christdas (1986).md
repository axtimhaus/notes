---
alias: "(Christdas 1986)"
---

```ad-tldr
title: Tl;dr

Here be dragons.
```

````ad-cite
title: Bibtex
collapse: close
```bibtex
@inproceedings{christdas1986constraining,
  title={On constraining the power of lexical phonology: Evidence from Tamil},
  author={Christdas, Prathima},
  booktitle={North East Linguistics Society},
  volume={17},
  number={1},
  pages={9},
  year={1986}
}
```
````
[[Prathima Christdas]]
Attachments: [[christdas1986constraining.pdf]]

Two assertions:
[[Morphophonological properties are hierarchically ordered.]]
[[Lexical phonology is excessively powerful.]]

