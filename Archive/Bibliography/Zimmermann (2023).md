---
alias: "(Zimmermann 2023)"
---

```ad-tldr
title: Tl;dr

Phrase embeddings -- vector representations of syntactically salient multi-word expressions -- are commonly computed naively from the sum of word embeddings, or as a sub-task to sentence embeddings. This ignores their syntactic structure, from which complex, compositional meaning is constructed by the human language faculty. Downstream systems using multi-word embeddings below sentence level therefore tend to be insensitive to word order, negation and similar phenomena. This is in stark contrast to human intuition and formal semantic readings of phrase structures. Instead of generating latent embeddings from a linearly connected neural architecture, I use tree-structured long short-term memory networks (Tree-LSTMs), which parse sentences "bottom-up" along a syntax tree. This thesis evaluates the efficacy Tree-LSTM phrase embeddings on linguistic and downstream tasks, including sentiment analysis and coreference resolution, and presents a case study on the text reuse alignment.
```

````ad-cite
title: Bibtex
collapse: close
```bibtex
@article{zimmermann2023compositional,
  title={Compositional phrase embeddings from latent Tree-LSTM representations.},
  author=Zimmermann, Victor},
  booktitle={Unpublished master's thesis},
  publisher={Universität Leipzig},
  year={2019}
}
```
````
[[Victor Zimmermann]]

## Learning

## Sisterhood prediction
[[Sisterhood prediction is a learning mechanism for function-argument semantics.]]
Distance between prediction and actual node: Grammaticality measure?

