Löbner, S., 2015. The semantics of nominals. In _The Routledge handbook of semantics_ (pp. 299-318). Routledge.
[[Sebastian Löbner]]

[[Nominals have semantic layers.]]