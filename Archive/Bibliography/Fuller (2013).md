---
alias: "(Fuller 2013)"
---
# On the special role of Faithfulness constraints in morphology-sensitive phonology: The M-Faithfulness Model
#to-read 

```ad-tldr
title: Tl;dr

Here be dragons.
```

````ad-cite
title: Bibtex
collapse: close
```bibtex
@phdthesis{fuller2013special,
  title={On the special role of Faithfulness constraints in morphology-sensitive phonology: The M-Faithfulness Model},
  author={Fuller, Matthew},
  year={2013},
  school={The University of North Carolina at Chapel Hill},
  note={MA thesis}
}
```
````
[[Matthew Fuller]]
Attachments: [[fuller2013faithfulness.pdf]]