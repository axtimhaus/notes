---
alias: "(Ritter 1991)"
---
# Two functional categories in noun phrases: Evidence from Modern Hebrew

```ad-tldr
title: Tl;dr

Number and definiteness need their own projections above NP, as can be shown with data from Hebrew genitive constructions. The two projections are DP and NumP (NBRP) respectively, which constitute the noun phrase. 
```

````ad-cite
title: Bibtex
collapse: close
```bibtex
@incollection{ritter1991two,
  title={Two functional categories in noun phrases: Evidence from Modern Hebrew},
  author={Ritter, Elizabeth},
  booktitle={Perspectives on phrase structure: Heads and licensing},
  pages={37--62},
  year={1991},
  publisher={Brill}
}
```
````
[[Elizabeth Ritter]]
Attachments: [[ritter1991two.pdf]]

## Ideas
- [[Noun phrases are DPs.]]
- [[Number and definiteness have functional heads.]]

## Notes
### Introduction
- Noun phrases have one or more functional heads
- Evidence from Modern Hebrew
- Parallelism between noun phrases and sentences
- **Noun phrases are DPs.**
- Argument of DP is not NP, but NBRP

### Conclusion
- Head movement analysis of derived word order
- N is specified for gender, NBR for number and D for definiteness
- Well-formedness also through agreement with subject

### Simple construct state noun phrases: Evidence for DP
- **Noun phrases are maximal projections of a functional head.**
- Construct state noun phrase (CS):
	- bare genitive phrase immediately following the head noun
	- alienable, inalienable possession
	- theme-source, qualification, quantification
- head noun precedes subject in CS
- two-argument CS: noun-subject-object
- subject asymmetrically c-commands the object
- NSO order parallel to VSO order
	- lexical head raises functional head that governs it
	- Preliminary structure: (.DP (D (.NP Subj (.N' N Obj)))
- derivation involves movement of lexical head
- landing site of moved head is a functional category
- Head Movement Constraint [[Travis (1984)]]:
	- A head can only move to the position of the head that properly governs it.
- ![[Ritter (1991) 2023-04-09 13.30.26.excalidraw]]
- modifying adjectives always agree in definiteness, number and gender
- ![[Ritter (1991) 2023-04-09 14.25.44.excalidraw]]
- adjectives take the definite markedness, because the N gets moved out of the thingy

### Free genitive noun phrases: Evidence for another functional Category
- overt case marker allows "ha-ahavat schel ha-mora" constructs with two definite nouns, so we need an intermediate projection, NumP
- FS DP completely different DP to the CS DP
- 