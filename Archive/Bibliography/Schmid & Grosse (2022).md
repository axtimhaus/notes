```ad-tldr
title: Tl;dr


```

````ad-cite
title: Bibtex
collapse: close
```bibtex
@inproceedings{schmid2022extracting,
  title={Extracting Knowledge With Constructivist Machine Learning: Conceptual and Procedural Models.},
  author={Schmid, Thomas and Grosse, Florian},
  booktitle={AAAI Spring Symposium: MAKE},
  year={2022}
}
```
````
[[Thomas Schmid]], [[Florian Grosse]]
Attachments: 
