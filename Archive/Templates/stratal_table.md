| **Stratum 1**     |     |                     |
|:----------------- |:--- | ------------------- |
| \[]               | \[] | Underlying          |
| ---               | --- | Phonological rules  |
| ---               | --- | Morphological rules |
| ---               | --- | Phonological rules  |
| ---               | --- | Other rules         |
| **Stratum 2**     |     |                     |
| ---               | --- | Phonological rules  |
| ---               | --- | Morphological rules |
| ---               | --- | Phonological rules  |
| ---               | --- | Other rules         |
| **Stratum final** |     |                     |
| ---               | --- | Phonological rules  |
| ---               | --- | Morphological rules |
| ---               | --- | Phonological rules  |
| \[]               | \[] |                     |