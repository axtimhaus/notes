|             Input | <span style="font-variant:small-caps;">Constraint 1</span> | <span style="font-variant:small-caps;">Constraint 2</span> | <span style="font-variant:small-caps;">Constraint 3</span> |
| -----------------:|:---------------------------------------------------------- |:---------------------------------------------------------- |:---------------------------------------------------------- |
| ☞ a.  Candidate A | \*                                                         | \*                                                         | \*\*\*                                                     |
| b.    Candidate B | \*                                                         | \*\*!                                                      |                                                            |

