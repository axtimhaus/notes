Seminar overview for the course taught by Jun.-Prof. [[Maria Kouneli]].

## Notes on organisational matters
- Readings before class, come prepared.
- Projektarbeit and presentation in this class or [[SE Morphological irregularities]].
	- Decide until 31/05/2023.
- Email if questions come up.

## Seminar notes
### Introduction (2023/04/04)
- book vs book-s
- Semantic number vs morphological number vs syntactic number
- grouping/counting vs individuating
- mass/count distinction
- verbal plurality
- **The semantics of number is an old research topic in philosophy of language.**
- one unit vs multiple units? not necessarily!
	- Does plural include singularity?
		- Inclusive vs exclusive plural.
		- *"Does Mary have children?"*
		- *"Weapons are prohibited here."*
	- *zero books*
		- *scissors*, *pants*
- Singular and plural? 
	- Also dual, paucal, general number, associative, distributive, ...
- Not all languages express number distinctions morphologically.
	- Number marking vs classifiers (modifiers that individuate)
	- Often binary (either marking or classifiers), excpt. Armenian
- Not all relevant items mark number (e.g. nouns): ?honesties
- In some languages number marking is optional
	- three books
- Number is not just a nominal category.
- There are mixed categories: participles, gerunds
- Number, together with person and gender constitute class of phi/$\phi$-features (participate in syntactic agreement).
	- Gender is a bit arbitrary ~ completely arbitrary in some languages.
	- "Unified Phi Theory"
- [[Adger & Harbour (2008)]]: three broad theoretical questions:
	- Substance
		- What is the inventory of number features?
			- feature values/descriptive labels vs primitive features
			- Is there a universal set of number features?
			- If there is a universal one, do all languages have it or do they chose from it?
	- Structure
		- How are number features structured and how do they relate to attested patterns of markedness?
			- privative or bivalent? (presence, absence of features, +PL or -SG?)
				- Is singular marked?
			- Feature geometry?
		- attested patterns of morphological syncretism, syntactic agreement, etc.
		- Markedness
			- Formal (morphological) markedness: Do you have more morphological material. E.g. plural morpheme in English
			- Functional markedness: unmarked if used in a wider range of functional environments, related to 'semantic markedness'
			- Relationship between these two types of markedness
	- Interaction
		- How do number features interact with other $\phi$-features?
		- What do such interactions tell us about theory?
		- [[Adger & Harbour (2008)]]: necessarily transmodular, capturing morphology, syntax _and_ semantics.
- What are features? How are they represented in the syntax?
	- Where in the structure are number features located?
	- There only one feature per head? If so, how fine-grained are number projections?


## Slides
1. [[number2023intro.pdf]]

## Readings
- [[Ritter (1991)]] (2023/4/11)
- **[[Corbett (2000)]]**
- [[Adger & Harbour (2008)]]
- [[Preminger (2020)]]
- [[Bernstein (1993)]]
- [[Carstens (1991)]]
- [[Wiltschko (2008)]]