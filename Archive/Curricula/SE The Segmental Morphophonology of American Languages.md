Overview over The Segmental Morphophonology of American Languages seminar by Prof. Dr [[Jochen Trommer]].

[Website](https://home.uni-leipzig.de/jtrommer/SegmentalAmerica2023.html)
[Moodle](https://moodle2.uni-leipzig.de/course/view.php?id=43096)

Journal: IJAL

## Notes on organisational matters
- Motivation: Egoistic reasons, connection to research project.
- Trommer mostly familiar with African languages.
- A bit like the reading group.
- Roughly the same things in this course and [[SE The Segmental Morphophonology of Asian Languages]].
- Everyone presents a paper
- Three presentations and paper
	- In-class presentation of a paper
	- Idea presentation at end of semester
	- Project presentation at end of semester break (Graded)
	- Project paper (Graded)

## Notes on the segmental morphophonology of American languages
### Introduction (2023/4/6)
- Segmental stuff
- Interaction with stuff like stress and tone
- Phonology and its connection to morphology
- Cyclic and stratal models of morphology-phonology interaction
- Stratal organisational claims
- Alternative claims

#### Stratal phonology
- How does morphology interact with phonology?
- Rule based approach:
	- [[Lexical Phonology]]
- Constraint based:
	- [[Stratal OT]]: Strata + [[Optimality Theory|OT]]
- English examples: *-in*, *-un*
	- *in-convenient*
	- *un-marked*
	- both mean (roughly) *not*
	- assimilation with base: *impossible*, _illogical_, _irregular_
		- but: _unlikely_
	- Is this idiosyncratic? (morpheme-specific phonology)
- Predicts the order in which affixes are added.
	- un-in-convenient
	- \*in-un-marked
	- The morphological ordering seems to predict the ordering in phonology.
- Inner level and outer level that behave in different ways.
- Many different models:
	- Affixation
	- Cyclicity: Phonology after every affix.
	- In some language some strata are cyclic, some are not.
	-  **Differing phonologies**
	- **Ordering asymmetries**
	- **Derivational interaction of morphology and phonology**
	- Bermúdez-Otero: Difference between storing with internal structure or without.
	- General model:

| **Stratum 1**     |                   |                       |
|:----------------- |:----------------- |:--------------------- |
| \[marked]         | \[possible]       | root                  |
| ---               | \[in\[possible]   | Affix                 |
| ---               | ---               | Affix                 |
| ---               | \[impossible]     | Phonology 1           |
| **Stratum 2**     |                   |                       |
| \[un\[marked]     | \[un\[impossible] | Affix                 |
| ---               | ---               | Affix                 |
| \[unmarked]       | \[unimpossible]   | Phonology 2           |
| **Stratum final** |                   |                       |
| ---               | ---               | word + word           |
| ---               | ---               | Postlexical phonology |
| \[unmarked]       | \[unimpossible]   |                       |

- assumes lexicalist architecture
	- different from Distributed Morphology
	- completely incompatible
- ((Can we read anti-Stratal OT paper by DM people?))
- Kiparsky: DM people are cheating.

#### Prosodic Phonology
- Feet, prosodic words
- Prosodic constituents have similar effects, what you would expect from strata.
- Example: _eigen_ - _eigen-e_ - _eigen-ʔartig_
	- ((Are vowel-initial semantic morphemes maybe just stored with initial glottal stop, like words?))

#### Cophonologies
- Grew out of Stratal OT
- Historically not so different
- Coëxisting phonological grammars in the same language
- Constuction phonology?
- Parallel vs stratal

#### American languages
- Often polysynthetic, especially North-American languages
	- "Lots of morphemes put together in a single word."
	- "A language which does in a word that other languages do in an entire sentence."
- Mohawk [[Baker (1996)|(Baker 1996)]]
	- Classical example for word formation in the syntax.
	- _Incorporation_
		- e.g. object noun becomes part of transitive verb
		- noun gets incorporated into the verb
		- You want to say it moved in syntactically
	- modifier stranding
		- claim: that isn't really a modifier
	- many languages, where incorporation looks more like compounding
	- [[Jaker & Kiparsky (2020)]]: six strata/levels
		- Tetsǫ́t'ıné data: [[Jaker & Cardinal (2020)]]
	- level and strata for our purposes synonymous
	- template morphology
		- affixes are specified for specific slots, 
		- one affix for specific template position
		- most of the time not true
		- similar to strata: some kind of ordering not due to syntax


## Readings
- [[Stonham (2007)]] (2023/4/13)