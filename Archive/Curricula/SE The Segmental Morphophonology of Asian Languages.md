Overview over The Segmental Morphophonology of American Languages seminar by Prof. Dr [[Jochen Trommer]].

## Notes on organisational matters


## Notes on the segmental morphophonology of Asian languages
### Introduction (2023/4/6)
- Historical notes: [[Paul Kiparsky|Kiparsky]] $\approx$ Chomsky
	- But: Kiparsky papers get better over time.
	- Really nice guy.
	- To Kiparsky's name there is connected the major development that happened.
	- He was at the beginning of both Lexical Phonology and Stratal OT.
- Idea: Strata tell you something about semantics. The presence of the contributing affixes has disappeared in later strata.
- Hardly everyone worked on semantics in stratal phonology.

#### Asian languages
- Vast continent
 [![](https://upload.wikimedia.org/wikipedia/commons/8/86/Language_families_of_Asia.png)](https://en.wikipedia.org/wiki/File:Language_families_of_Asia.png)
- Semitic: large selection of papers, you could do an entire course.
- Chinese: Trommer couldn't find many interesting things.
- Sino-Tibetan: Some interesting stuff
- Turkish: More in detail (2-3 sessions), huge literature, important role
	- Inkelas, Organ, Cophonologies
- Indo-European: Something on Hindi, Pashto, Armenian
- Uralic: Finnish (Kiparsky)
- Koreanic
- "What about the Caucasus?" - Georgian?
- Indonesian/Austronesian
- **Next week: Kiparsky on Arabic**

#### Turkish
- Velar deletion (Erdogan - /Erdoan/)
- Not all affixes trigger velar deletion
- edʒek does not trigger velar deletion, but undergoes it: edʒe-im
- Kiparsky: It works like a factory.
- Inkelas: We don't have to go through every "room" of the factory, so some phonological constraints (bisyllabic) don't apply.
- ((Maybe put the inkelas organ chart here))

#### Korean
- Coda Cluster Simplification (CCS)
	- At the end of a word the second consonant is deleted.
- Post-Obstruent Tensification (POT)
	- Voiceless stop after obstruent becomes tense stop
	- /kuk + pap/ - \[kukp'ap]
- Opaque application of POT
- Opacity: Rule interaction

| **Stratum 1** |             |              |
|:------------- |:----------- |:------------ |
| *malk*        | *talk*      | *Underlying* |
| malk*ta*      |             | Aff.         |
| malk'ta       |             | POT          |
| **Stratum 2** |             |              |
|               | talk*putʰə* | Aff.         |
| malt'a        |             | CCS          |

- Stratal and [[Cophonologies]] models give you opacity modelling

#### Eastern Mari
- Vowel Harmony between first and third vowel
- You can stress a Schwa!
- Stress the rightmost full-vowel syllable
- If Schwa: stress the leftmost Schwa syllable
- "Conflicting directionality"
- Parallel OT?

#### Malayalam
- [[Mohanan & Mohanan (1984)]]
- Two types of Compounding: Cocompounding and Subcompounding
- Word-melody, like sentence melody, but for words
- 

## Readings)
- [[Christdas (1986)]]
- [[Kiparsky (2000)]] (2023/4/13)