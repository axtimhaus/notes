Overview over the Morphological irregularities seminar by Prof. Dr [[Barbara Stiebels]].

## Notes on organisational matters
- Exam: Projektarbeit, small research project. Small presentation (30 minutes), documentation of results (paper), both graded separately.
	- Can be done in either [[SE Morphological irregularities]] or [[SE The morphosyntax of number]].
	- Decide in about a month, so ~2023/5.
- Class on 2023/6/27 will be rescheduled, presentations in 2023/9.
- We read papers, having discussion leads (but no handouts).

## Notes on morphological irregularities
### Introduction (2023/4/4)
- English irregular plurals: oxen, children, sheep, mice, etc.
- Irregular patterns, lexically specified.
- Class affiliation, often not predictable.
- Mostly high frequency items ((-> Haspelmath?))
- Retention of old morphological pattern.
- Subregularities do exist:
	- In German strong verbs the vowel of the preterite stem is different to the vowel of the base stem. Past participle has suffix -n. Past subjunctive exhibits front vowel. 
	- Remnants of old number distinction in preterite forms.
	
#### Morphomes
**Q: Which categories show morphomic patterns?
Q: Are these morphomic patterns synchronically active (e.g. leading to analogies)?
Q: Can alleged morphomic patterns be explained by extra-morphological factors?**  

- [[Aronoff (1994)]]: A morphome is a function that is neither morphosyntactic nor morphophonological.
	- Latin: 3rd stem is used as base for several derivational affixes
- [[Maiden (2021)]]: Only speak of "morphomic patterns". Original determinants are irrecoverably defunct, but still active.
	- Galician: "L-pattern", colber-caber merge, 1SG PRS.IND takes stem from PRS.SBJV.
	- Old Romanian: class of verbs with "weird" stem distribution pattern

#### Suppletion
**Q: Which categories and elements are subject to suppletion?
Q: Are there systematic patterns of suppletion (besides the \*ABA restriction)?
Q: Is suppletion local? Are there intervention effects?  
Q: Are there lexical items that supplete for more than one category? Which consequences does this have if the relevant categories have to be expressed simultaneously?**

- Introduced by [[Osthoff (1899)]]:
- Two linguistic units are formally, or materially completely distinct, but semantically related in an obvious way.
- E.g. good-better-best, bad-worse-worst, \*ABA-pattern
- Approach: Containment
- Categories: Number, case, poss, verbal number, honirification, pronouns, numerals, ...
- Bad habit: turning discoveries into tests.

#### Defective paradigms
[[Sims (2015)]]:
**Q: What conditions lead to defectiveness, and why do speakers sometimes choose to leave a paradigm cell empty, rather than applying some kind of repair strategy?  
Q: Once gaps arise, how are they learned by new generations? In particular, why do gaps often persist in a language, even in the absence of the conditions that originally gave rise to them?  
Q: How should paradigmatic gaps be represented within a theory of inflection? Are paradigmatic gaps inflectional anomalies? Should defectiveness be accounted for outside of the normal functioning of inflectional structure or in a way that is integrated with it?**  

- A lexeme lacks one or more forms of the paradigm
- forego-forgoed?-forewent?
- Russian: many verbs lack 1SG.PRS
- Lays bare matching requirement.
- Don't equate defectiveness with paradigmatic gap (e.g. mass nouns).
- Defective paradigms are "awkward", cannot be explained by systematic effects, do not form a natural class

##### Uninflectedness
**Q: Do uninflectable items allow derivational morphology? May they occur in compounds (as heads/non-heads)?
Q: Can the group of uninflectable items be characterized by some features? Are there systematic cases of uninflectable items (e.g. certain loans, the output of clippings, backformations or some other less regular pattern of word formation)
Q: Which inflectional categories are more likely to include uninflectable items?**

- [[Spencer (2020)]]: Some classes of lexemes are not associated with any inflectional paradigm.
- Some Slavic words don't fit any potential inflection classes (e.g. kangaroo)
- Haspelmath: That is just a footnote.
- Loans, but also other categories (e.g. proper names in Germanic languages). 
- backformation: notlanden - notgelandet? - genotlandet?
- loans: googlen, downloaden-gedownloaded?-downgeloaded?

#### Deponency
**Q: Which other form-function mismatches may be classified as instances of deponency?
Q: Is the list of items that display the form-function mismatch completely arbitrary or do they form a motivated class?**

- Mismatch between morphological form and syntactic function.
- Latin: am-o (I love) - am-or (I am loved), but hort-or (I encourage)
- Deponent forms lexically specified.
- Often near synonym pairs of deponent/active verbs.
	- hortor/moneo

#### Modeling subregularities
- General (template) for strong verbs in German: [[Wunderlich & Fabri (1995)]]
- You have a general kind of structure and the specific pattern indicates relations between various stems.
- It's not a big mess, there is structure.

## Handouts
1. [[morphirr2023intro.pdf]]

## Readings
Morphomes:
- **[[Maiden (2021)]]** (2023/4/11)
- [[Herce (2019)]]
- [[de Chene (2022)]]
- [[Enger (2019)]]
- [[Esher (2015)]]
- [[Esher (2016)]]
- [[Luís & Bermúdez-Otero (2016)]]
- [[O'Neill (2013)]]
- [[O'Neill (2014)]]

Suppletion:
- **[[Corbett (2007)]]**
- **[[Bobaljik (2015)]]**
- [[Vaselinova (2003)]]
- [[Vafaeian (2013)]]
- [[Juge (2019)]]
- [[Pomino & Remberger (2022)]]
- Surrey suppletion databases.

Defective paradigms:
- **[[Fábregas (2018)]]**
- **[[Sims (2015)]]**
- [[Chuang et al. (2022)]]
- Surrey defectiveness databases.

Uninflectedness:
- **[[Doleschal (2000)]]**
- **[[Spencer (2020)]]**

Deponency:
- **[[Grestenberger (2019)]]**
- **[[Müller (2013)]]**
- Surrey deponency databases