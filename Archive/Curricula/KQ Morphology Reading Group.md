Overview over the Morphology Reading Group by Prof. Dr [[Barbara Stiebels]].

Journals: Morphology, Word Structure, Glossa, NLLT

## Readings
- [[Pertsova (2022)]] (2023/04/13)
- [[Dolatian (2021)]] (2023/04/20)
- [[Storme (2022)]] (2023/05/04)
- [[Gouskova & Bobaljik (2022)]] (2023/05/11)
- [[Ershova (2020)]] (2023/05/25)