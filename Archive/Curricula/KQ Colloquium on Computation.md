# ✨Ned & Greg read a book✨

Instructor: Prof. Dr [[Greg Kobele]]
Draft: [[barker2014continuations.pdf]]
Book: [[barker2015continuations.pdf]]

## Notes on organisational matters
- Reading group
- Book: Continuations and Natural Language (Barker and Sheng)
- Categorial grammar

## Colloquium notes
### Introduction (2023/4/5)
![[KQ Colloquium on Computation 2023-04-05 13.36.50.excalidraw]]
![[KQ Colloquium on Computation 2023-04-05 13.38.14.excalidraw]]
![[KQ Colloquium on Computation 2023-04-05 13.39.15.excalidraw]]
![[KQ Colloquium on Computation 2023-04-05 13.40.32.excalidraw]]
![[drawing_collintro.excalidraw]]