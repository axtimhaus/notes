[[Mike Frazier]]

Overview: [[ellipsis2023syllabus.pdf]]

## Readings

## Seminar notes
### 2023/04/03: Introduction
- Minimalism vs Minimalist Grammar
- Syntactic, but interfaces with _everything_.
- **! Get psycholinguistic readings from Simon.**
- There's nothing wrong with changing your mind. That's how we grow.
- The ultimate form-meaning mismatch.
- How is the grammar organised to allow something like this?
- How do we know this is ellipsis vs other null-elements?
	- Antecedent must be linguistic (but not the same speaker).
	- Rare cases where this doesn't work (e.g. slogans "yes we can")
- Gapping: John brought beer and Mary wine.
- 